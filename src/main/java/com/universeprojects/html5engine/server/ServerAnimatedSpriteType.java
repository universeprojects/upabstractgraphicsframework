package com.universeprojects.html5engine.server;

import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteTypeFrame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Crokoking
 */
@SuppressWarnings("unused")
public class ServerAnimatedSpriteType extends AnimatedSpriteType {

    public String label;
    public List<ServerAnimatedSpriteTypeFrame> frames = new ArrayList<>();

    public ServerAnimatedSpriteType(ServerResourceManager resourceManager) {
        this(resourceManager, null);
    }

    public ServerAnimatedSpriteType(ServerResourceManager resourceManager, String label) {
        this.label = label;
        this.resourceManager = resourceManager;
    }

    public ServerAnimatedSpriteTypeFrame newFrame(ServerSpriteType spriteType, int duration, int adjustedX, int adjustedY, float adjustedScale) {
        ServerAnimatedSpriteTypeFrame frame = new ServerAnimatedSpriteTypeFrame(spriteType, duration, adjustedX, adjustedY, adjustedScale);
        frames.add(frame);
        return frame;
    }

    public ServerAnimatedSpriteTypeFrame newFrame(ServerSpriteType spriteType, int duration) {
        ServerAnimatedSpriteTypeFrame frame = new ServerAnimatedSpriteTypeFrame(spriteType, duration);
        frames.add(frame);
        return frame;
    }

    public int getMaxWidth() {
        int maxWidth = 0;
        for (ServerAnimatedSpriteTypeFrame frame : frames) {
            if (frame.spriteType == null) {
                return -1;
            }
            maxWidth = UPMath.max(maxWidth, (int) ((frame.getAdjustedScale() + 1) * frame.spriteType.getAreaWidth()) + frame.getAdjustedX());
        }
        return maxWidth;
    }

    public int getMaxHeight() {
        int maxHeight = 0;
        for (ServerAnimatedSpriteTypeFrame frame : frames) {
            if (frame.spriteType == null) {
                return -1;
            }
            maxHeight = UPMath.max(maxHeight, (int) ((frame.getAdjustedScale() + 1) * frame.spriteType.getAreaHeight()) + frame.getAdjustedY());
        }
        return maxHeight;
    }

    public boolean isReadyForSaving() {
        if (label == null || label.isEmpty()) {
            return false;
        }
        for (ServerAnimatedSpriteTypeFrame frame : frames) {
            if (frame.spriteType == null || !frame.spriteType.isReadyForSaving()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void removeFrame(int index) {
        frames.remove(index);
    }

    @Override
    public void reorderFrame(int fromIndex, int toIndex) {
        ServerAnimatedSpriteTypeFrame curFrame = frames.get(fromIndex);
        frames.remove(curFrame);
        frames.add(toIndex, curFrame);
    }

    @Override
    public AnimatedSpriteTypeFrame newFrame(String spriteTypeKey, int durationInMillis) {
        ServerSpriteType sr = (ServerSpriteType) resourceManager.getSpriteType(spriteTypeKey);
        return newFrame(sr, durationInMillis);

    }

    @Override
    public int getFrameNumber() {
        return frames.size();
    }

    @Override
    public AnimatedSpriteTypeFrame getFrame(int index) {
        return frames.get(index);
    }

    @Override
    public List<ServerAnimatedSpriteTypeFrame> getFrames() {
        return Collections.unmodifiableList(frames);
    }
}
