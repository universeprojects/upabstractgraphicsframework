package com.universeprojects.html5engine.server;

import com.universeprojects.html5engine.shared.abstractFramework.MarkerCircle;

/**
 * @author Crokoking
 */
public class ServerMarkerCircle extends MarkerCircle {

    public ServerMarkerCircle copy() {
        ServerMarkerCircle mc = new ServerMarkerCircle();
        mc.label = label;
        mc.x = x;
        mc.y = y;
        mc.setTypeLabel(getTypeLabel());
        mc.radius = radius;
        return mc;
    }

}
