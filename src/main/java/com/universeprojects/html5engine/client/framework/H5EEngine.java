package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.universeprojects.common.shared.callable.ReturningCallable0Args;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Check;
import com.universeprojects.common.shared.util.GWTDebug;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.shared.GenericEvent;
import com.universeprojects.html5engine.shared.abstractFramework.ResourceManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@SuppressWarnings("unused")
public class H5EEngine {

    private final Logger log = Logger.getLogger(H5EEngine.class);

    // Major objects...
    /**
     * The resource manager for this engine. Accessible via the getResourceManager() method.
     */
    private ResourceManager resourceManager;

    private boolean behavioursAreProcessing = false;
    private Set<H5EBehaviour> allBehaviours = new HashSet<>();
    private Set<H5EBehaviour> newBehaviours = new HashSet<>();
    private Set<H5EBehaviour> deleteBehaviours = new HashSet<>();

    /**
     * The viewport object controls how the layers get displayed. Viewports can be zoomed, translated and possibly rotated. A viewport can be thought of as a camera looking at the layers.
     */
    private Viewport uiViewport;
    private Viewport gameViewport;

    private OrthographicCamera uiCamera;
    private OrthographicCamera gameCamera;


    final InputMultiplexer inputMultiplexer = new InputMultiplexer();

    /**
     * A list of graphical layers (H5ELayer) that this engine maintains
     */
    private final List<H5ELayer> layers = new ArrayList<>();
    private final List<H5ELayer> layers_view = Collections.unmodifiableList(layers);
    private final Map<String, H5ELayer> layersByName = new HashMap<>();

    private final Map<String, Object> engineBoundObjects = new TreeMap<>();

    /**
     * The FPS we would like to achieve
     */
    private int targetFps = 60;
    /**
     * The ticks per second we would like to achieve
     */
    private int targetTps = 50;
    /**
     * Enables or disables the 'frameskip' feature. Frame skip will attempt to execute the onTick event at the requested target frame rate
     * and skip drawing graphical frames to do it. Making this value false will make the game operations dependant on frame rate.
     */
    private boolean frameSkip = true;

    /**
     * If this is turned on, a line of text will appear across the top of the canvas with some debugging details
     */
    private boolean debugMode = false;

    private Skin skin;
    private int tickCount;//TODO: update
    private long lastFrameDraw;

    private int fpsFrameCount = 0;
    private double fps = 0;
    private long lastFpsCalcTime;

    Stage debugInfoStage;
    Label debugInfoLabel;
    public final GenericEvent.GenericEvent0Args onRender = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onResize = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onTick = new GenericEvent.GenericEvent0Args();

    private boolean rotationMode;
    private float rotation;

    public void clearBehaviours() {
        allBehaviours.clear();
        deleteBehaviours.clear();
        newBehaviours.clear();
    }

    public void create(int screenWidth, int screenHeight) {
        uiCamera = new OrthographicCamera();
        gameCamera = new OrthographicCamera();
        uiViewport = new ScreenViewport(uiCamera);
        gameViewport = new ScreenViewport(gameCamera);
        uiViewport.update(screenWidth, screenHeight, true);
        gameViewport.update(screenWidth, screenHeight, true);

        setupInput();

        // Create the default layer (layer 0 - commonly used as background)
        newUILayer("default");
    }

    public void setupDebugInfoViewer() {
        debugInfoStage = new Stage(getUiViewport());
        debugInfoLabel = new Label("", skin);
        debugInfoLabel.setFillParent(true);
        debugInfoStage.addActor(debugInfoLabel);
        debugInfoLabel.setAlignment(Align.topLeft);
    }

    public <T> void setEngineBoundObject(String name, T object) {
        engineBoundObjects.put(name, object);
    }

    @SuppressWarnings("unchecked")
    public <T> T getEngineBoundObject(String name) {
        return (T) engineBoundObjects.get(name);
    }

    @SuppressWarnings("unchecked")
    public <T> T getOrCreateEngineBoundObject(String name, ReturningCallable0Args<T> generator) {
        T obj = (T) engineBoundObjects.get(name);
        if(obj != null) {
            return obj;
        }
        obj = generator.call();
        engineBoundObjects.put(name, obj);
        return obj;
    }

    public void addInputProcessor(InputProcessor processor) {
        inputMultiplexer.addProcessor(processor);
    }

    public void removeInputProcessor(InputProcessor processor) {
        inputMultiplexer.removeProcessor(processor);
    }

    public void toggleFullscreen() {
        if (Gdx.graphics.supportsDisplayModeChange()) {
            Graphics.Monitor currMonitor = Gdx.graphics.getMonitor();
            Graphics.DisplayMode displayMode = Gdx.graphics.getDisplayMode(currMonitor);
            if (Gdx.graphics.isFullscreen()) {
                final float windowFactor = 0.8f;
                Gdx.graphics.setWindowedMode((int) (displayMode.width * windowFactor), (int) (displayMode.height * windowFactor));
            } else {
                Gdx.graphics.setFullscreenMode(displayMode);
            }
        }
    }

    public void setupInput() {
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    public Skin getSkin() {
        return skin;
    }

    public int getTickCount() {
        return tickCount;
    }

    public void setResourceManager(ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    public void render() {
        try {
            doRender();
        } catch (Throwable ex) {
            Log.error("Exception on render-level", ex);
        }
    }

    protected void doRender() {
        onRender.fire();
        if (frameSkip) {

            if (lastFrameDraw == 0) {
                // initialize for frameskip mode
                lastFrameDraw = System.currentTimeMillis();
            }

            // Determine how many ticks we need to execute
            double millisPerFrame = UPMath.floor(1000 / this.targetTps);
            long timeDiff = System.currentTimeMillis() - lastFrameDraw;
            double ticksToProcess = UPMath.floor(timeDiff / (millisPerFrame));

            int ticksProcessed = 0;
            for (int i = 0; i < ticksToProcess; i++) {
                // First clear the debugValuesPerTick
                GWTDebug.debugValuesPerTick.clear();

                this.onTick.fire();

                ticksProcessed++;
                tickCount++;

                // This will ensure the GUI doesn't lock up by exiting the method every
                // 200 ticks of "catch-up"
                if (ticksProcessed > 200) {
                    break;
                }
            }

            // We are only going to remember the last frame draw time if we've actually caught up
            // If we haven't caught up yet, we're going to only do 100 operation cycles before we
            // allow the browser to execute any events it needs.
            // Instead, we will simply add on the number of cycles we did end up doing
            // to the lastFrameDraw time.
            lastFrameDraw += ticksToProcess * millisPerFrame;

            // Now exit early to ensure we don't lock up the browser and instead of drawing the graphics,
            // this method will be executed again for further catch-up
            if (ticksProcessed > 100) {
                return;
            }
        } else    //Otherwise, turn it off
        {
            onTick.fire();
        }

        // keep track of FPS
        measureFPS();

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        for (H5ELayer layer : layers) {
            layer.render();
        }

        drawDebugInfo();
    }

    protected void addBehaviour(H5EBehaviour behaviour) {
        if (behavioursAreProcessing)
            newBehaviours.add(behaviour);
        else
            allBehaviours.add(behaviour);

    }

    protected void removeBehaviour(H5EBehaviour behaviour) {
        if (behavioursAreProcessing)
            deleteBehaviours.add(behaviour);
        else
            allBehaviours.remove(behaviour);
    }

    public void processBehaviours(float delta) {
        try {
            behavioursAreProcessing = true;
            for (H5EBehaviour behaviour : allBehaviours) {
                behaviour.act(delta);
            }
        } finally {
            behavioursAreProcessing = false;
        }

        newBehaviours.removeAll(deleteBehaviours); //Make sure no new behaviours were already removed

        // Now delete all the behaviours that were attempted to be deleted while the behaviours were processing
        allBehaviours.removeAll(deleteBehaviours);
        deleteBehaviours.clear();

        // Now add all the behaviours that were attempted to be added while the behaviours were processing
        allBehaviours.addAll(newBehaviours);
        newBehaviours.clear();
    }

    private void measureFPS() {
        this.fpsFrameCount++;

        long msSinceLastFpsCalc = System.currentTimeMillis() - this.lastFpsCalcTime;
        if (msSinceLastFpsCalc >= 1000) {
            double secondsSinceLast = (double) msSinceLastFpsCalc / 1000d;
            this.fps = ((double) this.fpsFrameCount / secondsSinceLast);
            this.fpsFrameCount = 0;
            this.lastFpsCalcTime = System.currentTimeMillis();

            if(log.isDebugEnabled()) {
                log.debug("fps=" + fps);
            }
        }
    }

    private void drawDebugInfo() {
        if (!this.debugMode || debugInfoLabel == null) {
            return;
        }

        final float fpsText = Math.round(this.fps * 100) / 100f;
        StringBuilder debugInfo = new StringBuilder("FPS: " + fpsText);

        for (Object key : GWTDebug.debugValuesPerTick.keySet()) {
            debugInfo.append(", ").append(key).append("=").append(GWTDebug.debugValuesPerTick.get(key));
        }

        debugInfoLabel.setText(debugInfo.toString());

        debugInfoStage.draw();
    }

    public void resize(int width, int height) {
        uiViewport.update(width, height, true);
        gameViewport.update(width, height, true);
        onResize.fire();
    }


    public void dispose() {
        for (H5ELayer layer : layers) {
            layer.dispose();
        }
    }

    /**
     * Sets the target TPS for this instance of the engine.
     *
     * @param value The TPS that you wish the engine to attempt to achieve.
     */
    public void setTargetTps(int value) {
        if (value < 0)
            throw new IllegalArgumentException("Invalid target TPS: " + value);

        targetTps = value;
    }

    /**
     * @return the target TPS that this engine instance will attempt to achieve.
     */
    public int getTargetTps() {
        return targetTps;
    }

    /**
     * Sets the target FPS for this instance of the engine.
     *
     * @param value The FPS that you wish the engine to attempt to achieve.
     */
    public void setTargetFps(int value) {
        if (value < 0)
            throw new IllegalArgumentException("Invalid target FPS: " + value);

        targetFps = value;
    }

    /**
     * @return the target FPS that this engine instance will attempt to achieve.
     */
    public int getTargetFps() {
        return targetFps;
    }

    public void setFrameSkip(boolean value) {
        this.frameSkip = value;
    }

    public boolean getFrameSkip() {
        return frameSkip;
    }

    public void setDebugMode(boolean value) {
        debugMode = value;
    }

    public void setLayerDebugMode(boolean value) {
        for (H5ELayer layer : layers) {
            layer.setDebugAll(true);
        }
    }

    public boolean getDebugMode() {
        return debugMode;
    }

    /**
     * Gets the resource manager for this engine.
     * The resource manager allows full access to image/audio resources that
     * get loaded in.
     *
     * @return The resource manager for this engine. There is only one.
     */
    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    /**
     * Creates a new basic {@link H5ELayer} on top of the layer stack.
     * To add a custom layer that extends {@link H5ELayer}, use {@link #addLayer(H5ELayer)}.
     *
     * @return The newly created layer
     */
    public H5ELayer newUILayer(String name) {
        if (Strings.isEmpty(name)) {
            name = "Layer " + layers.size();
        }
        return addLayer(new H5ELayer(this, name, getUiViewport()));
    }

    public H5ELayer newGameLayer(String name) {
        if (Strings.isEmpty(name)) {
            name = "Layer " + layers.size();
        }
        return addLayer(new H5ELayer(this, name, getGameViewport()));
    }


    /**
     * This allows you to add a custom layer to the list of layers in the engine.
     * A custom layer extends {@link H5ELayer} to add special capabilities, not available in the base class.
     */
    public <T extends H5ELayer> T addLayer(T layer) {
        if (layer == null) {
            throw new IllegalArgumentException("Layer can't be null");
        }
        String layerName = layer.getName();
        if (Strings.isEmpty(layerName)) {
            throw new IllegalStateException("Layer must have a name");
        }
        if (layersByName.containsKey(layerName)) {
            throw new IllegalStateException("The engine already has layer " + Strings.inQuotes(layerName));
        }

        layers.add(layer);
        layersByName.put(layerName, layer);

        return layer;
    }

    /**
     * Returns the viewport this engine is currently using. A viewport is used
     * as a "camera", use the viewport's "translateViewport" function to move
     * the "camera".
     *
     * @return {H5EViewport} The viewport this engine is currently using.
     */
    public Viewport getUiViewport() {
        return uiViewport;
    }

    public Viewport getGameViewport() {
        return gameViewport;
    }

    public OrthographicCamera getUiCamera() {
        return uiCamera;
    }

    public OrthographicCamera getGameCamera() {
        return gameCamera;
    }

    public List<H5ELayer> getLayers() {
        return layers_view;
    }

    public ListIterator<H5ELayer> getLayersIterator() {
        return layers_view.listIterator(layers.size());
    }

    public H5ELayer getLayer(String name) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Name can't be null or empty");
        }
        return layersByName.get(name);
    }


    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    public int getWidth() {
        return uiViewport.getScreenWidth();
    }

    public int getHeight() {
        return uiViewport.getScreenHeight();
    }

    public float getGameZoom() {
        return gameCamera.zoom;
    }

    public void setGameZoom(float zoom) {
        gameCamera.zoom = zoom;
    }


    public boolean isRotationMode() {
        return rotationMode;
    }

    public void setRotationMode(boolean rotationMode) {
        this.rotationMode = rotationMode;
    }

    public float getRotation() {
        return rotation;
    }

    public void setGameCamera(float x, float y, float rotation) {
        final OrthographicCamera camera = getGameCamera();
        camera.position.set(x, y, 0);
        if (!Check.equal(this.rotation, rotation)) {
            camera.up.set(0, 1, 0);
            if (rotation != 0) {
                camera.up.rotate(camera.direction, rotation);
            }
            this.rotation = rotation;
        }
    }

    public void clearLayers() {
        layers.forEach(H5ELayer::dispose);
        layers.clear();
    }
}
