package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.math.UPMath;

public class H5EAnimatedSprite extends H5ESprite {

    private H5EAnimatedSpriteType animatedSpriteType = null;
    private int currentFrameIndex = -1;
    private Long startTime = null;
    private boolean playing = false;
    private float speedMultiplier = 1;
    private boolean looped = true;
    private boolean hideWhenFinished = false;

    public H5EAnimatedSprite(H5ELayer layer, String animatedSpriteTypeKey) {
        this(layer, animatedSpriteTypeKey, null);

    }

    public H5EAnimatedSprite(H5ELayer layer, H5EAnimatedSpriteType animatedSpriteType) {
        this(layer, animatedSpriteType, null);
    }

    public H5EAnimatedSprite(H5ELayer layer, String animatedSpriteTypeKey, Integer level) {
        super(layer, level);

        H5EAnimatedSpriteType animatedSpriteType = (H5EAnimatedSpriteType) layer.getEngine().getResourceManager().getAnimatedSpriteType(animatedSpriteTypeKey);
        if (animatedSpriteType == null) {
            throw new IllegalArgumentException("The animated sprite type key '" + animatedSpriteTypeKey + "' cannot be found in the resource manager");
        }

        this.animatedSpriteType = animatedSpriteType;
        activateFrame(0);
        setWidth(spriteType.getWidth());
        setHeight(spriteType.getHeight());
        setupBehaviour();
    }

    public H5EAnimatedSprite(H5ELayer layer, H5EAnimatedSpriteType animatedSpriteType, Integer level) {
        super(layer, level);

        this.animatedSpriteType = animatedSpriteType;
        activateFrame(0);
        setWidth(spriteType.getWidth());
        setHeight(spriteType.getHeight());
        setupBehaviour();
    }

    private void setupBehaviour() {
        // Add a custom behaviour so that we get access to the doTick() of the behaviour to update our animation
        new H5EBehaviour(this) {

            @Override
            public String getName() {
                return "H5EAnimatedSprite ticker";
            }

            @Override
            public void act(float delta) {
                // Exit early if the animation is not playing...
                if (!playing) {
                    return;
                }

                final long currentTime = System.currentTimeMillis();
                if (startTime > currentTime)
                    return;
                setVisible(true);
                long deltaMs = currentTime - startTime;

                // Exit early if it's not time for the next frame yet...
                int oldIndex = currentFrameIndex;
                H5EAnimatedSpriteTypeFrame currentFrame = animatedSpriteType.frames.get(oldIndex);
                float frameDuration = currentFrame.getDuration();
                if (frameDuration==0f) frameDuration = 1000f/(float)getEngine().getTargetTps();
                frameDuration /= speedMultiplier;
                if (deltaMs >= frameDuration) {

                    // TODO: A possible optimization here would be to calculate the
                    // total length of the animation and automatically subtract from
                    // the delta (if the delta is greater than the total animation length)
                    // so we don't have to keep going frame by frame if the animation has
                    // looped a number of times already.
                    //
                    // Now calculate what the next frame will be...
                    int newIndex = currentFrameIndex;
                    if (deltaMs >= frameDuration) {
                        int nFrames = (int) UPMath.floor(deltaMs / frameDuration);
                        deltaMs %= frameDuration;

                        if (currentFrameIndex >= animatedSpriteType.frames.size() - nFrames) {
                            if (looped) {
                                newIndex = 0;
                            } else {
                                stop();
                                if (hideWhenFinished) {
                                    setVisible(false);
                                }
                                return;
                            }

                        } else {
                            newIndex += nFrames;
                        }
                    }

                    // Now make the startTime equal what would be the start of the current
                    // frame; subtracting the remaining delta from the current time ...
                    startTime = currentTime - deltaMs;

                    activateFrame(newIndex);
                }
                return;
            }
        };

    }

    public void activateFrame(int index) {
        if (index < 0 || index >= animatedSpriteType.frames.size() || index == currentFrameIndex) {
            return;
        }
        H5EAnimatedSpriteTypeFrame newFrame = animatedSpriteType.frames.get(index);
//        if (currentFrameIndex > -1 && currentFrameIndex < animatedSpriteType.frames.size()) {
//            H5EAnimatedSpriteTypeFrame oldFrame = animatedSpriteType.frames.get(currentFrameIndex);
//            this.scale /= (1 + oldFrame.getAdjustedScale());
//            this.setOrigin(this.getOriginX() + oldFrame.getAdjustedX(), this.getOriginY() + oldFrame.getAdjustedY());
//        }

        setSpriteType(newFrame.spriteType);
//        if (maskingCanvases != null && maskingCanvases.length > index) {
//            this.setMaskingCanvas(maskingCanvases[index]);
//        }
        this.setScale(getScaleX() * (1 + newFrame.getAdjustedScale()));
        this.setOrigin(this.getOriginX() - newFrame.getAdjustedX(), this.getOriginY() - newFrame.getAdjustedY());

        currentFrameIndex = index;
    }

    public void setHideWhenFinished(boolean value) {
        this.hideWhenFinished = value;
    }

    public boolean isHiddenWhenFinished() {
        return this.hideWhenFinished;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void play(boolean looped) {
        play(looped, 0);
    }

    public void play(boolean looped, int delay) {
        if (playing) {
            return;
        }
        if (animatedSpriteType.frames.isEmpty()) {
            return;
        }
        if (delay > 0) {
            setVisible(false);
        }

        this.playing = true;
        this.looped = looped;
        this.startTime = System.currentTimeMillis() + delay;
        activateFrame(0);
    }

    public void pause() {
        playing = false;
    }

    public void stop() {
        startTime = null;
        currentFrameIndex = 0;
        playing = false;
        activateFrame(0);
    }

    public void setAnimatedSpriteType(H5EAnimatedSpriteType newAnimation, boolean continueFromLastFrameIndex) {
        if (!continueFromLastFrameIndex) {
            stop();
        }
        animatedSpriteType = newAnimation;
        if (!continueFromLastFrameIndex && newAnimation != null && newAnimation.frames.size() > 0) {
            activateFrame(0);
        }
        if (!continueFromLastFrameIndex && playing) {
            play(looped);
        }
    }

    public H5EAnimatedSpriteType getAnimatedSpriteType() {
        return animatedSpriteType;
    }

    public void setSpeedMultiplier(float newVal) {
        this.speedMultiplier = newVal;
    }

    public float getSpeedMultiplier() {
        return speedMultiplier;
    }

}
