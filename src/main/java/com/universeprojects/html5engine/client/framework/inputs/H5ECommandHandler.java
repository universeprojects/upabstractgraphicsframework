package com.universeprojects.html5engine.client.framework.inputs;

public abstract class H5ECommandHandler {
    protected H5ECommandHandler() {

    }

    public abstract void execute(H5ECommandParams currentParams);

    public abstract boolean isValid();
}
