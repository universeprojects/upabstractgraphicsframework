package com.universeprojects.html5engine.client.framework.inputs;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class H5EControlBindings<K, B extends H5EControlBinding> {

    private Map<K, List<B>> bindingsByKey;

    H5EControlBindings() {
    }

    final void clear() {
        bindingsByKey = null;
    }

    final boolean isEmpty() {
        return bindingsByKey == null || bindingsByKey.isEmpty();
    }

    final void add(K key, B binding) {
        if (bindingsByKey == null) {
            bindingsByKey = new LinkedHashMap<>();
        }
        List<B> bindings = bindingsByKey.get(key);
        if (bindings == null) {
            bindings = new ArrayList<>(1);
            bindingsByKey.put(key, bindings);
        }
        bindings.add(binding);
    }

    final void doTick() {
        if (bindingsByKey != null) {
            for (List<B> bindings : bindingsByKey.values()) {
                for (B binding : bindings) {
                    binding.doTick();
                }
            }
        }
    }

    final void processFor(K key, BindingAction<B> action) {
        if (bindingsByKey != null && bindingsByKey.containsKey(key)) {
            for (B binding : bindingsByKey.get(key)) {
                action.apply(binding);
            }
        }
    }

    public interface BindingAction<B> {
        void apply(B binding);
    }

}
