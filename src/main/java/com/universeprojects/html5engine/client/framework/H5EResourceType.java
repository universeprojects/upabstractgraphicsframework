package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;

public enum H5EResourceType {

    FILE_VERSION("file version"),
    IMAGE("image"),
    AUDIO("audio"),
    SPRITE_TYPE("H5ESpriteType"),
    MAP_OBJECT_TYPE("H5EMapObjectType"),
    ANIMATED_SPRITE_TYPE("H5EAnimatedSpriteType"),
    SPRITE_TYPE_MARKER("H5ESpriteTypeMarker");

    private final String strForm;

    private H5EResourceType(String strForm) {
        Dev.checkNotEmpty(strForm);
        this.strForm = strForm;
    }

    @Override
    public String toString() {
        return strForm;
    }

    /**
     * @return The enum value that matches the passed string, NULL if not found
     */
    public static H5EResourceType fromString(String str) {
        if (Strings.isEmpty(str))
            return null;

        for (H5EResourceType value : values()) {
            if (value.strForm.equals(str)) {
                return value;
            }
        }

        return null;
    }

}
