package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EMouseWheelBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final int inputParameter;
    public final H5ECommandInputTranslator1Args<Integer, C> translator;

    public H5EMouseWheelBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Integer, C> translator, C command, int inputParameter) {
        super(controlSetup, command);
        this.translator = translator;
        this.inputParameter = inputParameter;
    }

    public void fire(int value) {
        H5ECommandParams<C> inp = translator.generateParams(command, value);
        fire(inp);
    }
}
