package com.universeprojects.html5engine.client.framework.drawable;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;

public class ScaledDrawable<T extends TransformDrawable> implements TransformDrawable {
    private final T drawable;
    private float scaleX = 1;
    private float scaleY = 1;

    public ScaledDrawable(T drawable, float scaleX, float scaleY) {
        this.drawable = drawable;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }

    public ScaledDrawable(T drawable, float scale) {
        this(drawable, scale, scale);
    }

    public ScaledDrawable(T drawable) {
        this(drawable, 1f, 1f);
    }

    public void setScale(float val) {
        setScaleX(val);
        setScaleY(val);
    }

    public T getDrawable() {
        return drawable;
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    @Override
    public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        drawable.draw(batch, x, y, originX, originY, width, height, this.scaleX * scaleX, this.scaleY * scaleY, rotation);
    }

    @Override
    public void draw(Batch batch, float x, float y, float width, float height) {
        drawable.draw(batch, x, y, width / 2f, height / 2f, width, height, this.scaleX, this.scaleY, 0);
    }

    @Override
    public float getLeftWidth() {
        return drawable.getLeftWidth();
    }

    @Override
    public void setLeftWidth(float leftWidth) {
        drawable.setLeftWidth(leftWidth);
    }

    @Override
    public float getRightWidth() {
        return drawable.getRightWidth();
    }

    @Override
    public void setRightWidth(float rightWidth) {
        drawable.setRightWidth(rightWidth);
    }

    @Override
    public float getTopHeight() {
        return drawable.getTopHeight();
    }

    @Override
    public void setTopHeight(float topHeight) {
        drawable.setTopHeight(topHeight);
    }

    @Override
    public float getBottomHeight() {
        return drawable.getBottomHeight();
    }

    @Override
    public void setBottomHeight(float bottomHeight) {
        drawable.setBottomHeight(bottomHeight);
    }

    @Override
    public float getMinWidth() {
        return drawable.getMinWidth();
    }

    @Override
    public void setMinWidth(float minWidth) {
        drawable.setMinWidth(minWidth);
    }

    @Override
    public float getMinHeight() {
        return drawable.getMinHeight();
    }

    @Override
    public void setMinHeight(float minHeight) {
        drawable.setMinHeight(minHeight);
    }
}
