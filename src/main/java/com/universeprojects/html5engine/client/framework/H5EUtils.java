package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.math.UPMath;


public class H5EUtils {
    public static Float distance(Float x1, Float y1, Float x2, Float y2) {
        return UPMath.sqrt(UPMath.pow(x2 - x1, 2) + UPMath.pow(y2 - y1, 2));
    }

    public static Float distance(Float x1, Float y1, Float z1, Float x2, Float y2, Float z2) {
        return UPMath.sqrt(UPMath.pow(x2 - x1, 2) + UPMath.pow(y2 - y1, 2) + UPMath.pow(z2 - z1, 2));
    }

    public static float dotProduct(float x1, float y1, float x2, float y2) {
        return (x1 * x2) + (y1 * y2);
    }
}
