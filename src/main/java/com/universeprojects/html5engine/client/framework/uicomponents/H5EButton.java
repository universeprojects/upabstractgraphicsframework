package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;


public class H5EButton extends ImageTextButton implements GraphicElement {

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public H5EButton(H5ELayer layer) {
        this("", layer, null, "default");
    }

    public H5EButton(String text, H5ELayer layer) {
        this(text, layer, null, "default");
    }

    public H5EButton(H5ELayer layer, Integer level) {
        this("", layer, level, "default");
    }

    public H5EButton(String text, H5ELayer layer, Integer level, String style) {
        super(text.toUpperCase(), layer.getEngine().getSkin(), style);
        layer.addElement(this, level);
        layer.addToTop(this);
    }

    public H5EButton(H5ELayer layer, ImageTextButtonStyle style) {
        this("", layer, style);
    }

    public H5EButton(String text, H5ELayer layer, ImageTextButtonStyle style) {
        this(text, layer, null, style);
    }

    public H5EButton(String text, H5ELayer layer, Integer level, ImageTextButtonStyle style) {
        super(text.toUpperCase(), style);
        layer.addElement(this, level);
        layer.addToTop(this);
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, H5ESpriteType spriteType) {
        return createSpriteBacked(layer, spriteType, "");
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, H5ESpriteType spriteType, String text) {
        if (spriteType == null) {
            throw new IllegalArgumentException("The given spriteType is null.");
        }
        return new H5EButton(text, layer);
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, String spriteTypeKey) {
        return createSpriteBacked(layer, spriteTypeKey, "");
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, String spriteTypeKey, String text) {
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
        if (spriteType == null) {
            throw new RuntimeException("The sprite type '" + spriteTypeKey + "' was not found.");
        }
        return createSpriteBacked(layer, spriteType, text);
    }

    @Deprecated
    public static H5EButton createRectBacked(H5ELayer layer) {
        return createRectBacked(layer, "");
    }

    @Deprecated
    public static H5EButton createRectBacked(H5ELayer layer, String text) {
        return new H5EButton(text, layer);
    }

    @Deprecated
    public static H5EButton createNoBacking(H5ELayer layer, String text) {
        return new H5EButton(text, layer);
    }

    public void addButtonListener(Callable0Args callable) {
        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                callable.call();
                event.handle();
            }
        });
    }

    public void addCheckedButtonListener(Callable0Args callable) {
        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (isChecked()) {
                    callable.call();
                }
                event.handle();
            }
        });
    }

    @Override
    public void setText(CharSequence text) {
        super.setText(text.toString().toUpperCase());
    }
}
