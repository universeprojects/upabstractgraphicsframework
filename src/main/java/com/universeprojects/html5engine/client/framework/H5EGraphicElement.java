package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.*;


@SuppressWarnings("unused")
public class H5EGraphicElement extends Actor implements GraphicElement {
    private final H5EEngine engine;

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return engine;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public H5EGraphicElement(H5ELayer layer) {
        this(layer, null);
    }

    public H5EGraphicElement(H5ELayer layer, Integer level) {
        this(layer, true, level);
    }

    private H5EGraphicElement(H5ELayer layer, boolean top, Integer level) {
        layer.addElement(this, level);
        if (top) {
            layer.addToTop(this);
        }
        engine = layer.getEngine();
    }


    /**
     * Effectively deletes the graphic element.
     * Destroys all behaviours.
     */
    public boolean remove() {
        if (hasBehaviours()) {
            for (Class behaviourClass : behaviours.keySet()) {
                H5EBehaviour behaviour = behaviours.get(behaviourClass);
                if (behaviour != null) {
                    behaviour.deactivate();
                    this.getEngine().removeBehaviour(behaviour);
                    behaviour.onDestroy();
                }
            }
            behaviours.clear();
        }
        return super.remove();
    }

    LinkedHashMap<Class<? extends H5EBehaviour>, H5EBehaviour> behaviours = null;
    public void addBehaviour(H5EBehaviour behaviour) {
        if (behaviour==null) return;
        if (behaviours == null)
            behaviours = new LinkedHashMap<>();
        else
            if (behaviours.containsKey(behaviour.getClass()))
                throw new IllegalStateException("Attempted to add "+behaviour.getClass().getSimpleName()+" more than once to the same element.");

        behaviours.put(behaviour.getClass(), behaviour);
        engine.addBehaviour(behaviour);
    }

    public void removeBehaviour(H5EBehaviour behaviour)
    {
        if (behaviour==null) return;
        if (behaviours == null) return;

        behaviours.remove(behaviour.getClass());
        engine.removeBehaviour(behaviour);
    }


    public <T extends H5EBehaviour> T getBehaviour(Class<T> behaviourClass)
    {
        if (behaviours ==null) return null;
        return (T) behaviours.get(behaviourClass);
    }

    public Collection<H5EBehaviour> getBehaviours()
    {
        if (behaviours ==null) return null;
        return behaviours.values();
    }

    public boolean hasBehaviours()
    {
        if (behaviours==null) return false;
        return behaviours.isEmpty()==false;
    }

}
