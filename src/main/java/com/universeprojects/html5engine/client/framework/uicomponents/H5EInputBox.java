package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5EInputBox extends TextField implements GraphicElement {

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public void focus() {
        getLayer().setKeyboardFocus(this);
    }

    public void unfocus() {
        getLayer().setKeyboardFocus(getLayer().getRoot());
    }

    public boolean hasFocus() {
        return getLayer().getKeyboardFocus() == this;
    }

    public H5EInputBox(H5ELayer layer) {
        this(layer, null);
    }

    public H5EInputBox(H5ELayer layer, Integer level) {
        super("", layer.getEngine().getSkin());
        layer.addElement(this, level);
        layer.addToTop(this);
    }

    public void setTypeNumber() {
        setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
    }
}
