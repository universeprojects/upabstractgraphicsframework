package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5ELayer extends Stage {

    /**
     * A reference to the engine so we can do work inside this class and can be self sufficient
     */
    protected final H5EEngine engine;

    protected final Group[] levels;

    private final int numLevels;
    private final int defaultLevel;

    private boolean eventTransparent = false;

    @SuppressWarnings("FieldCanBeLocal")
    private final InputProcessor inputProcessor = new LayerInputDelegator(this);

    protected final String name;

    public H5ELayer(H5EEngine engine, String name, Viewport viewport) {
        this(engine, name, 1, 0, viewport);
    }

    @SuppressWarnings("unchecked")
    public H5ELayer(H5EEngine engine, String name, int numLevels, int defaultLevel, Viewport viewport) {
        this.engine = Dev.checkNotNull(engine);
        this.name = name;
        getRoot().setName("Layer " + name);

        if (numLevels < 1) {
            throw new IllegalArgumentException("Invalid number of levels (must be 1 or higher): " + numLevels);
        }
        this.numLevels = numLevels;

        if (defaultLevel < 0) {
            throw new IllegalArgumentException("Invalid default level (must be 0 or higher): " + defaultLevel);
        }
        this.defaultLevel = defaultLevel;

        this.levels = new Group[numLevels];

        setViewport(viewport);

        for (int i = 0; i < numLevels; i++) {
            final Group level = createLevel();
            level.setName(level.getClass().getSimpleName() + " Level " + i);
            levels[i] = level;
            addActor(level);
        }

        engine.inputMultiplexer.addProcessor(0, inputProcessor);
    }

    protected Group createLevel() {
        return new Group();
    }

    public boolean isLevelEventTransparent(int level) {
        return levels[level].isTouchable();
    }

    public void setLevelEventTransparent(int level, boolean transparent) {
        levels[level].setTouchable(transparent ? Touchable.disabled : Touchable.enabled);
    }

    public int translateLevelName(String name) {
        throw new IllegalStateException("Not implemented");
    }

    public void render() {
        act();
        try {
            draw();
        } finally {
            if(getBatch().isDrawing()) {
                getBatch().end();
            }
        }
    }

    public String getName() {
        return name;
    }

    public int getDefaultLevel() {
        return defaultLevel;
    }

    public void setEventTransparent(boolean value) {
        this.eventTransparent = value;
    }

    public boolean isEventTransparent() {
        return eventTransparent;
    }

    protected boolean receivesInputs() {
        return !isEventTransparent() && isVisible();
    }

    /**
     * Shows the layer (sets the layer's visible property to TRUE)
     */
    public void show() {
        setVisible(true);
    }

    /**
     * Hides the layer (sets the layer's visible property to FALSE)
     */
    public void hide() {
        setVisible(false);
    }

    /**
     * Hides/Shows the layer
     *
     * @return TRUE    - if the layer is shown as a result of this operation
     * FALSE   - if the layer is hidden as a result of this operation
     */
    public boolean toggleVisibility() {
        if (isVisible()) {
            hide();
            return false;
        } else {
            show();
            return true;
        }
    }

    /**
     * Sets the layer's visible property. An invisible layer is simply not drawn.
     */
    public void setVisible(boolean newVisible) {
        if (newVisible == isVisible()) return;
        getRoot().setVisible(newVisible);
    }

    /**
     * Gets the visible state of this layer.
     */
    public boolean isVisible() {
        return getRoot().isVisible();
    }

    public void addElement(H5EGraphicElement element) {
        addElement(element, null);
    }

    /**
     * Appends the given graphic element to this layer. The layer will be marked as invalid (to be re-drawn)
     */
    public <T extends Actor & GraphicElement> void addElement(T element, Integer level) {
        Dev.checkNotNull(element);
        if (getActors().contains(element, true)) {
            throw new IllegalStateException("The layer already contains this element");
        }

        if (level == null) {
            level = defaultLevel;
        } else if (level < 0 || level > numLevels - 1) {
            throw new IllegalArgumentException("Invalid level: " + level);
        }
        element.setLevel(level);
    }

    /**
     * Moves the given element to the "foreground" within its level. This is useful, when we would like
     * to have the element render "above" all the other elements in the level.
     *
     * @throws IllegalStateException if the layer does not contain this element
     */
    public <T extends Actor & GraphicElement> void moveToForeground(T element) {
        Dev.checkNotNull(element);
        if (!getActors().contains(element, true)) {
            throw new IllegalStateException("The given element is not in this layer");
        }

        removeFromTop(element);
        addToTop(element);
    }

    /**
     * Removes the given graphic element from this layer. The layer will be marked as invalid (to be re-drawn)
     */
    public <T extends Actor & GraphicElement> void removeElement(T element) {
        getActors().removeValue(element, true);
        removeFromTop(element);
        element.setLevel(null);;
    }

    public void removeAllElements() {
        for (Group level : levels) {
            level.clear();
        }
    }

    public int getNumLevels() {
        return numLevels;
    }

    public H5EEngine getEngine() {
        return engine;
    }

    public <T extends Actor & GraphicElement> void addToTop(T el) {
        addActorToTop(el, el.getLevel());
    }

    public void addActorToTop(Actor el) {
        addActorToTop(el, defaultLevel);
    }

    public void addActorToTop(Actor el, int levelIndex) {
        final Group level = levels[levelIndex];
        if (el.getParent() == level) {
            return;
        }

        level.addActor(el);
        level.setVisible(true);
    }

    protected <T extends Actor & GraphicElement> void removeFromTop(T el) {
        levels[el.getLevel()].getChildren().removeValue(el, true);
    }

    protected <T extends Actor & GraphicElement> void moveRelativeTo(T el, H5EGraphicElement target, int rel) {
        Dev.checkNotNull(el);
        Dev.checkNotNull(target);
        if (target == el) {
            return;
        }
        Dev.check(el.getLayer() == target.getLayer(), "Can only move objects if they share the same layer and level");
        int index = el.getLevel();
        int targetLevel = target.getLevel();
        Dev.check(index == targetLevel, "Can only move objects if they share the same layer and level");
        final Group level = levels[index];
        if (target.getParent() != level) {
            return;
        }

        level.removeActor(el);
        int targetIndex = level.getChildren().indexOf(target, true);
        level.addActorAt(targetIndex + rel, el);
    }
}
