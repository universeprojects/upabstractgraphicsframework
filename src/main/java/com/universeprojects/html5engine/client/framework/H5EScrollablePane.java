package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.shared.abstractFramework.Container;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5EScrollablePane extends ScrollPane implements Container {

    private final Table content;

    private Integer level;

    public <T extends Actor> Cell<T> add(T actor) {
        return content.add(actor);
    }

    public Cell add() {
        return content.add();
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public H5EScrollablePane(H5ELayer layer) {
        this(layer, null, "default");
    }

    public H5EScrollablePane(H5ELayer layer, Integer level) {
        this(layer, level, "default");
    }

    public H5EScrollablePane(H5ELayer layer, String styleName) {
        this(layer, null, styleName);
    }

    public H5EScrollablePane(H5ELayer layer, Integer level, String styleName) {
        super(new Table(), layer.getEngine().getSkin(), styleName);
        layer.addElement(this, level);
        layer.addToTop(this);
        setFadeScrollBars(false);
        addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if(pointer == -1) {
                    layer.setScrollFocus(H5EScrollablePane.this);
                }
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if(pointer == -1) {
                    layer.setScrollFocus(null);
                }
            }
        });
        this.content = (Table) getActor();
    }

//    public void scrollBy(float power) {
//        float minY = pane.getHeight() - content.getHeight();
//        float maxY = 0;
//        content.setY(UPMath.cap(content.getY() - power * 20, minY, maxY));
//        updateSlider();
//    }

    public void scrollToPosition(float position) {
        scrollTo(0, position, content.getWidth(), getHeight()/2f);
//        float minY = pane.getHeight() - content.getHeight();
//        float maxRange = -minY;
//        if (position < 0) {
//            throw new IllegalArgumentException("Scroll position " + position + " is less than 0");
//        }
//        position = Math.min(position, maxRange);
//        content.setY(-position);
//        updateSlider();
    }

    public float getScrollPosition() {
        return getScrollY();
    }

//    private void updateSlider() {
//        float scrollRange = UPMath.capMin(content.getHeight() - pane.getHeight(), 0);
//        if (scrollRange <= 0) {
//            displaySlider = false;
//            slider.hide();
//        } else {
//            displaySlider = true;
//            if (pane.isVisible()) {
//                slider.show();
//            }
//
//            float pageProportion = (float) pane.getHeight() / (float) content.getHeight();
//            float sliderHeight = UPMath.cap(pageProportion * pane.getHeight(), 40, pane.getHeight());
//            sliderMinY = sliderHeight / 2;
//            sliderMaxY = pane.getHeight() - sliderHeight / 2;
//            Dev.check(sliderMaxY > sliderMinY);
//
//            float scrollPosition = -content.getY();
//            float scrollProportion = scrollPosition / scrollRange;
//            float sliderY = (sliderMaxY - sliderMinY) * scrollProportion + sliderMinY;
//
//            slider.setHeight((int) sliderHeight);
//            slider.setOrigin(slider.getWidth(), sliderHeight / 2);
//            slider.setY(sliderY);
//        }
//    }


//    @Override
//    public H5EGraphicElement getRootElement() {
//        return pane;
//    }

    public Table getContent() {
        return content;
    }

//    public void setWidth(int width) {
//        pane.setWidth(width);
//        content.setWidth(width);
//        updateSlider();
//    }
//
//    public void setHeight(int height) {
//        pane.setHeight(height);
//        if (content.getHeight() < height) {
//            content.setHeight(height);
//        }
//        updateSlider();
//    }
//
//    public int getWidth() {
//        return pane.getWidth();
//    }
//
//    public int getHeight() {
//        return pane.getHeight();
//    }
//
//    public void setContentHeight(int contentHeight) {
//        content.setHeight(UPMath.max(contentHeight, pane.getHeight()));
//        updateSlider();
//    }
//
//    public void setSliderColor(String color) {
//        slider.setFillColor(color);
//    }

    @Deprecated
    public <T extends Actor & GraphicElement> T addChild(T child) {
        content.add(child);
        return child;
    }

    public <T extends Actor & GraphicElement> void removeChild(T child) {
        content.removeActor(child);
    }

}
