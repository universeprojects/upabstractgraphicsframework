package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPMath;

public class H5EJoystickState {

    private final float x;
    private final float y;

    private final float angle;


    protected H5EJoystickState(float x, float y) {
        this.x = x;
        this.y = y;

        float a = (float) (UPMath.toDegrees(UPMath.atan2(y, x)) + 90);
        while (a < 0) {
            a += 360;
        }
        this.angle = a;

    }

    public boolean isRight() {
        return isInGeneralDirection(0);
    }

    public boolean isDown() {
        return isInGeneralDirection(90);
    }

    public boolean isLeft() {
        return isInGeneralDirection(180);
    }

    public boolean isUp() {
        return isInGeneralDirection(270);
    }

    private boolean isInGeneralDirection(float dirAngle) {
        if (getPower() < 30) {
            // ignore small displacements
            return false;
        }

        float lower = dirAngle - 55;
        if (lower < 0) {
            lower += 360;
        }

        float upper = dirAngle + 55;
        if (upper >= 360) {
            upper -= 360;
        }

        if (lower < upper) {
            return (lower < angle) && (angle < upper);
        } else {
            return ((lower < angle) && (angle < 360)) ||
                ((0 <= angle) && (angle < upper));
        }

    }

    public float getAngle() {
        return angle;
    }

    public float getPower() {
        return (float) UPMath.sqrt(x * x + y * y);
    }

    public float getPowerX() {
        return (float) UPMath.abs(x);
    }

    public float getPowerY() {
        return (float) UPMath.abs(y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Vector2 getVector() {
        return new Vector2(x, y);
    }

}
