package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Pools;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.shared.abstractFramework.Sprite;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;
import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class H5ESprite extends H5EGraphicElement implements Sprite {

    private static final float FACTOR_3D_SHIFT = 20f;

    /**
     * The sprite type being used
     */
    protected H5ESpriteType spriteType;

    private BlendingMode blendingMode = BlendingMode.DEFAULT;

    private float shift3DZ = 0;

    private float areaXAdjust = 0;
    private float areaYAdjust = 0;
    private float areaWidthAdjust = 0;
    private float areaHeightAdjust = 0;
    private CompositeSpriteConfiguration compositeSpriteConfiguration;

    private boolean revertViewportZoomScaling = false;
    private boolean revertViewportRotation = false;

    protected H5ESprite(H5ELayer layer, Integer level) {
        super(layer, level);
    }

    public H5ESprite(H5ELayer layer, H5ESpriteType spriteType) {
        this(layer, spriteType, null);
    }

    public H5ESprite(H5ELayer layer, String spriteTypeKey) {
        this(layer, spriteTypeKey, null);
    }

    public H5ESprite(H5ELayer layer, H5ESpriteType spriteType, Integer level) {
        super(layer, level);
        if (spriteType == null) {
            throw new IllegalArgumentException("Sprite type reference can't be null");
        }
        this.spriteType = spriteType;
        setWidth(spriteType.getWidth());
        setHeight(spriteType.getHeight());
    }

    public H5ESprite(H5ELayer layer, String spriteTypeKey, Integer level) {
        super(layer, level);
        if (Strings.isEmpty(spriteTypeKey)) {
            throw new IllegalArgumentException("Sprite type key can't be null or empty");
        }

        H5ESpriteType tmpSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
        if (tmpSpriteType == null) {
            Log.error("Could not obtain sprite type for key: " + spriteTypeKey);
        }
        this.spriteType = tmpSpriteType;
        if (spriteType != null) {
            setWidth(spriteType.getWidth());
            setHeight(spriteType.getHeight());
        }
    }

    @Override
    public void setAreaAdjustments(float areaXAdjust, float areaYAdjust, float areaWidthAdjust, float areaHeightAdjust) {
        if (this.areaXAdjust == areaXAdjust && this.areaYAdjust == areaYAdjust
            && this.areaWidthAdjust == areaWidthAdjust && this.areaHeightAdjust == areaHeightAdjust)
            return;
        this.areaXAdjust = areaXAdjust;
        this.areaYAdjust = areaYAdjust;
        this.areaWidthAdjust = areaWidthAdjust;
        this.areaHeightAdjust = areaHeightAdjust;
        checkAreaOverrideBounds();
    }

    public BlendingMode getBlendingMode() {
        return blendingMode;
    }

    public void setBlendingMode(BlendingMode blendingMode) {
        this.blendingMode = blendingMode;
    }

    public float getZ()
    {
        return shift3DZ;
    }

    public void setZ(float new3DShiftZValue)
    {
        this.shift3DZ = new3DShiftZValue;
    }

    @Override
    public void setX(float x)
    {
        super.setX(x);
//        super.setX(x - getOriginX());
    }

    @Override
    public void setY(float y)
    {
        super.setY(y);
//        super.setY(y - getOriginY());
    }

    @Override
    public float getX()
    {
        return super.getX();
//        return super.getX()+getOriginX();
    }

    public float getY()
    {
        return super.getY();
//        return super.getY()+getOriginY();
    }

    public void setPositionLikeANormalPerson(float x, float y)
    {
        setX(x + getOriginX());
        setY(y + getOriginY());
    }

    public float getPositionXLikeANormalPerson()
    {
        return getX()-getOriginX();
    }

    public float getPositionYLikeANormalPerson()
    {
        return getY()-getOriginY();
    }

    /**
     * This method will change the spriteType the sprite is using. The next time
     * this sprite is drawn; the new spriteType will be displayed.
     *
     * @param newSpriteType The sprite type to be used for the graphical data
     */
    @Override
    public void setSpriteType(SpriteType newSpriteType) {
        if (spriteType == newSpriteType)
            return;

        spriteType = (H5ESpriteType) newSpriteType;
        checkAreaOverrideBounds();
    }

    /**
     * This method simply retrieves the spriteType being used to draw this sprite.
     *
     * @return The current H5ESpriteType that is being used for this sprite's image data
     */
    @Override
    public H5ESpriteType getSpriteType() {
        return spriteType;
    }

    /**
     * Returns the width for this sprite. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     *
     * @return The width of the sprite as it is drawn on the layer
     */
    @Override
    public float getWidth() {
        if (spriteType == null)
            return 0;
        else
            return spriteType.getWidth();
    }

    @Override
    public float getHeight() {
        if (spriteType == null)
            return 0;
        else
            return spriteType.getHeight();
    }

    private void checkAreaOverrideBounds() {
        if (areaXAdjust == 0 && areaYAdjust == 0 && areaWidthAdjust == 0 && areaHeightAdjust == 0)
            return;
        spriteType.stabilize();
        float sourceImageX = spriteType.getAreaX();
        float sourceImageY = spriteType.getAreaY();
        float sourceImageWidth = spriteType.getAreaWidth();
        float sourceImageHeight = spriteType.getAreaHeight();
        float maxX = sourceImageX + sourceImageWidth;
        float maxY = sourceImageY + sourceImageHeight;

        sourceImageX += areaXAdjust;
        sourceImageY += areaYAdjust;
        sourceImageWidth += areaWidthAdjust;
        sourceImageHeight += areaHeightAdjust;
        if (sourceImageX + sourceImageWidth > maxX)
            areaWidthAdjust = maxX - sourceImageX - spriteType.getAreaWidth();
        if (sourceImageY + sourceImageHeight > maxY)
            areaHeightAdjust = maxY - sourceImageY - spriteType.getAreaHeight();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (spriteType == null) {
            return;
        }

        Object graphicData = spriteType.getGraphicData();
        if (graphicData == null)
            return;
        spriteType.stabilize();
        final TextureRegion textureRegion = getSpriteType().getTextureRegion();
        int sourceImageX = (int) (textureRegion.getRegionX() + areaXAdjust);
        int sourceImageY = (int) (textureRegion.getRegionY() + areaYAdjust);
        int sourceImageWidth = (int) (textureRegion.getRegionWidth() + areaWidthAdjust);
        int sourceImageHeight = (int) (textureRegion.getRegionHeight() + areaHeightAdjust);

        if (sourceImageWidth <= 0 || sourceImageHeight <= 0) return;

        BlendingMode oldMode = BlendingMode.fromBatch(batch);

        blendingMode.applyToBatch(batch);

        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        float scaleX = getScaleX();
        float scaleY = getScaleY();

        if (revertViewportZoomScaling && isInGameLayer()) {
            scaleX *= getEngine().getGameZoom();
            scaleY *= getEngine().getGameZoom();
        }

        float rotation = getRotation();
        if(revertViewportRotation && isInGameLayer()) {
            rotation -= getEngine().getRotation();
        }

        // Calculate 3D shift if applicable
        float shiftX = 0;
        float shiftY = 0;
        if (shift3DZ!=0)
        {
            OrthographicCamera camera = getEngine().getGameCamera();
            float shiftFactor = (1 / (FACTOR_3D_SHIFT * (camera.zoom+5))) * shift3DZ;

            Vector2 coords = Pools.get(Vector2.class).obtain();
            coords.x = getOriginX();
            coords.y = getOriginY();
            // It's possible we could avoid doing these transformations by using the batch's matricies somehow?
            localToStageCoordinates(coords);
            getStage().stageToScreenCoordinates(coords);


            coords.x = (coords.x - camera.viewportWidth/2) * shiftFactor;
            coords.y = (coords.y - camera.viewportHeight/2) * shiftFactor;

            coords.rotate(getEngine().getRotation());

            shiftX = coords.x;
            shiftY = coords.y;

            Pools.free(coords);
        }


        batch.draw(textureRegion.getTexture(),
            getX() + shiftX, getY() - shiftY,
            getOriginX(), getOriginY(),
            getWidth(), getHeight(),
            scaleX, scaleY,
            rotation,
            sourceImageX,
            sourceImageY,
            sourceImageWidth,
            sourceImageHeight,
            false,
            false);

        if (compositeSpriteConfiguration != null) {
            JSONObject config = compositeSpriteConfiguration.getFinalConfiguration();
            JSONArray layerDefinitions = (JSONArray) config.get("layerDefinitions");

            if (layerDefinitions.isEmpty())
                return;

            JSONObject primarySpriteJson = (JSONObject) layerDefinitions.get(0);
            String primarySpriteTypeKey = (String) primarySpriteJson.get("spriteTypeKey");
            H5ESpriteType primarySpriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(primarySpriteTypeKey);
            if (primarySpriteType == null) throw new IllegalStateException("Sprite type '" + primarySpriteTypeKey + "' could not be found.");

            // Go through each of the layers in the raw json layer definition and generate child graphic elements from it
            for (Object layerDefinitionObj : layerDefinitions) {
                JSONObject layerDefinition = (JSONObject) layerDefinitionObj;
                String layerType = (String) layerDefinition.get("type");

                if (layerType.equals("Sprite")) {
                    drawLayerTypeSprite(batch, layerDefinition, scaleX, scaleY);
                }
                //TODO: Handle color layer types
                else
                    throw new IllegalArgumentException("Unhandled type: " + layerType);

            }
        }

//            setBlendingModeOnContext(context);
//
//            if (motionBlur && oldTransformations.size() == 5) {
//                float factor = 0f;
//                for (int i = 0; i < 3; i++) {
//                    Matrix4 tf = oldTransformations.get(i);
//                    batch.setTransformMatrix(tf);
//                    factor += 0.10;
//
//                    context.setGlobalAlpha(factor * transparency);
//                    drawImage(context,
//                        spriteType,
//                        sourceImageX,
//                        sourceImageY,
//                        sourceImageWidth,
//                        sourceImageHeight);
//                }
//                Matrix4 tf = oldTransformations.getLast();
//                tf.applyToContext(context);
//                context.setGlobalAlpha(factor * transparency);
//                batch.draw(spriteType.getGraphicData(),
//                    getX(), getY(),
//                    getOriginX(), getOriginY(),
//                    getWidth(), getHeight(),
//                    getScaleX(), getScaleY(),
//                    getRotation());
//                drawImage(context,
//                    spriteType,
//                    sourceImageX,
//                    sourceImageY,
//                    sourceImageWidth,
//                    sourceImageHeight);
//                oldTransformations.get(3).applyToContext(context);
//
//            }
//            //if (transparency < 1) {
//            context.setGlobalAlpha(transparency);
//            //}
//
//            if (shadowColor != null) {
//                context.setShadowColor(shadowColor);
//            }
//            if (shadowBlur != 0) {
//                context.setShadowBlur(shadowBlur);
//            }
//
//            // draw the image for the first time
//            drawImage(context,
//                spriteType,
//                sourceImageX,
//                sourceImageY,
//                sourceImageWidth,
//                sourceImageHeight);
//
//
//            if (highlight > 0) {
//                // If we're using the highlight feature, we need to draw the image
//                // again with the 'lighter' composite option...
//                context.setGlobalCompositeOperation("lighter");
//                context.setGlobalAlpha(highlight);
//
//                drawImage(context,
//                    spriteType,
//                    sourceImageX,
//                    sourceImageY,
//                    sourceImageWidth,
//                    sourceImageHeight);
//            }
//
//            if (showMarkers) {
//                context.setStrokeStyle("red");
//                for (Marker m : getSpriteType().getMarkers()) {
//                    if (m instanceof H5EDrawableMarker) {
//                        ((H5EDrawableMarker) m).drawSelfTo(context);
//                    }
//                }
//            }
        oldMode.applyToBatch(batch);
    }

    protected boolean isInGameLayer() {
        return getStage().getViewport() == getEngine().getGameViewport();
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        if (touchable && this.getTouchable() != Touchable.enabled) return null;
        if(revertViewportZoomScaling && isInGameLayer()) {
            final Vector2 vec = Pools.obtain(Vector2.class);
            vec.set(x, y);
            localToParentCoordinates(vec);
            parentToLocalCoordinatesWithRevertedZoomScaling(vec);
            x = vec.x;
            y = vec.y;
            Pools.free(vec);
        }
        return super.hit(x, y, touchable);
    }

    private Vector2 parentToLocalCoordinatesWithRevertedZoomScaling(Vector2 parentCoords) {
        final float rotation = getRotation();
        final float scaleX = getScaleX() * getEngine().getGameZoom();
        final float scaleY = getScaleY() * getEngine().getGameZoom();
        final float childX = getX();
        final float childY = getY();
        final float originX = this.getOriginX();
        final float originY = this.getOriginY();
        if (rotation == 0) {
            if (scaleX == 1 && scaleY == 1) {
                parentCoords.x -= childX;
                parentCoords.y -= childY;
            } else {

                parentCoords.x = (parentCoords.x - childX - originX) / scaleX + originX;
                parentCoords.y = (parentCoords.y - childY - originY) / scaleY + originY;
            }
        } else {
            final float cos = (float) Math.cos(rotation * MathUtils.degreesToRadians);
            final float sin = (float) Math.sin(rotation * MathUtils.degreesToRadians);
            final float tox = parentCoords.x - childX - originX;
            final float toy = parentCoords.y - childY - originY;
            parentCoords.x = (tox * cos + toy * sin) / scaleX + originX;
            parentCoords.y = (tox * -sin + toy * cos) / scaleY + originY;
        }
        return parentCoords;
    }

    private Map<String, H5ESpriteType> layerSpriteTypes;

    private void drawLayerTypeSprite(Batch batch, JSONObject layerDefinition, float scaleX, float scaleY) {
        // Get the sprite type
        String spriteTypeKey = (String) layerDefinition.get("spriteTypeKey");
        if (layerSpriteTypes == null) {
            layerSpriteTypes = new HashMap<>();
        }
        H5ESpriteType spriteType = layerSpriteTypes.get(spriteTypeKey);
        if (spriteType == null) {
            spriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(spriteTypeKey);
            if (spriteType == null) throw new IllegalStateException("Sprite type '" + spriteTypeKey + "' could not be found.");
            layerSpriteTypes.put(spriteTypeKey, spriteType);
        }

        // Get the parameters for this layer
        Number transparency = (Number) layerDefinition.get("transparency");
        if (transparency == null) transparency = 1f;
        BlendingMode blendingMode = BlendingMode.DEFAULT;
        if (layerDefinition.get("blendingMode") != null)
            blendingMode = BlendingMode.valueOf((String) layerDefinition.get("blendingMode"));

        blendingMode.applyToBatch(batch);
        batch.setColor(Color.WHITE);
        batch.getColor().a = transparency.floatValue();

        final TextureRegion textureRegion = spriteType.getTextureRegion();

        final float width = getWidth();
        final float height = getHeight();
        int sourceImageWidth = (int) Math.min(textureRegion.getRegionWidth(), width);
        int sourceImageHeight = (int) Math.min(textureRegion.getRegionHeight(), height);


        float rotation = getRotation();
        if(revertViewportRotation) {
            rotation -= getEngine().getRotation();
        }

        batch.draw(textureRegion.getTexture(),
            getX(), getY(),
            getOriginX(), getOriginY(),
            width, height,
            scaleX, scaleY,
            rotation,
            textureRegion.getRegionX(),
            textureRegion.getRegionY(),
            sourceImageWidth,
            sourceImageHeight,
            false,
            false);
    }

    public CompositeSpriteConfiguration getCompositeSpriteConfiguration() {
        return compositeSpriteConfiguration;
    }

    public void setCompositeSpriteConfiguration(CompositeSpriteConfiguration compositeSpriteConfiguration) {
        this.compositeSpriteConfiguration = compositeSpriteConfiguration;
    }

    public boolean isRevertViewportZoomScaling() {
        return revertViewportZoomScaling;
    }

    public void setRevertViewportZoomScaling(boolean revertViewportZoomScaling) {
        this.revertViewportZoomScaling = revertViewportZoomScaling;
    }

    public boolean isRevertViewportRotation() {
        return revertViewportRotation;
    }

    public void setRevertViewportRotation(boolean revertViewportRotation) {
        this.revertViewportRotation = revertViewportRotation;
    }
}
