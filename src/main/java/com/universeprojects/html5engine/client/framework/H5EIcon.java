package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

/**
 * @author Crokoking
 */
public class H5EIcon extends Image implements GraphicElement {

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public static H5EIcon fromStyle(H5ELayer layer, String styleName, Scaling scaling, int align) {
        Drawable drawable = layer.getEngine().getSkin().getDrawable(styleName);
        return new H5EIcon(layer, drawable, scaling, align);
    }

    public static H5EIcon fromStyle(H5ELayer layer, String styleName) {
        return fromStyle(layer, styleName, Scaling.stretch, Align.center);
    }

    public static H5EIcon fromSpriteType(H5ELayer layer, String key, Scaling scaling, int align) {
        H5ESpriteType spriteType = (H5ESpriteType) layer.engine.getResourceManager().getSpriteType(key);
        if(spriteType == null) {
            throw new IllegalStateException("Unable to find sprite-type "+key);
        }
        return new H5EIcon(layer, new TextureRegionDrawable(spriteType.getGraphicData()), scaling, align);
    }

    public static H5EIcon fromSpriteType(H5ELayer layer, String key) {
        return fromSpriteType(layer, key, Scaling.stretch, Align.center);
    }

    public H5EIcon(H5ELayer layer, H5ESpriteType spriteType) {
        this(layer, new TextureRegionDrawable(spriteType.getGraphicData()), Scaling.stretch, Align.center);
    }

    public H5EIcon(H5ELayer layer, Drawable drawable) {
        this(layer, drawable, Scaling.stretch, Align.center);
    }

    public H5EIcon(H5ELayer layer, Drawable drawable, Scaling scaling, int align) {
        super(drawable, scaling, align);
        layer.addElement(this, level);
        setStage(layer);
    }
}
