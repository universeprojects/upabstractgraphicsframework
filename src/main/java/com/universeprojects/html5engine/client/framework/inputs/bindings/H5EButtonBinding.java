package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class H5EButtonBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final H5EButton button;
    private final H5ECommandInputTranslator1Args<Boolean, C> translator;

    public H5EButtonBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Boolean, C> translator, C command, H5EButton button) {
        super(controlSetup, command);
        this.button = button;
        this.translator = translator;
    }

    public void fire(boolean on) {
        H5ECommandParams<C> params = translator.generateParams(command, on);
        fire(params);
    }
}
