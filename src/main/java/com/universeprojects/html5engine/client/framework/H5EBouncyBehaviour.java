package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.math.UPMath;

public class H5EBouncyBehaviour extends H5EBehaviour {
    //Operating variables...
    /**
     * An internal variable keeping track of the current X speed
     */
    private float speedX = 0;
    /**
     * An internal variable keeping track of the current Y speed
     */
    private float speedY = 0;
    /**
     * An internal variable keeping track of the current rotation speed
     */
    private float speedRotation = 0;
    /**
     * An internal variable keeping track of the current scale speed
     */
    private float speedScale = 0;

    /**
     * An internal variable keeping track of the desired X destination
     */
    private Float destinationX;
    /**
     * An internal variable keeping track of the desired Y destination
     */
    private Float destinationY;
    /**
     * An internal variable keeping track of the desired rotation destination
     */
    private Float destinationRotation;
    /**
     * An internal variable keeping track of the desired scale destination
     */
    private Float destinationScale;

    /**
     * This defines how fast a the animation will take place overall. A standard value is 0.03
     */
    private float speed_scale = 0.03f;
    /**
     * This defines the bounce's friction. A higher friction will cause the bouncing to last only a short time while a smaller friction will cause the bouncing to last much longer. Generally, a value from 0.8 to 0.9 will give a good friction.
     */
    private float friction = 0.83f;

    public <T extends H5EGraphicElement> H5EBouncyBehaviour(T graphicElement) {
        super(graphicElement);
        destinationX = graphicElement.getX();
        destinationY = graphicElement.getY();
        destinationRotation = graphicElement.getRotation();
        destinationScale = graphicElement.getScaleX();
    }

    /**
     * This defines how fast a the animation will take place overall. A standard value is 0.03
     */
    public void setSpeedMultiplier(float value) {
        speed_scale = value;

    }

    /**
     * This defines the bounce's friction. A higher friction will cause the bouncing to last only a short
     * time while a smaller friction will cause the bouncing to last much longer.
     * <p>
     * Generally, a value from 0.8 to 0.9 will give a good friction.
     **/
    public void setFriction(float friction) {
        this.friction = friction;
    }

    /**
     * Set the desired position of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired spot.
     *
     * @param x The X coordinate that the sprite is to animate to
     * @param y The Y coordinate that the sprite is to animate to
     */
    public void setPosition(Float x, Float y) {
        destinationX = x;
        destinationY = y;
    }

    public Float getDestinationX() {
        return destinationX;
    }

    public Float getDestinationY() {
        return destinationY;
    }


    /**
     * Set the desired rotation of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired angle.
     *
     * @param newRot The rotation (in degrees; 0-360) that the sprite is to animate to
     */
    public void setRotation(Float newRot) {
        destinationRotation = newRot;
    }

    public Float getDestinationRotation() {
        return destinationRotation;
    }


    /**
     * Set the desired scale of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired size.
     *
     * @param newScale The scale (1 = 100%, 0.5 = 50%) that the sprite is to animate to
     */
    public void setScale(Float newScale) {
        destinationScale = newScale;
    }

    public Float getDestinationScale() {
        return destinationScale;
    }

    @Override
    public void act(float delta) {
        // Bouncy X...
        if (destinationX != element.getX() || speedX != 0) {
            float dist_x = (destinationX - element.getX()) * speed_scale;
            speedX += dist_x;
            speedX *= friction;
            element.setX(element.getX() + speedX);
            if (destinationX == UPMath.round(element.getX()) && speedX < speed_scale && speedX > -speed_scale) {
                element.setX(destinationX);
                speedX = 0;
            }
        }
        // Bouncy Y...
        if (destinationY != element.getY() || speedY != 0) {
            float dist_y = (destinationY - element.getY()) * speed_scale;
            speedY += dist_y;
            speedY *= friction;
            element.setY(element.getY() + speedY);
            if (destinationY == UPMath.round(element.getY()) && speedY < speed_scale && speedY > -speed_scale) {
                element.setY(destinationY);
                speedY = 0;
            }
        }

        if (element instanceof H5ESprite) {
            H5ESprite sprite = (H5ESprite) element;

            // Bouncy rotation...
            if (destinationRotation != sprite.getRotation() || speedRotation != 0) {
                float dist_rotation = (destinationRotation - sprite.getRotation()) * speed_scale;
                speedRotation += dist_rotation;
                speedRotation *= friction;
                sprite.setRotation(sprite.getRotation() + speedRotation);
                if (destinationRotation == UPMath.round(sprite.getRotation()) && speedRotation < speed_scale && speedRotation > -speed_scale) {
                    sprite.setRotation(destinationRotation);
                    speedRotation = 0;
                }
            }
            // Bouncy scale...
            if (destinationScale != sprite.getScaleX() || speedScale != 0) {
                float dist_scale = (destinationScale - sprite.getScaleX()) * speed_scale;
                speedScale += dist_scale;
                speedScale *= friction;

                float newScale = sprite.getScaleX() + speedScale;
                if (newScale < 0) newScale = 0;

                sprite.setScale(newScale);
                if (sprite.getScaleX() > destinationScale - speed_scale / 10 && sprite.getScaleX() < destinationScale + speed_scale / 10 && speedScale < speed_scale && speedScale > -speed_scale) {
                    sprite.setScale(destinationScale);
                    speedScale = 0;
                }
            }
        }
        return;
    }

    @Override
    public String getName() {
        return "bouncy";
    }


}
