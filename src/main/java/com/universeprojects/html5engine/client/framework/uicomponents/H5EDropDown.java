package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.List;

public class H5EDropDown extends SelectBox implements GraphicElement {
	private Integer level;

	public H5EDropDown (List<String> options, H5ELayer layer) {
		this(options, layer, null);
	}

	public H5EDropDown (H5ELayer layer) {
		this(null, layer, null);
	}

	public H5EDropDown (List<String> options, H5ELayer layer, Integer level) {
		super(layer.getEngine().getSkin());
		layer.addElement(this, level);
		this.setItems(options.toArray());
	}

	@Override
	public H5ELayer getLayer() {
		return (H5ELayer)getStage();
	}

	@Override
	public H5EEngine getEngine() {
		return getLayer().getEngine();
	}

	@Override
	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	public int getLevel() {
		if (level == null) return getLayer().getDefaultLevel();
		return level;
	}

	public void focus() {
		getLayer().setKeyboardFocus(this);
	}

	public void unfocus() {
		getLayer().setKeyboardFocus(getLayer().getRoot());
	}

	public boolean hasFocus() {
		return getLayer().getKeyboardFocus() == this;
	}

	public void addDropDownHandler(Callable1Args callable) {
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (isVisible()) {
					callable.call(getSelected());
				}

				event.handle();
			}
		});
	}
}
