package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.universeprojects.html5engine.shared.abstractFramework.Container;

/**
 * This element has no rendering of its own, it is solely for the purpose of grouping other elements
 */
public class H5EStack extends Stack implements Container {

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public H5EStack(H5ELayer layer) {
        this(layer, null);
    }

    public H5EStack(H5ELayer layer, Integer level) {
        layer.addElement(this, level);
        layer.addToTop(this);
    }
}
