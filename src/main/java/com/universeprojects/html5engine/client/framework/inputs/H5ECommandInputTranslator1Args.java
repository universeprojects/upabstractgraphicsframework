package com.universeprojects.html5engine.client.framework.inputs;

public interface H5ECommandInputTranslator1Args<A1, C extends H5ECommand> extends H5ECommandInputTranslator {
    H5ECommandParams<C> generateParams(C command, A1 arg1);
}
