package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.math.UPMath;

public class H5EConstantMotionBehaviour extends H5EBehaviour {
    //Operating variables...
    /**
     * An internal variable keeping track of the current X speed
     */
    private float speedX = 0;
    /**
     * An internal variable keeping track of the current Y speed
     */
    private float speedY = 0;
    /**
     * An internal variable keeping track of the current rotation speed
     */
    private float speedRotation = 0;
    /**
     * An internal variable keeping track of the current scale speed
     */
    private float speedScale = 0;

    public <T extends H5EGraphicElement> H5EConstantMotionBehaviour(T graphicElement) {
        super(graphicElement);
    }

    public void setTranslateSpeed(Float x, Float y) {
        speedX = x;
        speedY = y;
    }

    public Float getTranslateSpeedX() {
        return speedX;
    }

    public Float getTranslateSpeedY() {
        return speedY;
    }


    public void setRotationSpeed(Float newRot) {
        speedRotation = newRot;
    }

    public Float getRotationSpeed() {
        return speedRotation;
    }


    public void setScaleSpeed(Float newScale) {
        speedScale = newScale;
    }

    public Float getScaleSpeed() {
        return speedScale;
    }

    @Override
    public void act(float delta) {
        if (speedX != 0) {
            element.setX(element.getX()+speedX);
        }
        if (speedY != 0) {
            element.setY(element.getY()+speedX);
        }

        if (element instanceof H5ESprite)
        {
            H5ESprite sprite = (H5ESprite) element;

            if (speedRotation != 0) {
                sprite.setRotation(sprite.getRotation()+speedRotation);
            }
            if (speedScale != 0) {
                sprite.setScale(sprite.getScaleX()+speedScale, sprite.getScaleY()+speedScale);
            }
        }
        return;
    }

    @Override
    public String getName() {
        return "ConstantMotion";
    }


}
