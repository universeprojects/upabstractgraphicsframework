package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.audio.Sound;
import com.universeprojects.html5engine.shared.abstractFramework.Audio;

public class H5EAudio extends Audio {

    protected Sound sound;

    public H5EAudio(String url) {
        this.url = url;
    }

    public Sound getSound() {
        return sound;
    }

    @Override
    public String getURL() {
        return url;
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }
}