package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.shared.abstractFramework.Container;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

/**
 * This element has no rendering of its own, it is solely for the purpose of grouping other elements
 */
public class H5EContainer extends Table implements Container {

    private Integer level;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer)getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public int getLevel() {
        if (level == null) return getLayer().getDefaultLevel();
        return level;
    }

    public H5EContainer(H5ELayer layer) {
        this(layer, null);
    }

    public H5EContainer(H5ELayer layer, Integer level) {
        super(layer.getEngine().getSkin());
        layer.addElement(this, level);
        layer.addToTop(this);
    }

    public <T extends Actor & GraphicElement> T addChild(T child) {
        add(child);
        return child;
    }
}
