package com.universeprojects.html5engine.client.framework;

import java.util.Collection;

public interface ResourceLoader {
    void loadResources(Collection<H5EImage> imageFiles, Collection<H5EAudio> audioFiles);

    H5EImage loadSingleImage(String imageUrl);

    String getDefaultSpriteKey();
}
