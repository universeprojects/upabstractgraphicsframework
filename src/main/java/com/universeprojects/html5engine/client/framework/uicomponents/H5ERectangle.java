package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.drawable.ShapeDrawable;

public class H5ERectangle extends H5EContainer {

    /**
     * The radius to use for rounded corners (in pixels). If value is 0, the corners will not be rounded.
     */
    private int roundedCornerRadiusTL = 0;
    private int roundedCornerRadiusTR = 0;
    private int roundedCornerRadiusBR = 0;
    private int roundedCornerRadiusBL = 0;

    private Color fillColor;
    private Color strokeColor;
    private Integer strokeWidth;

    public H5ERectangle(H5ELayer layer) {
        this(layer, null);
    }

    public H5ERectangle(H5ELayer layer, Integer level) {
        super(layer, level);
        background(new ShapeDrawable(getColor(), ShapeRenderer.ShapeType.Filled) {
            @Override
            protected void drawShape(ShapeRenderer shapeRenderer, float x, float y, float width, float height) {
                if (fillColor != null) {
                    shapeRenderer.setColor(fillColor);
                    shapeRenderer.rect(x, y, width, height);
                }
                if (strokeColor != null && strokeWidth != null) {
                    shapeRenderer.end();
                    shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
                    shapeRenderer.setColor(strokeColor);
                    for (int i = 0; i < strokeWidth; i++) {
                        drawRect(shapeRenderer, x + i, y + i, width - 2 * i, height - 2 * i);
                    }
                    shapeRenderer.end();
                    shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                }
            }

            private void drawRect(ShapeRenderer shapeRenderer, float x, float y, float width, float height) {
                float cornerTLX = x + roundedCornerRadiusTL;
                float cornerTLY = y + height - roundedCornerRadiusTL;
                float cornerTRX = x + width - roundedCornerRadiusTR;
                float cornerTRY = y + height - roundedCornerRadiusTR;
                float cornerBLX = x + roundedCornerRadiusBL;
                float cornerBLY = y + roundedCornerRadiusBL;
                float cornerBRX = x + width - roundedCornerRadiusBR;
                float cornerBRY = y + roundedCornerRadiusBR;
                shapeRenderer.line(cornerBLX, y, cornerBRX, y);
                shapeRenderer.line(cornerTLX, y + height, cornerTRX, y + height);

                shapeRenderer.line(x, cornerBLY, x, cornerTLY);
                shapeRenderer.line(x + width, cornerBRY, x + width, cornerTRY);

//                shapeRenderer.rect(cornerBLX, cornerBLY, cornerTRX - cornerBLX, cornerTRY - cornerBLY);
                if (roundedCornerRadiusTL > 0) {
                    shapeRenderer.arc(cornerTLX, cornerTLY, roundedCornerRadiusTL, 90, 90);
                }
                if (roundedCornerRadiusTR > 0) {
                    shapeRenderer.arc(cornerTRX, cornerTRY, roundedCornerRadiusTR, 0, 90);
                }
                if (roundedCornerRadiusBL > 0) {
                    shapeRenderer.arc(cornerBLX, cornerBLY, roundedCornerRadiusBL, 180, 90);
                }
                if (roundedCornerRadiusBR > 0) {
                    shapeRenderer.arc(cornerBRX, cornerBRY, roundedCornerRadiusBR, -90, 90);
                }
            }
        });
    }

    public void setStrokeColor(Color strokeColor) {
        this.strokeColor = strokeColor;
    }

    public void setStrokeWidth(Integer strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerTopLeftRadius() {
        return roundedCornerRadiusTL;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerTopRightRadius() {
        return roundedCornerRadiusTR;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerBottomRightRadius() {
        return roundedCornerRadiusBR;
    }

    /**
     * @return The radius of rounded corners (in pixels)
     */
    public int getRoundedCornerBottomLeftRadius() {
        return roundedCornerRadiusBL;
    }

    /**
     * Sets the radius of rounded corners, in pixels (a value of 0 or greater)
     */
    public void setRoundedCornerRadius(int val) {
        setRoundedCornerRadius(val, val, val, val);
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * Sets the radius of rounded corners, in pixels (a value of 0 or greater)
     */
    public void setRoundedCornerRadius(int newTL, int newTR, int newBR, int newBL) {
        newTL = UPMath.max(newTL, 0);
        newTR = UPMath.max(newTR, 0);
        newBR = UPMath.max(newBR, 0);
        newBL = UPMath.max(newBL, 0);
        if (newTL == roundedCornerRadiusTL && newTR == roundedCornerRadiusTR && newBR == roundedCornerRadiusBR && newBL == roundedCornerRadiusBL) {
            return;
        }
        //TODO: change arc forumula above. currently draws extra lines. maybe use https://stackoverflow.com/questions/30699321/libgdx-drawing-arc-curve
//        roundedCornerRadiusTL = newTL;
//        roundedCornerRadiusTR = newTR;
//        roundedCornerRadiusBR = newBR;
//        roundedCornerRadiusBL = newBL;
    }

}
