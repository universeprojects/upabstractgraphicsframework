package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;
import com.universeprojects.html5engine.client.framework.inputs.H5EGamePad;

public abstract class H5EGamepadBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final H5EGamePad gamePad;

    protected H5EGamepadBinding(H5EControlSetup controlSetup, C command, int gamepadIndex) {
        super(controlSetup, command);

        if (gamepadIndex < H5EGamePad.getGamepadCount()) {
            this.gamePad = controlSetup.getGamePad(gamepadIndex);
        } else {
            this.gamePad = null;
        }
    }

    @Override
    public final void doTick() {
        if (gamePad != null && gamePad.isActive()) {
            doTickWhenActive();
        }
    }

    protected abstract void doTickWhenActive();

}
