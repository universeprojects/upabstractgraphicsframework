package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @author Crokoking
 */
public class MarkerRectangle extends Marker {

    public static final String markerClassString = "MarkerRectangle";
    public int width = 20;
    public int height = 20;
    public int rotation = 0;//0-360

    @Override
    public String getMarkerClassString() {
        return markerClassString;
    }

    // See TODO on parent class
    /*
    @Override
    public Object clone() throws CloneNotSupportedException {
        H5EAbstractMarkerRectangle mr = (H5EAbstractMarkerRectangle) super.clone();
        mr.width = width;
        mr.height = height;
        mr.rotation = rotation;
        return mr;
    }
    */
}
