package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @param <T> This is the type that holds the raw data for the graphics.
 */
public abstract class GraphicSource<T> {
    public abstract T getGraphicData();
}
