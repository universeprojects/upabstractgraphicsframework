package com.universeprojects.html5engine.shared.abstractFramework;

import java.util.List;

/**
 * @author Crokoking
 */
public abstract class AnimatedSpriteType {

    protected AbstractResourceManager resourceManager = null;

    public abstract int getFrameNumber();

    public abstract AnimatedSpriteTypeFrame getFrame(int index);

    /**
     * Appends a new frame to this animation using the given spriteTypeKey and returns the resulting frame for further
     * editing.
     *
     * @param spriteTypeKey    Sprite type key that is used to reference a spriteType object.
     * @param durationInMillis OPTIONAL - The duration will default to 50 milliseconds if this field is not given.
     *                         Setting this parameter will set the duration that the new frame will play (in milliseconds).
     * @return The newly created H5EAnimatedSpriteTypeFrame
     */
    public abstract AnimatedSpriteTypeFrame newFrame(String spriteTypeKey, int durationInMillis);

    /**
     * Removes the frame at the given index.
     * <p>
     * This method will throw an exception when the index is out of bounds.
     *
     * @param index The index of the frame you wish to remove
     */
    public abstract void removeFrame(int index);

    /**
     * Moves the frame at the given index to a new index (newIndex) in the frames array. If the newIndex is out of
     * bounds, it will be forced to the closest bound. If index is out of bounds, an exception will be thrown.
     *
     * @param index    The index of the frame you wish to move
     * @param newIndex The new index the frame you wish to move will now have
     */
    public abstract void reorderFrame(int index, int newIndex);

    @SuppressWarnings("rawtypes")
    public abstract List getFrames();
}
