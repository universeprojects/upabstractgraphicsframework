package com.universeprojects.html5engine.shared.abstractFramework;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPVector;

/**
 * @author Crokoking
 */
public abstract class Marker {

    public String label;
    private String typeLabel;
    public int x = 10;
    public int y = 10;

    public abstract String getMarkerClassString();


    @Override
    public String toString() {
        return label;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @return the typeLabel
     */
    public String getTypeLabel() {
        return typeLabel;
    }

    /**
     * @param typeLabel the typeLabel to set
     */
    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public UPVector getPosition(float originX, float originY, float height) {
        return new UPVector(x - originX, height - y - originY);
    }

    public Vector2 getPosition(Vector2 out, float originX, float originY, float height) {
        out.set(x - originX, height - y - originY);
        return out;
    }
}
