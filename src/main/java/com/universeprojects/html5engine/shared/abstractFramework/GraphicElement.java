package com.universeprojects.html5engine.shared.abstractFramework;

import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public interface GraphicElement {
    H5ELayer getLayer();
    H5EEngine getEngine();
    void setLevel(Integer level);
    int getLevel();

    boolean remove();

    void setVisible(boolean visible);
}
