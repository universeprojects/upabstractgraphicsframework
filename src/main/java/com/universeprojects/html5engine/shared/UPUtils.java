package com.universeprojects.html5engine.shared;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

import java.util.Comparator;

public class UPUtils {

    private static final float FACTOR_3D_SHIFT = 20f;

    @SuppressWarnings("ComparatorCombinators")
    public static Comparator<Class<?>> classComparator = (o1, o2) -> o1.getCanonicalName().compareTo(o2.getCanonicalName());

    public static boolean isServer() {
        //noinspection SimplifiableIfStatement
        if (Gdx.app == null) return true; //Test
        return Gdx.app.getType() == Application.ApplicationType.HeadlessDesktop;
    }

    public static boolean isClient() {
        return !isServer();
    }

    public static void setAlpha(Actor container, float value) {
        container.getColor().a = value;
    }

    public static void clientSideOnly() {
        if (isServer()) {
            throw new RuntimeException("This code is only allowed to be used on the client side");
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> castClass(Class<?> aClass) {
        return (Class<T>) aClass;
    }


    public static String createRandom8CharString() {
        String value = Long.toString(Double.doubleToLongBits(UPMath.random()), 32);
        return padStringTo8(value);
    }

    public static String padStringTo8(String value) {
        if (value.length() > 8) {
            return value.substring(0, 8);
        } else if (value.length() < 8) {
            return "00000000".substring(value.length()) + value;
        } else {
            return value;
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean equal(Object a, Object b) {
        if (a == b) {
            return true;
        }

        if (a == null || b == null) {
            return false;
        }

        return a.equals(b);
    }

    public static boolean isSubclassOf(Class<?> subClass, Class<?> superClass) {
        if (superClass.isInterface()) {
            //cant test interfaces in gwt
            return true;
        }
        while (!subClass.equals(Object.class)) {
            if (subClass.equals(superClass)) {
                return true;
            }
            subClass = subClass.getSuperclass();
        }
        return false;
    }

    /**
     * Class.getSimpleName() is not supported in GWT 2.5
     */
    public static String getSimpleName(Class<?> clazz) {
        Dev.checkNotNull(clazz);
        String name = clazz.getName();
        int dotIndex = name.lastIndexOf(".");
        if (dotIndex == -1) return name;
        return name.substring(dotIndex + 1);
    }

    /**
     * Class.getSimpleName() is not supported in GWT 2.5
     */
    public static String getSimpleClassName(Object obj) {
        Dev.checkNotNull(obj);
        return getSimpleName(obj.getClass());
    }

    public static void strokeRect(ShapeRenderer shapeRenderer, int stroke, int x, int y, int width, int height) {
        shapeRenderer.rect(x, y, width, stroke);
        shapeRenderer.rect(x, y + stroke, stroke, height - stroke);
        shapeRenderer.rect(x + stroke, y + height - stroke, width - stroke, stroke);
        shapeRenderer.rect(x + width - stroke, y + stroke, stroke, height - 2 * stroke);
    }

    public static UPVector toUPVector(Vector2 location) {
        return new UPVector(location.x, location.y);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static Vector2 calculate3DShift(OrthographicCamera camera, float targetX, float targetY, float zIndex, Vector2 target) {
        float shiftFactor = (1 / (FACTOR_3D_SHIFT * camera.zoom)) * zIndex;
        float shiftX = (targetX - camera.position.x) * shiftFactor;
        float shiftY = (targetY - camera.position.y) * shiftFactor;

        return target.set(shiftX, shiftY);
    }

    public static void tieButtonToKey(H5EInputBox txt, int key, H5EButton btn) {
        txt.addCaptureListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == key) {
                    Gdx.app.postRunnable(() -> {
                        btn.setChecked(true);
                        btn.setChecked(false);
                    });
                }
                return false;
            }
        });
    }

    public static void tieButtonToKey(Window window, int key, H5EButton btn) {
        window.addListener(new InputListener() {
            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (window.isVisible() && keycode == key) {
                    Gdx.app.postRunnable(() -> {
                        btn.setChecked(true);
                        btn.setChecked(false);
                    });
                }
                return false;
            }
        });
    }


    public static void positionProportionally(Actor actor, Float screenProportionX, Float screenProportionY) {
        actor.setOrigin(actor.getWidth() / 2f, actor.getHeight() / 2f);
        /*
         *	Implementation note:
         *
         *  Though H5EGraphicElement supports proportional positioning, we do not use that mechanism
         *  as it introduces undesired side-effects on the rendering of sprites that assemble the
         *
         *  More specifically, due to round-off errors "seams" between the sprites become visible.
         *  This implementation makes sure to only use int precision, and simply overrides the existing
         *  coordinates.
         */

        if (screenProportionX != null && (screenProportionX < 0 || screenProportionX > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        if (screenProportionY != null && (screenProportionY < 0 || screenProportionY > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        final H5EEngine engine = ((H5ELayer)actor.getStage()).getEngine();
        int newX = screenProportionX == null ? 0 : (int)(screenProportionX * engine.getWidth());
        int newY = screenProportionY == null ? 0 : (int)(screenProportionY * engine.getHeight());

//        // Limit the top of the dialog box to the top of the screen
//        if (screenProportionY != null) {
//            float engineHeight = getEngine().getHeight();
//            newY = Math.min((int)(engineHeight - getHeight() / 2f), (int)(screenProportionY * engineHeight));
//        }

        // override the pre-existing coordinates with the new coordinates
        actor.setX(newX, Align.center);
        actor.setY(newY, Align.center);

        int cappedY = Math.min((int)(engine.getHeight() - actor.getHeight() / 2f), (int)(actor.getY() + actor.getHeight() / 2f));
        actor.setY(cappedY, Align.center);
    }

    public static Vector2 setVector2(Vector2 vector2, UPVector upVector) {
        return vector2.set(upVector.x, upVector.y);
    }

    public static Vector2 addVector2(Vector2 vector2, UPVector upVector) {
        return vector2.add(upVector.x, upVector.y);
    }
}
