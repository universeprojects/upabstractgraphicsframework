<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comet Weather</title>
        <script type="text/javascript">
        var url = "http://localhost:8080/Html5Engine/H5EConnection";
        var request =  new XMLHttpRequest();
        request.open("POST", url, true);
        function go()
        {
            request.setRequestHeader("Content-Type","application/x-javascript;");
            request.onreadystatechange = function()
            {
                if (request.readyState == 4)
                {
                    if (request.status == 200)
                    {
                        if (request.responseText)
                        {
                            document.getElementById("forecasts").innerHTML = request.responseText;
                        }
                    }
                    go();
                }
            };
            request.send("\255\154\243\251\233 \015 \004 \003 \002 \001 \000");
            
        }
        </script>
    </head>
    <body>
        <h1>Rapid Fire Weather</h1>
        <input type="button" onclick="go()" value="Go!"></inputData>
        <div id="forecasts"></div>
    </body>
</html>
