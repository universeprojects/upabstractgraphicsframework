/**
 * Construct a new Layer object.
 * @class This is the basic Layer class.
 * Though it is not an abstract class, it can certainly be extended as is done
 * with the TileMap class.
 * @constructor
 * @param {H5EEngine} engine The engine object that this layer belongs to
 * @return A new layer
 */
function H5ELayer(engine)
{
    this._engine=engine; // A reference to the engine so we can do work inside this class and can be self sufficient
    this._sprites = new Hashmap();   // All sprites that are on this layer
    this._invalid= true;     // Is this layer inavlid? If true, it needs to be redrawn
    this._visible= true;       // is this layer visible or not?
    this._canvas = null;
    this._context = null;
    this._drawDirectMode = true;
    this._ignoreViewport = false;
    this._lastGivenUniqueId = 0;

    //Internal Events...
    this._doClick = null;       //An external component triggers this "event" for use internally
    this._doMouseMove = null;   //An external component triggers this "event" for use internally


    this.setIgnoreViewport = function(value)
    {
        this._ignoreViewport = value;
        this.invalidate();
    };

    this.getIgnoreViewport = function()
    {
        return this._ignoreViewport;

    };

    /**
     * This method will move this layer up one index in the engine so it will be
     * on top of the layer on top of it.
     */
    this.moveUp = function()
    {
        var currentIndex = this._engine.layers.indexOf(this);
        if (currentIndex<this._engine.layers.length-1)
        {
            // Swap this layer and the one in front of it...
            var temp = this._engine.layers[currentIndex+1];
            this._engine.layers[currentIndex+1] = this;
            this._engine.layers[currentIndex] = temp;
        }
    };

    /**
     * Sets the sprite's visible property to true.
     */
    this.show = function()
    {
        this.setVisible(true);
    };

    /**
     * Sets the sprite's visible property to false.
     */
    this.hide = function()
    {
        this.setVisible(false);
    };

    /**
     * Sets the sprite's visible property. An invisible sprite is simply not drawn.
     */
    this.setVisible = function(newVal)
    {
        if (newVal!=this._visible)
        {
            this._visible = newVal;
            this.invalidate();
        }
    };

    /**
     * Gets the visible state of this sprite.
     */
    this.isVisible = function()
    {
        return this._visible;
    };

    /**
     * Creates a new sprite on this layer of the sprite type that matches the given spriteTypeKey.
     */
    this.newSprite=function(spriteTypeKey)
    {
        var spriteType = engine.getResourceManager().getSpriteType(spriteTypeKey);
        if (spriteType==null)
            throw("Unable to find spriteType by the key: "+spriteTypeKey);
        var sprite = new H5ESprite(this._engine, this, spriteType);
        this.addSprite(sprite);
        return sprite;
    };

    /**
     * Removes the given sprite from this layer. The layer will be marked as invalid.
     */
    this.removeSprite=function(sprite)
    {
        this._sprites.remove(sprite.layerId);
        this.invalidate();
    };

    /**
     * Appends the given sprite object to the list of GraphicElements in this layer.
     */
    this.addSprite=function(sprite)
    {
    	this._lastGivenUniqueId++;
    	sprite.layerId = this._lastGivenUniqueId;
        this._sprites.put(sprite.layerId, sprite);
        this.invalidate();
        return sprite.layerId;
    };

    this.getSprites=function()
    {
    	return this._sprites.getValues();
    };
    
    /**
     * Returns true or false if this graphic element is invalid or not; if it needs to be
     * redrawn.
     */
    this.isInvalid=function()
    {
        return this._invalid;
    };

    /**
     * Marks this layer (as well as the parent engine) as invalid and needing to
     * be redrawn in the next graphics pass.
     */
    this.invalidate=function()
    {
        this._invalid = true;
        this._engine.invalidate();
    };

    this.drawSelfTo=function(context)
    {
        //First, ensure our canvas and the engine's main display are the same size...
        if (this._canvas==null || this._canvas.width!=this._engine._canvas.width || this._canvas.height!=this._engine._canvas.height)
        {
            if (this._canvas==null)
                this._canvas = document.createElement("canvas");
            this._canvas.width = this._engine._canvas.width;
            this._canvas.height = this._engine._canvas.height;
            this._context = this._canvas.getContext("2d");
            this._invalid=true;
        }

        if (this.isInvalid() || this._drawDirectMode==true)
        {
            if (this.isInvalid()==true)
                this._drawDirectMode=true;
            
            var ctxToUse = null;
            if (this._drawDirectMode==true && this.isInvalid()==false)
            {
                this._drawDirectMode=false;
                this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
                ctxToUse = this._context;
            }
            else
                ctxToUse = context;

            this._drawRawSelfTo(ctxToUse);

            this._invalid=false;
        }

        // Now draw our canvas 'cache' to the context given...
        if (this._drawDirectMode==false)
            context.drawImage(this._canvas, 0, 0);
    };

    this._drawRawSelfTo = function(context)
    {
        for(var spriteIndex = 0;spriteIndex<this.getSprites().length; spriteIndex++)
        {
            var sprite = this.getSprites()[spriteIndex];
            if (sprite.isVisible()==true)
            {
                sprite.drawSelfTo(context);
            }
        }
    };

    this.removeAllSprites = function()
    {
        this._sprites = new Hashmap();
        this.invalidate();
    };

}