/**
 * The H5EAnimatedSpriteType class.
 * @class The animated sprite type class contains all information necessary
 * for a frame-by-frame animation of a sprite (much like an animated gif or a movie).
 * This is meant more for defining short animations where pixel by pixel detail
 * is key. Each frame in the animation has a pxiel offset x/y as well as a duration
 * in milliseconds. It's not possible to show 2 frames at once (like in a GIF) so
 * you will need a graphics program to combine frames and use your combined image
 * instead.
 *
 * Literally, a frame's graphics comes from a H5ESpriteType. See the documentation
 * of H5ESpriteType for details there.
 *
 * @constructor 
 * @param {H5EEngine} engine The engine object that this animated sprite type belongs to
 * @return A new H5EAnimatedSpriteType
 */
function H5EAnimatedSpriteType(engine)
{
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine=engine;

    /**All frames (H5EAnimatedSpriteTypeFrame) that this animated sprite type object has @type Array*/
    this.frames = [];

    /**
     * Appends a new frame to this animation using the given spriteTypeKey and
     * returns the resulting frame for further editing.
     * @param {String} spriteTypeKey Sprite type key that is used to reference a spriteType object.
     * @param {int} durationInMillis OPTIONAL - The duration will default to 50 milliseconds if this field is not given.
     * Setting this parameter will set the duration that the new frame will play (in milliseconds).
     * @return The newly created H5EAnimatedSpriteTypeFrame
     * @type H5EAnimatedSpriteTypeFrame
     */
    this.newFrame = function(spriteTypeKey, durationInMillis)
    {
        var frame = new H5EAnimatedSpriteTypeFrame(this._engine, spriteTypeKey, durationInMillis);
        this.frames.push(frame);
        return frame;
    }

    /**
     * Moves the frame at the given index to a new index (newIndex) in the frames
     * array. If the newIndex is out of bounds, it will be forced to the closest
     * bound. If index is out of bounds, an exception will be thrown.
     * @param {int} index The index of the frame you wish to move
     * @param {int} newIndex The new index the frame you wish to move will now have
     */
    this.reorderFrame = function(index, newIndex)
    {
        if (newIndex<0) newIndex = 0;
        if (newIndex>this.frames.length-1) newIndex = this.frames.length-1;
        if (index<0 || index>this.frames.length-1)
            throw("Index out of bounds when attempting to reorder an animation frame");
        
        var frameToMove = this.frames[index];
        this.frames.splice(index, 1);
        this.frames.splice(newIndex, 0, frameToMove);
    }

    /**
     * Removes the frame at the given index.
     *
     * This method will throw an exception when the index is out of bounds.
     *
     * @param {int} index The index of the frame you wish to remove
     */
    this.removeFrame = function(index)
    {
        if (index<0 || index>this.frames.length-1)
            throw("Index out of bounds when attempting to remove an animation frame");

        this.frames.splice(index, 1);
    }

    /**
     * Saves this object to a file (the file parameter is actually just a string
     * that will be 'saved' later). This method simply adds this object's data
     * to the 'file'.
     * Most objects can be saved this way. They all use a function that looks
     * like this to add their data to the file.
     *
     * @param {String} file The 'file' data that we will be adding to.
     * @return The modified file string
     * @type String
     */
    this._saveTo = function(file)
    {
        file += "H5EAnimatedSpriteType:\n";

        // Save the sprite type key as it is in the resource manager
        var index = this._engine.getResourceManager()._animatedSpriteTypes.getValues().indexOf(this);
        var key = "";
        if (index!=-1)
            key = this._engine.getResourceManager()._animatedSpriteTypes.getKeys()[index];
        file += key+"\n";

        // Save the frame count
        file += this.frames.length+"\n";

        // Save each frame...
        for(var i = 0; i<this.frames.length; i++)
        {
            file += this.frames[i].spriteTypeKey+"\n";
            file += this.frames[i].duration+"\n";
            file += this.frames[i].adjustedX+"\n";
            file += this.frames[i].adjustedY+"\n";
            file += this.frames[i].adjustedScale+"\n";
        }

        return file;
    };



    //********************
    // Constructor area...
    //********************


}




/**
 * Creates a new H5EAnimatedSpriteType in the given resourceManager from the data
 * located in the file (lines) starting from the given line number (lineNum).
 * @param {String[]} lines The file data
 * @param {int} lineNum The current position in the file data
 * @param {H5EResourceManager} resourceManager The resource manager to create this sprite type in
 * @return The new line position in the file. The line position will always be the 'next' line
 * in the file. In other words, there is no need to increment the line number once this method returns.
 * @type int
 */
H5EAnimatedSpriteType.createFromFile = function(lines, lineNum, resourceManager)
{
    var animSpriteTypeKey = lines[lineNum];
    lineNum++;
    var frameCount = parseInt(lines[lineNum]);
    lineNum++;

    var animSpriteType = new H5EAnimatedSpriteType(resourceManager._engine);
    resourceManager.addAnimatedSpriteType(animSpriteTypeKey, animSpriteType);

    for(var i = 0; i<frameCount; i++)
    {
        var spriteTypeKey = lines[lineNum];
        lineNum++;
        var duration = parseInt(lines[lineNum]);
        lineNum++;
        var adjustedX = parseInt(lines[lineNum]);
        lineNum++;
        var adjustedY = parseInt(lines[lineNum]);
        lineNum++;
        var adjustedScale = parseFloat(lines[lineNum]);
        lineNum++;

        // Now create the frame...
        var frame = animSpriteType.newFrame(spriteTypeKey, duration);
        frame.adjustedScale = adjustedScale;
        frame.adjustedX = adjustedX;
        frame.adjustedY = adjustedY;
    }
    return lineNum;
};
