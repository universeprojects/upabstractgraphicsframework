/**
 * The H5EAnimatedSpriteTypeFrame class.
 * @class The animated sprite type frame contains a reference to a sprite type
 * as well as other settings for a single frame (like duration) for an
 * H5EAnimatdSpriteType.
 * @constructor
 * @param {H5EEngine} engine The H5EEngine object
 * @param {String} spriteTypeKey The key associagted with the sprite type (image) used for this animation frame
 * @param {int} durationInMillis The amount of time to display this frame before moving on to the next. The time is represented in milliseconds. This is not a required field.
 * @return A new H5EAnimatedSpriteTypeFrame
 */
function H5EAnimatedSpriteTypeFrame(engine, spriteTypeKey, durationInMillis)
{
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine = engine;

    if (durationInMillis==null)
        durationInMillis = 50;

    /**The sprite type key that is used to identify a sprite type object in the resource manager. @type String*/
    this.spriteTypeKey = spriteTypeKey;
    /**The sprite type that is being used for the graphics for this frame. @type H5ESpriteType*/
    this.spriteType = engine.getResourceManager().getSpriteType(spriteTypeKey);
    if (this.spriteType==null)
        throw("The spriteTypeKey '"+spriteTypeKey+"' is not associated with any sprite type");

    /**The duration that this frame will be displayed in milliseconds. @type int*/
    this.duration = durationInMillis;
    /**The pixel adjustment X that will be applied to the graphic before it is rendered. @type int*/
    this.adjustedX = 0;
    /**The pixel adjustment Y that will be applied to the graphic before it is rendered. @type int*/
    this.adjustedY = 0;
    /**The scale adjustment that will be applied to the graphic before it is rendered. @type int*/
    this.adjustedScale = 0;

    //********************
    // Constructor area...
    //********************


}