/**
 * Construct a new H5EAnimatedSpriteBehaviour object.
 * @extends H5EBehaviour
 * @class This behaviour will animate a sprite object according to an AnimatedSpriteType
 * that is passed to this behaviour after creation. Use this behaviour to control
 * the animation of a sprite.
 * @constructor
 * @param {H5ESprite} h5eSprite The sprite object that this behaviour belongs to
 * @return A new animated sprite behaviour object
 */
function H5EAnimatedSpriteBehaviour(h5eSprite, h5eAnimatedSpriteType)
{
    // Inherit from h5eBehaviour...
    this._super = H5EBehaviour;
    this._super(h5eSprite);

    //Operating variables...

    this._animatedSpriteType = h5eAnimatedSpriteType;
    this._currentFrame = 0;
    this._startTime = null;
    this.playing = false;
    this.speedMultiplier = 1;
    this.looped = true;


    this.play = function()
    {
        if (this.playing==false)
        {
            this.playing = true;
            this._startTime = new Date().getTime();
            this._element.setSpriteType(this._animatedSpriteType.frames[0].spriteType);
        }
    }

    this.pause = function()
    {
        this.playing = false;
    }

    this.stop = function()
    {
        this._startTime = null;
        this._currentFrame = 0;
        this.playing = false;
        this._element.setSpriteType(this._animatedSpriteType.frames[0].spriteType);
    }

    this.setAnimatedSpriteType = function(newAnimation, continueFromLastFrameIndex)
    {
        if (continueFromLastFrameIndex!=true)
            this.stop();
        this._animatedSpriteType = newAnimation;
        if (continueFromLastFrameIndex==false && newAnimation!=null && newAnimation.frames.length>0)
            this._element.setSpriteType(newAnimation.frames[0].spriteType);
        if (continueFromLastFrameIndex!=true && this.playing==true)
            this.play();
    };


    /**
     * Overriden update function from H5EBehaviour.
     * @see H5EBehaviour#update
     */
    this.update = function()
    {
        var lastFrame = this._currentFrame;
        // Exit early if the animation is not playing...
        if (this.playing==false)
            return;

        var currentTime = new Date().getTime();
        var delta = currentTime-this._startTime;

        // Exit early if it's not time for the next frame yet...
        if (delta<this._animatedSpriteType.frames[this._currentFrame].duration/this.speedMultiplier)
            return;

        // TODO_OLD: A possible optimization here would be to calculate the
        // total length of the animation and automatically subtract from
        // the delta (if the delta is greater than the total anim length) 
        // so we don't have to keep going frame by frame if the animation has
        // looped a number of times already.
        // 
        // Now calculate what the next frame will be...
        while(delta>=this._animatedSpriteType.frames[this._currentFrame].duration/this.speedMultiplier)
        {
            delta -= (this._animatedSpriteType.frames[this._currentFrame].duration/this.speedMultiplier);

            if (this._currentFrame+1>=this._animatedSpriteType.frames.length)
            {
                if (this.looped)
                    this._currentFrame = 0;
                else
                {
                    this.stop();
                    return;
                }

            }
            else
                this._currentFrame++;

        }

        // Now make the startTime equal what would be the start of the current
        // frame; subtracting the currentTime from the remaining delta...
        this._startTime = currentTime-delta;

        this._element.setAdjustedX(this._animatedSpriteType.frames[this._currentFrame].adjustedX);
        this._element.setAdjustedY(this._animatedSpriteType.frames[this._currentFrame].adjustedY);
        this._element.setSpriteType(this._animatedSpriteType.frames[this._currentFrame].spriteType);


    };

    /**
     * Overriden from H5EBehaviour.
     * @see H5EBehaviour#getName
     */
    this.getName = function()
    {
        return "animated sprite";
    };
    //********************
    // Constructor area...
    //********************

}