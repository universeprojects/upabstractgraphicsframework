/**
 * Construct a new H5EBouncyBehaviour object.
 * @extends H5EBehaviour
 * @class This behaviour controls the transformation of a sprite by animating
 * motion (scale, rotation and x/y translation) in such a way that it appears to
 * bounce around before arriving at its destination.
 *
 * Move the sprite using the method provided in this behaviour instead of
 * applying transformations directly to the sprite itself.
 * @constructor
 * @param {H5EGraphicElement} h5eGraphicElement The graphic element object that this behaviour belongs to
 * @return A new bouncy behaviour object
 */
function H5EBouncyBehaviour(h5eGraphicElement)
{
    // Inherit from h5eBehaviour...
    this._super = H5EBehaviour;
    this._super(h5eGraphicElement);

    //Operating variables...
    /**An internal variable keeping track of the current X speed @type float*/
    this._speedX = 0;
    /**An internal variable keeping track of the current Y speed @type float*/
    this._speedY = 0;
    /**An internal variable keeping track of the current rotation speed @type float*/
    this._speedRotation = 0;
    /**An internal variable keeping track of the current scale speed @type float*/
    this._speedScale = 0;

    /**An internal variable keeping track of the desired X destination  @type int*/
    this._destinationX = null;
    /**An internal variable keeping track of the desired Y destination  @type int*/
    this._destinationY = null;
    /**An internal variable keeping track of the desired rotation destination  @type int*/
    this._destinationRotation = null;
    /**An internal variable keeping track of the desired scale destination  @type float*/
    this._destinationScale = null;

    /**This defines how fast a the animation will take place overall. A standard value is 0.03  @type float*/
    this._speed_scale = 0.03;
    /**This defines the bounce's friction. A higher friction will cause the bouncing to last only a short time while a smaller friction will cause the bouncing to last much longer. Generally, a value from 0.8 to 0.9 will give a good friction. @type float*/
    this._friction = 0.83;

    /**
     * Set the desired position of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired spot.
     * @param {int} x The X coordinate that the sprite is to animate to
     * @param {int} y The Y coordinate that the sprite is to animate to
     */
    this.setPosition = function(x,y)
    {
        this._originalX = this._element.x;
        this._originalY = this._element.y;
        this._destinationX = x;
        this._destinationY = y;

    };


    /**
     * Set the desired rotation of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired angle.
     * @param {int} newRot The rotation (in degrees; 0-360) that the sprite is to animate to
     */
    this.setRotation = function(newRot)
    {
        this._destinationRotation = newRot;
    };

    /**
     * Set the desired scale of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired size.
     * @param {float} newScale The scale (1 = 100%, 0.5 = 50%) that the sprite is to animate to
     */
    this.setScale = function(newScale)
    {
        this._destinationScale = newScale;
    };

    /**
     * Overriden update function from H5EBehaviour.
     * @see H5EBehaviour#update
     */
    this.update = function()
    {
        // Bouncy X...
        if (this._destinationX!=this._element._x || this._speedX!=0)
        {
            var dist_x = (this._destinationX-this._element._x)*this._speed_scale;
            this._speedX+=dist_x;
            this._speedX*=this._friction;
            this._element.setX(this._element._x+this._speedX);
            if (this._destinationX==Math.round(this._element._x) && this._speedX<this._speed_scale && this._speedX>-this._speed_scale)
            {
                this._element._x = this._destinationX;
                this._speedX = 0;
            }
        }
        // Bouncy Y...
        if (this._destinationY!=this._element._y || this._speedY!=0)
        {
            var dist_y = (this._destinationY-this._element._y)*this._speed_scale;
            this._speedY+=dist_y;
            this._speedY*=this._friction;
            this._element.setY(this._element._y+this._speedY);
            if (this._destinationY==Math.round(this._element._y) && this._speedY<this._speed_scale && this._speedY>-this._speed_scale)
            {
                this._element._y = this._destinationY;
                this._speedY = 0;
            }
        }
        // Bouncy rotation...
        if (this._destinationRotation!=this._element._rotation || this._speedRotation!=0)
        {
            var dist_rotation = (this._destinationRotation-this._element._rotation)*this._speed_scale;
            this._speedRotation+=dist_rotation;
            this._speedRotation*=this._friction;
            this._element.setRotation(this._element._rotation+this._speedRotation);
            if (this._destinationRotation==Math.round(this._element._rotation) && this._speedRotation<this._speed_scale && this._speedRotation>-this._speed_scale)
            {
                this._element._rotation = this._destinationRotation;
                this._speedRotation = 0; 
            }
        }
        // Bouncy scale...
        if (this._destinationScale!=this._element._scale || this._speedScale!=0)
        {
            var dist_scale = (this._destinationScale-this._element._scale)*this._speed_scale;
            this._speedScale+=dist_scale;
            this._speedScale*=this._friction;
            this._element.setScale(this._element._scale+this._speedScale);
            if (this._element._scale>this._destinationScale-this._speed_scale/10 && this._element._scale<this._destinationScale+this._speed_scale/10 && this._speedScale<this._speed_scale && this._speedScale>-this._speed_scale)
            {
                this._element._scale = this._destinationScale;
                this._speedScale = 0;
            }
        }
    };

    /**
     * Overriden from H5EBehaviour.
     * @see H5EBehaviour#getName
     */
    this.getName = function()
    {
        return "bouncy";
    };
    //********************
    // Constructor area...
    //********************

}