/**
 * This class can be considered abstract.
 * @class This is the abstract class that is used for all visual components that are
 * created on a H5ELayer. For example, the H5ESprite object extends this class.
 * @constructor The constructor for this class should not be called directly as
 * this class can be considered abstract.
 * @param {H5EEngine} engine The engine object that this image belongs to
 * @param {H5ELayer} layer The layer this graphic element belongs to
 * @return A new image
 */
function H5EGraphicElement(engine, layer)
{
    /** A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine = engine;
    /** A reference to the layer so we can do work inside this class and can be self sufficient @type H5ELayer*/
    this._layer = layer;
    /** The x coordinate placement for this element on the layer @type int*/
    this._x = 0;
    /** The y coordinate placement for this element on the layer @type int*/
    this._y = 0;
    /** The x coordinate adjusted position for this element on the layer. This is normally only used for slight adjustments to the graphic and shouldn't be considered as part of 'actual' element placement logic. @type int*/
    this._adjustedX = 0;
    /** The y coordinate adjusted position for this element on the layer. This is normally only used for slight adjustments to the graphic and shouldn't be considered as part of 'actual' element placement logic. @type int*/
    this._adjustedY = 0;
    /**The origin of the image. This point is used for transformations. For example, when rotating, setting the origin to the middle (originX = width/2) will cause the image to rotate via it's center @type int*/
    this._originX = 0;      
    /**The origin of the image. This point is used for transformations. For example, when rotating, setting the origin to the middle (originY = height/2) will cause the image to rotate via it's center @type int*/
    this._originY = 0;
    /**An array of the H5EBehaviour objects associated with this graphic element. Behaviours are used to control aspects of the graphic element automatically. Some behaviours are designed for specific sub-classes of the graphic element. @type H5EBehaviour[]*/
    this._behaviours=[];
    /**Keeps track of whether or not this graphic element is currently visible on the layer @type boolean*/
    this._visible = true;
    /**Keeps track of whether or not this graphic element needs to be redrawn or not @type boolean */
    this._invalid = true;

    // ************** ABSTRACT METHODS...
    /**
     * Abstact method. This method must be implemented by the extending class.
     * @return The width of the graphic element on the layer
     * @type int
     */
    this.getWidth = function()
    {
        throw("The getWidth() method is not implemented.");
    };

    /**
     * Abstact method. This method must be implemented by the extending class.
     * @return The height of the graphic element on the layer
     * @type int
     */
    this.getHeight = function()
    {
        throw("The getHeight() method is not implemented.");
    };
    // *************** END of abstract methods

    /**
     * Sets the graphic element's visible property to true.
     */
    this.show = function()
    {
        this.setVisible(true);
    };

    /**
     * Sets the graphic element's visible property to false.
     */
    this.hide = function()
    {
        this.setVisible(false);
    };

    /**
     * Sets the graphic element's visible property. An invisible graphic element is simply not drawn.
     * @param {boolean} newVal Is this graphic element to be visible or not
     */
    this.setVisible = function(newVal)
    {
        if (newVal!=this._visible)
        {
            this._visible = newVal;
            this.invalidate();
        }
    };

    /**
     * Gets the visible state of this graphic element.
     * @return True/false; is the graphic element is currently visible
     * @type boolean
     */
    this.isVisible = function()
    {
        return this._visible;
    };

    /**
     * Marks this element and the layer that owns this element as invalid (needing to be redrawn).
     */
    this.invalidate = function()
    {
        this._invalid = true;
        if (this._layer!=null)
            this._layer.invalidate();
    };

    /**
     * Returns whether or not this graphic element needs to be redrawn.
     * @return Whether or not this graphic element needs to be redrawn.
     * @type boolean
     */
    this.isInvalid = function()
    {
        return this._invalid;
    };


    /**
     * This method sets the origin to the center of the image. The default is
     * the top left corner (0,0).
     */
    this.setOriginCenter=function()
    {
        this._originX = this.getWidth()/2;
        this._originY = this.getHeight()/2;
    };


    /**
     * This method will reset the origin of the image to the top left corner (0,0).
     */
    this.resetOriginCenter=function()
    {
        this._originX = 0;
        this._originY = 0;
    };

    /**
     * Sets the x position of this graphic element on the canvas and invalidates
     * the layer this sprite is on.
     * @param {int} newX The new x position for this graphic element on the layer
     */
    this.setX = function(newX)
    {
        this._x = newX;
        this.invalidate();
    };

    /**
     * Sets the y position of this graphic element on the canvas and invalidates
     * the layer this sprite is on.
     * @param {int} newY The new y position for this graphic element on the layer
     */
    this.setY = function(newY)
    {
        this._y = newY;
        this.invalidate();
    };

    /**
     * Gets the x position of this graphic element on the layer
     * @return The x position of this graphic element
     * @type int
     */
    this.getX = function()
    {
        return this._x;
    };

    /**
     * Gets the y position of this graphic element on the layer
     * @return The y position of this graphic element
     * @type int
     */
    this.getY = function()
    {
        return this._y;
    };

    /**
     * Sets the adjusted x position of this graphic element on the canvas and invalidates
     * the layer this sprite is on.
     * @param {int} newX The new adjusted x position for this graphic element on the layer. Default is 0.
     */
    this.setAdjustedX = function(newX)
    {
        this._adjustedX = newX;
        this.invalidate();
    };

    /**
     * Sets the adjusted y position of this graphic element on the canvas and invalidates
     * the layer this sprite is on.
     * @param {int} newY The new adjusted y position for this graphic element on the layer. Default is 0.
     */
    this.setAdjustedY = function(newY)
    {
        this._adjustedY = newY;
        this.invalidate();
    };

    /**
     * Gets the adjusted x position of this graphic element on the layer
     * @return The adjusted x position of this graphic element
     * @type int
     */
    this.getAdjustedX = function()
    {
        return this._adjustedX;
    };

    /**
     * Gets the adjusted y position of this graphic element on the layer
     * @return The adjusted y position of this graphic element
     * @type int
     */
    this.getAdjustedY = function()
    {
        return this._adjustedY;
    };

    /**
     * This method sets the origin of the graphic element. The default is
     * the top left corner (0,0).
     * Setting the origin will cause any transformations to occur relative to the
     * origin.
     * @param {int} newX The new origin X position on the graphic element
     * @param {int} newY The new origin Y position on the graphic element
     * @see #setOriginCenter For another description about how the origin works
     */
    this.setOrigin = function(newX, newY)
    {
        this._originX = newX;
        this._originY = newY;
        this.invalidate();
    };


    /**
     * Gets the x position of this graphic element on the canvas.
     * @return The X origin on the graphic element
     * @type int
     */
    this.getOriginX = function()
    {
        return this._originX;
    };

    /**
     * Gets the x position of this graphic element on the canvas.
     * @return The Y origin on the graphic element
     * @type int
     */
    this.getOriginY = function()
    {
        return this._originY;
    };

    /**
     * Adds a new behaviour to this graphic element's behaviours list.
     */
    this.addBehaviour = function(behaviour)
    {
        this._behaviours.push(behaviour);
    };

    /**
     * Removes a behaviour from this graphic element.
     * The parameter may be an index OR it can be the behaviour object itself OR
     * it can be the string name that identifies the behaviour.
     * @param {Object} behaviour A String OR an int specifying the behaviour you wish to remove
     */
    this.removeBehaviour = function(behaviour)
    {
        if (isInstanceOf(behaviour, "Number"))
            this._behaviours.splice(parseInt(behaviour), 1);
        else if (isInstanceOf(behaviour, "String"))
        {
            var bIndex = getBehaviour(behaviour);
            if (bIndex!=null)
            {
                this._behaviours.splice(bIndex, 1);
            }

        }
        else
        {
            var index = this._behaviours.indexOf(behaviour);
            if (index>-1)
            {
                this._behaviours.splice(index, 1);
            }
        }
    };

    /**
     * Returns a behaviour this graphic element that matches the given behaviourName.
     * @param {String} behaviourName The name of the behaviour as specified by the behaviour class
     * @return The H5EBehaviour object associated with the behaviourName or null if not found
     * @type H5EBehaviour
     */
    this.getBehaviour = function(behaviourName)
    {
        for(var bIndex=0; bIndex<this._behaviours.length; bIndex++)
        {
            var behaviour = this._behaviours[bIndex];
            if (behaviour.getName().toLowerCase() == behaviourName.toLowerCase())
                return behaviour;
        }
        return null;
    };


    /**
     * This method gets called as part of the main loop in order to update any
     * behaviours that might be part of this graphic element.
     */
    this.update = function()
    {
        for(var index; index<this._behaviours; index++)
        {
            this._behaviours[index].update();
        }
    };
}