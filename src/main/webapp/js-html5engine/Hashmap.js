/**
 * @constructor
 * @class This object works similarily to the java equivalent but with some minor
 * tweaks. HOWEVER, no actual hashing is done. This object actually uses javascript's
 * dynamic object fields to create the 'hash' so the keys must be modified according
 * to field naming rules for objects. Please just use simple string
 * keys (or url paths, thats ok too) and be aware of this (you may get a key conflict
 * if you're not careful). For example, these 3 keys are the same...<br>
 * <br>fun/stuff
 * <br>fun\stuff
 * <br>fun:stuff
 * <br>The above 3 keys would end up being the same thing since the non-alphanumeric
 * characters get replaced with underscores when performing the key save.
 * @return A new Hashmap object
 *
 */
function Hashmap()
{
    /**The list of values stored in this hashmap. @type Object[]*/
    this._values = new Array();
    /**The list of keys stored in this hashmap. @type Object[]*/
    this._keys = new Array();
    /**Mapping a string key to an index where the value is stored in the _array object. This is accomplished by storing the keys as fields of the object. @type Object*/
    this._map = new Object();

    /**
     * Puts a new value into the hashmap.
     * @param {String} key Any key used to access the hashmap (this shouldn't be too long)
     * @param {Object} value Any value (object or primitive) that you would like to store against the key
     */
    this.put = function(key, value)
    {
    	if (typeof(key)=='number' || typeof(key)=='object')
    		key = key+"";

        var index = this._map[stringToKey(key)];
        if (index==null)
        {
            this._keys.push(key);
            this._values.push(value);
            this._map[stringToKey(key)] = this._values.length-1;
        }
        else
        {
            this._values[index] = value;
        }
    };

    /**
     * Retrieves a value from the hashmap by the given key.
     * @param {String} key Any key used to access a value within the hashmap
     * @return The last value that was put into the hashmap against this key
     * @type Object
     */
    this.get = function(key)
    {
    	if (typeof(key)=='number' || typeof(key)=='object')
    		key = key+"";
        return this._values[this._map[stringToKey(key)]];
    };

    /**
     * Removes a put item from the hashmap.
     * @param {String} key The key that was used to put a value into the hashmap
     */
    this.remove = function(key)
    {
    	if (typeof(key)=='number' || typeof(key)=='object')
    		key = key+"";
        // Now we need to find the key in our key list so we can determine it's index...
        var index = -1;

        for(var i = 0; i<this._keys.length; i++)
            if (this._keys[i] == key)
            {
                index = i;
                break;
            }

        if (index>-1)
        {
            this._keys.splice(index, 1);
            this._values.splice(index, 1);
            this._map[stringToKey(key)]=null;
            this._remapObjects();
        }
    };

    this._remapObjects = function()
    {
        for(var i = 0; i<this._keys.length; i++)
            this._map[stringToKey(this._keys[i])] = i;
    };
    /**
     * This retrieves all values in the hashmap in an array.
     * @return All values within the hashmap
     * @type Object[]
     */
    this.getValues = function()
    {
        return this._values;
    };

    /**
     * This retrieves all keys in the hashmap in an array.
     * @return All keys within the hashmap
     * @type String[]
     */
    this.getKeys = function()
    {
        return this._keys;
    };


    /**
     * The number of elements in the hashmap.
     * @return The number of elements in the hashmap.
     * @type int
     */
    this.getSize = function()
    {
        return this._values.length;
    };
}