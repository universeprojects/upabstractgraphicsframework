function H5ECharacter(engine, layer, mapObjectType, northAnim, northEastAnim, eastAnim, southEastAnim, southAnim, southWestAnim, westAnim, northWestAnim, tileX, tileY, tileSize)
{

    this.northAnim = northAnim;
    this.northEastAnim = northEastAnim;
    this.eastAnim = eastAnim;
    this.southEastAnim = southEastAnim;
    this.southAnim = southAnim;
    this.southWestAnim = southWestAnim;
    this.westAnim = westAnim;
    this.northWestAnim = northWestAnim;

    // Operating variables...
    this._speedMultiplier = 1;
    this._motionToAnimationRatio = 1;
    this._currentDirection = null;
    this._nextDirection = null;
    /**@type H5EAnimatedSpriteBehaviour*/
    this._animBehaviour = null;
    this._motionBehaviour = null;


    // Events...
    this.onPathCheck = null;
    //////////////

    this.go = function(direction)
    {
        // Attempting to move a character. We need to determine if we're already
        // in motion and if so, record the direction request into the 'next direction'
        // variable so we can move in that direction when we're finished....
        if (this.isMoving())
        {
            this._nextDirection = direction;
            return;
        }


        // direction can be 0 for north, 1 for north-east, 2 for east..etc
        // 0 = north, 2 = east, 4 = south, 6 = west
        if (direction == 0)
        {
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX(), this.getTileY()-1);
            this.setY(this.getY()+this._tileSize);
            this._motionBehaviour.setPosition(null, this._y-this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.northAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 1)
        {
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()+1, this.getTileY()-1);
            this.setX(this.getX()-this._tileSize);
            this.setY(this.getY()+this._tileSize);
            this._motionBehaviour.setPosition(this._x+this._tileSize, this._y-this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.northEastAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 2)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()+1, this.getTileY());
            this.setX(this.getX()-this._tileSize);
            this._motionBehaviour.setPosition(this._x+this._tileSize, null, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.eastAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 3)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()+1, this.getTileY()+1);
            this.setX(this.getX()-this._tileSize);
            this.setY(this.getY()-this._tileSize);
            this._motionBehaviour.setPosition(this._x+this._tileSize, this._y+this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.southEastAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 4)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX(), this.getTileY()+1);
            this.setY(this.getY()-this._tileSize);
            this._motionBehaviour.setPosition(null, this._y+this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.southAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 5)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()-1, this.getTileY()+1);
            this.setX(this.getX()+this._tileSize);
            this.setY(this.getY()-this._tileSize);
            this._motionBehaviour.setPosition(this._x-this._tileSize, this._y+this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.southWestAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 6)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()-1, this.getTileY());
            this.setX(this.getX()+this._tileSize);
            this._motionBehaviour.setPosition(this._x-this._tileSize, null, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.westAnim, true);
            this._animBehaviour.play();
        }
        else if (direction == 7)
        {
            // Check if we're allowed to walk here...
            // Check if we're allowed to walk here...
            if (this.onPathCheck!=null)
            {
                var newDirection = this.onPathCheck(direction);
                if (newDirection==null)
                    return;
                else if (newDirection!=direction)
                {
                    this.go(newDirection);
                    return;
                }
            }
            this._currentDirection = direction;

            this.setPosition(this.getTileX()-1, this.getTileY()-1);
            this.setX(this.getX()+this._tileSize);
            this.setY(this.getY()+this._tileSize);
            this._motionBehaviour.setPosition(this._x-this._tileSize, this._y-this._tileSize, this._speedMultiplier/this._motionToAnimationRatio);
            this._animBehaviour.setAnimatedSpriteType(this.northWestAnim, true);
            this._animBehaviour.play();
        }
    }

    this.setSpeedMultiplier = function(value)
    {
        this._speedMultiplier = value;
        this._animBehaviour.speedMultiplier = value;
        this._motionBehaviour._positionalSpeed = value/this._motionToAnimationRatio;
    };

    this.setMotionToAnimationRatio = function(value)
    {
        this._motionToAnimationRatio = value;
        this._motionBehaviour._positionalSpeed = this._speedMultiplier/value;
    };

    this.isMoving = function()
    {
        return this._motionBehaviour.isMoving();
    };

    this.playCustomAnimation = function(animatedSpriteType)
    {
        this._animBehaviour.setAnimatedSpriteType(animatedSpriteType);
    }

    this.stopCustomAnimation = function()
    {
        this._animBehaviour.stop();
    }


    //*********************
    // Constructor area...
    //*********************

    // Create the animation behaviour and associate it with ourselves
    this._animBehaviour = new H5EAnimatedSpriteBehaviour(this, northAnim);
    this._motionBehaviour = new H5EMotionBehaviour(this);
    this.addBehaviour(this._animBehaviour);
    this.addBehaviour(this._motionBehaviour);
    // When a motion is complete, then we want to continue moving if we have a
    // 'next motion' available.
    this._motionBehaviour.onComplete = function()
    {
//        if (self._currentDirection==0)
//            self._tileY -= 1;
//        else if (self._currentDirection==2)
//            self._tileX += 1;
//        else if (self._currentDirection==4)
//            self._tileY += 1;
//        else if (self._currentDirection==6)
//            self._tileX -= 1;

        self._currentDirection = null;

        if (self._nextDirection!=null)
        {
            self.go(self._nextDirection);
            self._nextDirection = null;
        }
        else
            self._animBehaviour.stop();
    };

}