/**
 * Construct a new H5EBehaviour object. This is an ABSTRACT class.
 * @class This is an ABSTRACT class. It shouldn't be instansiated on it's own
 * but instead extended by other classes. Behaviours are applied to graphical
 * elements (like sprites) and provide extra functionality. For example, the
 * H5EBouncyBehaviour controls the rotation, scale and translation of a sprite.
 * If you were to set the rotation of the sprite through the bouncy behaviour,
 * the behaviour would animate the sprite so it looked like the sprite was flowing
 * to the destination rotation fluidly and bounce around until it finally arrived
 * at the destination rotation. Some other ideas for behaviours could be: rumble
 * (a behaviour that shakes a sprite or layer), fade (controlling fade-in/fade-out
 * of a sprite or layer and how long it should last).
 * @constructor
 * @param {H5EGraphicElement} h5eGraphicElement The graphic element object that this behaviour belongs to
 * @return A new behaviour object
 */
function H5EBehaviour(h5eGraphicElement)
{
    /**The element that this behaviour controls @type H5EGraphicElement*/
    this._element = h5eGraphicElement;

    /**
     * This is an ABSTRACT method; it must be overriden by the extending behaviour
     * class. This method is called in the engine._mainLoop() automatically at a rate
     * specified by the desired tps (ticks per second).
     */
    this.update = function()
    {
        throw("The update function of a behaviour has not been overridden.");
    };

    /**
     * This is an ABSTRACT method; it must be overriden by the extending behaviour
     * class. This method specifies the unique name given to this behaviour, a user
     * will be able to retrieve a behaviour from a graphic element based on the name
     * alone. For example: sprite.getBehaviourByName('bouncy') - This method would
     * search through the behaviours in the sprite and look for the one where
     * getName() returns 'bouncy'.
     */
    this.getName = function()
    {
        throw("The getName function of a behaviour has not been overridden.");
    };
    //********************
    // Constructor area...
    //********************

}