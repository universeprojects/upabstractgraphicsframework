/**
 * Construct a new H5EResourceManager object.
 * @class The H5EEngine class will automatically create one of these to facilitate
 * the handling of resources (loading and storing). The manager generally uses
 * 'keys' to retrieve stored resources. Use the stringToKey function provided to convert
 * any string into a valid key. Usually the url names are used as strings (for images).
 * Only one resource manager is necessary per engine instance.
 *
 * This class also facilitates the creation and loading of a file that describes
 * all the resources in a project. This includes images and audio resources
 * as well as sprite types, animated sprites, video..etc.
 * 
 * @constructor Constructs a new H5EResourceManager object.
 * @param {H5EEngine} engine The engine object that this resource manager belongs to
 * @param {String} serverUrl Most resources are stored as relative paths. If this parameter is supplied, it will be used to complete the relative paths of the resources loaded.
 * @return A new resource manager
 */
function H5EResourceManager(engine, serverUrl)
{
    // Correct the parameters passed in
    if (serverUrl==null)
        serverUrl="";

    // References...
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine=engine;

    // Operating variables...
    /**A total number of resources that have been added to this resource manager @type int*/
    this._totalItems=0;
    /**An array of key/src string pairs. For example: ["house", "images/house.png", "test", "images/test.gif"] @type String[]*/
    this._imageLoadQueue=[];
    /**All images loaded for this resource manager but stored by key @type Hashmap*/
    this._images=new Hashmap();
    /**An array of key/src string pairs. For example: ["ping", "audio/ping.oog", "background", "audio/background.wav"] @type String[]*/
    this._audioLoadQueue=[];
    /**All audio files loaded for this resource manager but stored by key @type Hashmap*/
    this._audio=new Hashmap();
    /**All sprite types loaded for this resource manager @type Hashmap*/
    this._spriteTypes= new Hashmap();
    /**All map object types loaded for this resource manager @type Hashmap*/
    this._mapObjectTypes= new Hashmap();
    /**All animated sprite types loaded for this resource manager @type Hashmap*/
    this._animatedSpriteTypes = new Hashmap();
    /** This is the url path to the server. The resource manager keeps lists of relative paths for all it's resources. In some cases, a serverUrl needs to be specified to find the images if they are located outside of the domain of this object. @type String*/
    this._serverUrl = serverUrl;
    /**Keeps track of the resource library file version we are using. This field is updated when a new library is loaded in. This allows for understanding of how to handle older file types. @type String*/
    this._fileVersion = "1";
    /**Keeps track of the ajax object we use for loading the library just in case we have to use it more than once. @type XMLHttpRequest*/
    this._ajaxObj = null;
    /**Keeps track of the option to show a progress bar when loading. This is set by the load() method @type boolean*/
    this._showProgress = true;

    // Events...
    /**When all queued loads are complete, this event fires @type function*/
    this.onComplete= null;
    /**When an error has occured while attempting to load, this event fires. A single string parameter will be passed into the event handler assigned to this event with the error message. @type function*/
    this.onError= null;
    /**When loading progress is made, this event is fired @type function*/
    this.onProgress= null;   


    /**
     * Creates a new sprite type using the given imageKey
     * and stores it in the resource manager under the given spriteTypeKey.
     *
     * @param {String} imageKey The key used to identify a loaded image that will be used as the graphic for the spriteType
     * @param {String} spriteTypeKey The key used to identify this spriteType so it can be used later. This parameter can be skipped and the imageKey will be used instead.
     * @return The new H5ESpriteType object
     * @type H5ESpriteType
     */
    this.newSpriteType=function(imageKey, spriteTypeKey)
    {
        if (spriteTypeKey==null)
            spriteTypeKey=imageKey;
        var spriteType = new H5ESpriteType(this._engine, imageKey);
        this._spriteTypes.put(spriteTypeKey, spriteType);
        return spriteType;
    };


    /**
     * This method will accept a spriteType object as a parameter
     * and add it to this resource manager's list of sprite types.
     * This method is similar to newSpriteType() except newSpriteType()
     * will actually create the spriteType object for you.
     * The purpose of this method is to allow extended sprite types to be stored
     * by the resource manager.
     * @param {String} spriteTypeKey The key used to access this sprite type object later
     * @param {H5ESpriteType} spriteType The sprite type object you wish to be stored in the resource manager
     */
    this.addSpriteType=function(spriteTypeKey, spriteType)
    {
        this._spriteTypes.put(spriteTypeKey, spriteType);
    };

    /**
     * Returns the sprite type that is associted with the given spriteTypeKey.
     *
     * @param {String} spriteTypeKey A key (string) that is used to identify a particular spriteType within the resource manager
     * @return The associated H5ESpriteType or null if the sprite type could not be found
     * @type H5ESpriteType
     *
     */
    this.getSpriteType=function(spriteTypeKey)
    {
        return this._spriteTypes.get(spriteTypeKey);
    };


    /**
     * Returns all H5ESpriteType objects stored in this resource manager in an Array.
     * @return An array of H5ESpriteType objects.
     * @type Array
     */
    this.getSpriteTypes = function()
    {
        return this._spriteTypes.getValues();
    };


    /**
     * Returns all keys associated with H5ESpriteType objects, stored in this
     * resource manager, in an Array.
     * @return An array of Strings; all keys that are stored against sprite types in this resource manager
     * @type Array
     */
    this.getSpriteTypeKeys = function()
    {
        return this._spriteTypes.getKeys();
    };

    /**
     * Removes the sprite type that is associated with the given spriteTypeKey.
     *
     * @param {String} spriteTypeKey A key (string) that is used to identify a particular spriteType within the resource manager
     *
     */
    this.removeSpriteType=function(spriteTypeKey)
    {
        this._spriteTypes.remove(spriteTypeKey);
    };

    /**
     * This method will accept a mapObjectType object as a parameter
     * and add it to this resource manager's list of map object types.

     * @param {String} mapObjectTypeKey The key used to access this map object type object later
     * @param {H5EMapObjectType} mapObjectType The map object type object you wish to be stored in the resource manager
     */
    this.addMapObjectType=function(mapObjectTypeKey, mapObjectType)
    {
        this._mapObjectTypes.put(mapObjectTypeKey, mapObjectType);
    };

    /**
     * Returns the map object type that is associted with the given mapObjectTypeKey.
     *
     * @param {String} mapObjectTypeKey A key (string) that is used to identify a particular mapObjectType within the resource manager
     * @return The associated H5EMapObjectType or null if the map object type could not be found
     * @type H5EMapObjectType
     *
     */
    this.getMapObjectType=function(mapObjectTypeKey)
    {
        return this._mapObjectTypes.get(mapObjectTypeKey);
    };


    /**
     * Returns all H5EMapObjectType objects stored in this resource manager in an Array.
     * @return An array of H5EMapObjectType objects.
     * @type Array
     */
    this.getMapObjectTypes = function()
    {
        return this._mapObjectTypes.getValues();
    };


    /**
     * Returns all keys associated with H5EMapObjectType objects, stored in this
     * resource manager, in an Array.
     * @return An array of Strings; all keys that are stored against map object types in this resource manager
     * @type Array
     */
    this.getMapObjectTypeKeys = function()
    {
        return this._mapObjectTypes.getKeys();
    };

    /**
     * Removes the map object type that is associated with the given mapObjectTypeTypeKey.
     *
     * @param {String} mapObjectTypeKey A key (string) that is used to identify a particular mapObjectType within the resource manager
     *
     */
    this.removeMapObjectType=function(mapObjectTypeKey)
    {
        this._mapObjectTypes.remove(mapObjectTypeKey);
    };
    /**
     * Creates a new animated sprite type and stores it in the
     * resource manager under the given animatedSpriteTypeKey.
     *
     * @param {String} animatedSpriteTypeKey The key used to identify this animatedSpriteType so it can be used later.
     * @return The new H5EAnimatedSpriteType object
     * @type H5EAnimatedSpriteType
     */
    this.newAnimatedSpriteType = function(animatedSpriteTypeKey)
    {
        var animSpriteType = new H5EAnimatedSpriteType(this._engine);
        this._animatedSpriteTypes.put(animatedSpriteTypeKey, animSpriteType);
        return animSpriteType;
    };

    /**
     * This method will accept an animatedSpriteType object as a parameter
     * and add it to this resource manager's list of animated sprite types.
     * This method is similar to newAnimatedSpriteType() except newAnimatedSpriteType()
     * will actually create the animatedSpriteType object for you.
     * The purpose of this method is to allow extended animated sprite types to be stored
     * by the resource manager.
     * @param {String} animatedSpriteTypeKey The key used to access this animated sprite type object later
     * @param {H5EAnimatedSpriteType} animatedSpriteType The animated sprite type object you wish to be stored in the resource manager
     */
    this.addAnimatedSpriteType=function(animatedSpriteTypeKey, animatedSpriteType)
    {
        this._animatedSpriteTypes.put(animatedSpriteTypeKey, animatedSpriteType);
    };

    /**
     * Returns the animated sprite type that is associted with the given animatedSpriteTypeKey.
     *
     * @param {String} animatedSpriteTypeKey A key (string) that is used to identify a particular animatedSpriteType within the resource manager
     * @return The associated H5EAnimatedSpriteType or null if the animated sprite type could not be found
     * @type H5EAnimatedSpriteType
     *
     */
    this.getAnimatedSpriteType=function(animatedSpriteTypeKey)
    {
        return this._animatedSpriteTypes.get(animatedSpriteTypeKey);
    };

    /**
     * Returns all H5EAnimatedSpriteType objects stored in this resource manager in an Array.
     * @return An array of H5EAnimatedSpriteType objects.
     * @type Array
     */
    this.getAnimatedSpriteTypes = function()
    {
        return this._animatedSpriteTypes.getValues();
    };


    /**
     * Returns all keys associated with H5ESpriteType objects, stored in this
     * resource manager, in an Array.
     * @return An array of Strings; all keys that are stored against animated sprite types in this resource manager
     * @type Array
     */
    this.getAnimatedSpriteTypeKeys = function()
    {
        return this._animatedSpriteTypes.getKeys();
    };

    /**
     * Removes the animated sprite type that is associated with the given animatedSpriteTypeKey.
     *
     * @param {String} animatedSpriteTypeKey A key (string) that is used to identify a particular animatedSpriteType within the resource manager
     *
     */
    this.removeAnimatedSpriteType=function(animatedSpriteTypeKey)
    {
        this._animatedSpriteTypes.remove(animatedSpriteTypeKey);
    };


    /**
     * This method will add a single image into the resource manager's queue for loading.
     *
     * Note: Actual loading will not start until .load() is called.
     *
     * @param {String} key A string identifier that will be used to get the image from the resourceManager again later. You may use the url for this
     * @param {String} src The url pointing to the image to load
     *
     */
    this.addImage=function(key, src)
    {
        if (src==null)
            src = key;
        this._imageLoadQueue.push(key);
        this._imageLoadQueue.push(src);
        this._totalItems++;
    };

    /**
     * This method will add a list of images into the resource manager's queue of images to load.
     * The url used to locate the image will also be used as a key to access it.
     *
     * @param {String[]} imageList An array of urls that locate the images you wish to load in. It will also be used as the imageKey
     */
    this.addImages=function(imageList)
    {
        for(var i = 0; i<imageList.length; i++)
        {
            var src = imageList[i];
            this.loadImage(src, src);
        }
    };


    /**
     * Gets an image stored in this resource manager by the given key.
     *
     * @param {String} key The string identifier that was used when loading the image
     * @return The image associated with the given key
     * @type H5EImage
     *
     */
    this.getImage=function(key)
    {
        var img = this._images.get(key);
        return img;
    };

    /**
     * Returns all H5EImage objects stored in this resource manager in an Array.
     * @return An array of H5EImage objects.
     * @type Array
     */
    this.getImages = function()
    {
       return this._images.getValues();
    };

    /**
     * Returns all keys associated with H5EImage objects, stored in this
     * resource manager, in an Array.
     * @return An array of Strings; all keys that are stored against images in this resource manager
     * @type Array
     */
    this.getImageKeys = function()
    {
       return this._images.getKeys();
    };

    /**
     * Sets an image stored in this resource manager to null according to the given key.
     *
     * @param {String} key The string identifier that was used when loading the image
     *
     */
    this.removeImage=function(key)
    {
        this._images.remove(key);
    };


    /**
     * This method will add a single audio file into the resource manager's queue for loading.
     *
     * Note: Actual loading will not start until .load() is called.
     *
     * @param {String} key A string identifier that will be used to get the audio file from the resourceManager again later. You may use the url for this
     * @param {String} src The url pointing to the audio file to load
     *
     */
    this.addAudio=function(key, src)
    {
        if (src==null)
            src = key;
        this._audioLoadQueue.push(key);
        this._audioLoadQueue.push(src);
        this._totalItems++;
    };

    /**
     * This method will add a list of audio files into the resource manager's queue of audio files to load.
     * The url used to locate the audio file will also be used as a key to access it.
     *
     * @param {String[]} audioList An array of urls that locate the audio files you wish to load in. It will also be used as the audioKey
     */
    this.addAudioFiles=function(audioList)
    {
        for(var i = 0; i<audioList.length; i++)
        {
            var src = audioList[i];
            this.loadAudio(src, src);
        }
    };

    /**
     * Gets an audio file stored in this resource manager by the given key.
     *
     * @param {String} key The string identifier that was used when loading the audio file
     * @return The audio file associated with the given key
     * @type H5EAudio
     *
     */
    this.getAudio=function(key)
    {
        var aud = this._audio.get(key);
        return aud;
    };

    /**
     * Sets an audio file stored in this resource manager to null according to the given key.
     * The data should be garbage collected when it is appropriate by the browser.
     *
     * @param {String} key The string identifier that was used when loading the audio file
     *
     */
    this.removeAudio=function(key)
    {
        this._audio.remove(key);
    };

    /**
     * Plays the audio file by the given key. If the key does not return
     * an audio file, an exception is thrown.
     *
     * Currently this method does not allow for multiple instances of the same
     * sound to play overlapping. It will simply restart the sound from the beginning
     * if it was already playing.
     *
     * TODO_OLD: Allow overlapping plays of the same sound
     * @param {String} key The string identifier that was used when loading the audio file
     */
    this.playAudio = function(key)
    {
        var audio = this.getAudio(key);
        if (audio==null)
            return;
//            throw("Unable to find audio file by the key: "+key);


        var elem = audio.getJSAudio();
        elem.pause();
        elem.currentTime=0;
        elem.play();
    };
    /**
     * This starts the process of loading all resources. Once the load is complete
     * the onComplete event will fire. If there was an error, the onError event
     * will fire. If the load was aborted, the onError event will fire.
     * Every time a single resource (image, audio..etc) is finished, the onProgress
     * event will fire.
     */
    this.loadResources=function(showProgress)
    {
        if (showProgress==null)
            showProgress=true;
        if (showProgress==true)
        {
            this._updateLoadProgress();
        }
        // Load all images queued...
        var imageCount = this._imageLoadQueue.length;
        if (imageCount>0)
        {
            // Ok get the key and src that is next in the queue
            var key = this._imageLoadQueue.shift();
            var src = this._imageLoadQueue.shift();

            // Now trigger the load...
            var image = new H5EImage(engine);
            image.getJSImage().resourceManager = this;
            this._images.put(key, image);
            image.loadImage(src, this._doResourceLoadComplete, this._doResourceLoadError, this._doResourceLoadError);
        }

        // Load all queued audio...
        var audioCount = this._audioLoadQueue.length;
        if (audioCount>0)
        {
            // Ok get the key and src that is next in the queue
            key = this._audioLoadQueue.shift();
            src = this._audioLoadQueue.shift();

            // Now trigger the load...
            var audio = new H5EAudio(engine);
            audio.getJSAudio().resourceManager = this;
            this._audio.put(key, audio);
            audio.loadAudio(src, this._doResourceLoadComplete, this._doResourceLoadError, this._doResourceLoadError);
        }


        // All loading is complete, fire the onComplete event
        if (imageCount==0 && audioCount==0)
        {
            if (this.onComplete!=null)
                this.onComplete();
        }
    };

    /**
     * This is an internal event handler that is fired by an image or audio element
     * that is loading.
     */
    this._doResourceLoadComplete=function()
    {
        if (isInstanceOf(this, "HTMLImageElement"))
        {
            if (this.width==0 || this.height==0)
            {
                if (onError!=null)
                    onError("There was an error loading the an image. (Javascript reported it complete but the image size was 0) Resource url: "+this.src);
            }
        }
        else if (isInstanceOf(this, "HTMLAudioElement"))
        {
            if (this.duration==0)
            {
                if (onError!=null)
                    onError("There was an error loading the an audio file. (Javascript reported it complete but the duration was 0) Resource url: "+this.src);
            }
        }
        if (this._onProgress!=null)
            this._onProgress();
        this.resourceManager.loadResources();
    };

    /**
     * This is an internal event handler that is fired by an image or audio element
     * that is loading.
     */
    this._doResourceLoadError=function(e)
    {
        if (this.onError!=null)
            this.onError("There was an error loading the a resource. Resource url: "+this.src);
        else
            throw(e);
    };

    this._updateLoadProgress = function()
    {
        var itemsLeft = this._imageLoadQueue.length+this._audioLoadQueue.length;
        var percent = Math.floor(((this._totalItems-(itemsLeft/2))/this._totalItems)*100);
        var text = "";
        if (this._imageLoadQueue.length>0)
            text = "Loading Images: "+percent+"%";
        else if (this._audioLoadQueue.length>0)
            text = "Loading Audio: "+percent+"%";
        else
            text = "Loading complete!";

        this._updateProgressBar(text);
    }
    /**
     * This method will draw a progress bar/status on the screen while the resource manager is loading.
     * The resourceManager.load() function calls this every time a new resource has finished loading.
     * TODO_OLD: Update this method. It is incredibly inefficient as it recreates a number of objects that could be stored instead
     *
     */
    this._updateProgressBar = function(text)
    {
        var engine = this._engine;
        var context = engine._context;
        var width = engine._canvas.width;
        var height = engine._canvas.height;
        var font = "30px arial black";
        context.clearRect(0,0,width, height);
        context.font = font;
        var grad = context.createLinearGradient(0,height/2-20-24,0,height/2-20);
        grad.addColorStop(0, "#FFFFFF");
        grad.addColorStop(1, "#CCCCCC");
        context.fillStyle = grad;

        context.fillText(text, width/2-(text.length*8), height/2-20);
        context.font = font;
        context.strokeStyle = "#EEEEEE";
        context.strokeText(text, width/2-(text.length*8), height/2-20);
        if (engine._doubleBuffer==true)
            engine._pageFlip();
    };


    /**
     * This method starts the H5L (Html5Engine Library) loading process. It begins
     * by requesting the library file using the given libUrl. Once the library is
     * received, the resources specified within the library file will be loaded.
     * Finally, the onComplete event will be fired when all is finished loading.
     * 
     * @param {String} libUrl The url that points to the Html5Engine library file that is to be downloaded and used
     * @param {boolean} showProgress Should this load show it's progress using the engine's progress bar? True|false
     */
    this.loadLibrary = function(libUrl, showProgress)
    {
        this._showProgress = showProgress;
        if (showProgress)
            this._updateProgressBar("Retrieving resource library...");

        if (this._ajaxObj == null)
        {
            var browser = navigator.appName;
            if(browser == "Microsoft Internet Explorer")
            {
                this._ajaxObj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else
            {
                this._ajaxObj = new XMLHttpRequest();
            }
            this._ajaxObj.resourceManager = this;
        }
        this._ajaxObj.open("get",libUrl);
        this._ajaxObj.onreadystatechange = this._doLibLoaded;
        this._ajaxObj.send(null);
    };

    /**
     * This event handler is called when the loadLibrary() method has returned from the server
     * with some sort of answer. It will continue the loading process by beginning
     * to load in all resources into the resource manager.
     * This method is a continuation of the loadLibrary() method.
     */
    this._doLibLoaded = function()
    {
        if(this.readyState == 4)
        {
            if(this.status == 200)
            {
                // Read the library file settings into the resource manager (ourselves)...
                this.resourceManager.readLibraryFile(this.responseText);
                // Begin the image/audio/video load sequence
                this.resourceManager.loadResources(this.resourceManager._showProgress);
            }
            else
            {
                if (this.resourceManager._showProgress)
                    this.resourceManager._updateProgressBar("Error retreiving resource library: "+this.statusText);
                throw("Resource library load failed: "+this.statusText);
            }
        }
    };


    /**
     * Loads all resources from the given file. The file itself is to be in the format
     * created by the resource manager when it saves.
     * @param {String} file The textual file containing all data necessary for the resource manager to load all resources in
     */
    this.readLibraryFile = function(file)
    {
        var state = "";
        var lines = file.split(/\r?\n/);
        for(var i = 0; i<lines.length; i++)
        {
            var line = lines[i];

            // Determine the read state
            if (line.match(".*:")!=null)
            {
                state = line.substr(0, line.length-1);
                continue;
            }

            // Read the data...
            if (state == "file version")
                this._fileVersion = line;
            else if (state == "image")
            {
                var key = line;
                i++;
                var src = lines[i];
                this.addImage(key, src);
            }
            else if (state == "audio")
            {
                key = line;
                i++;
                src = lines[i];
                this.addAudio(key, src);
            }
            else if (state!=null)
            {
                i = eval(state+".createFromFile(lines, i, this);");

                // IMPORTANT: The createFromFile method on all objects will return
                // the current line number as the "next" line however, the for loop
                // will also increase the line num by 1 so we have to decrease by 1
                // first in order to not miss any lines.
                i--;
                state = null;
            }
        }
    };

    /**
     * This method will generate a String that can be later saved as a h5l file
     * (Html5Engine Library file).
     * @return The contents of the h5l file.
     * @type String
     */
    this.writeLibraryFile = function()
    {
        var file = "";
        add = function(text)
        {
            file += text+"\n";
        };
        // Save filetype version...
        add("file version:");
        add("1");

        // Save images...
        var imageKeys = this._images.getKeys();
        var images = this._images.getValues();
        for(var i = 0; i<this._images.getSize(); i++)
        {
            add("image:");
            add(imageKeys[i]);
            add(images[i].getOriginalUrl());
        }
        // Save audio...
        var audioKeys = this._audio.getKeys();
        var audio = this._audio.getValues();
        for(var i = 0; i<this._audio.getSize(); i++)
        {
            add("audio:");
            add(audioKeys[i]);
            add(audio[i].getOriginalUrl());
        }
        // Save sprite types...
        var spriteTypes = this._spriteTypes.getValues();
        for(var i = 0; i<spriteTypes.length; i++)
        {
            var spriteType = spriteTypes[i];
            file = spriteType._saveTo(file);
        }
        // Save animated sprite types...
        var animSpriteTypes = this._animatedSpriteTypes.getValues();
        for(var i = 0; i<animSpriteTypes.length; i++)
        {
            var animSpriteType = animSpriteTypes[i];
            file = animSpriteType._saveTo(file);
        }
        // Save map object types...
        var mapObjectTypes = this._mapObjectTypes.getValues();
        for(var i = 0; i<mapObjectTypes.length; i++)
        {
            var mapObjectType = mapObjectTypes[i];
            file = mapObjectType._saveTo(file);
        }
        return file;
    };

    setServerUrl = function(newUrl)
    {
        this._serverUrl = newUrl;
    };
}