function H5EViewport(engine)
{
    this._engine = engine;

    /**The X coordinate of the viewport. Increasing this value will shift the view to the right. @type int*/
    this._viewX = 0;
    /**The Y coordinate of the viewport. Increasing this value will shift the view down. @type int*/
    this._viewY = 0;
    /**The X coordinate of the viewport before it was changed. This is used for optimizations. @type int*/
    this._previousViewX = 0;
    /**The Y coordinate of the viewport before it was changed. This is used for optimizations. @type int*/
    this._previousViewY = 0;

    this.setViewport = function(x, y)
    {
        // If there is no change, just exit
        if ((x==null && y==null) || (x==this._viewX && y==this._viewY))
            return;

        if (x!=null)
            this._viewX = x;
        if (y!=null)
            this._viewY = y;

        // We invalidate the layers because it causes them to redraw rather than using
        // a cached image
        for(var i = 0; i<this._engine.layers.length; i++)
            if (this._engine.layers[i].getIgnoreViewport()==false)
                this._engine.layers[i].invalidate();
    }

    this.translateViewport = function(x, y)
    {
        // If there is no change, just exit
        if ((x==null && y==null) || (x==0 && y==0))
            return;

        if (x!=null)
            this._viewX += x;
        if (y!=null)
            this._viewY += y;
        
        // We invalidate the layers because it causes them to redraw rather than using
        // a cached image
        for(var i = 0; i<this._engine.layers.length; i++)
            if (this._engine.layers[i].getIgnoreViewport()==false)
                this._engine.layers[i].invalidate();
    };

    this.getViewportX = function()
    {
        return this._viewX;
    };
    this.getViewportY = function()
    {
        return this._viewY;
    };

    this.transformToActualPixelX = function(canvasX)
    {
        return canvasX+this._viewX;
    };

    this.transformToActualPixelY = function(canvasY)
    {
        return canvasY+this._viewY;
    };

    this.transformToViewportPixelsX = function(actualX)
    {
        return actualX-this._viewX;
    };

    this.transformToViewportPixelsY = function(actualY)
    {
        return actualY-this._viewY;
    };

}