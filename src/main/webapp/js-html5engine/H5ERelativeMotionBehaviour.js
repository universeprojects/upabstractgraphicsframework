/**
 * Construct a new H5ERelativeMotionBehaviour object.
 * @extends H5EBehaviour
 * @class This behaviour will perform very basic animations (translation, rotation)
 * to a graphic element.
 * @constructor
 * @param {H5EGraphicElement} h5eGraphicElement The graphic element object that this behaviour belongs to
 * @return A new motion behaviour object
 */
function H5ERelativeMotionBehaviour(h5eGraphicElement)
{
    // Inherit from h5eBehaviour...
    this._super = H5EBehaviour;
    this._super(h5eGraphicElement);


    //Operating variables...
    /**An internal variable keeping track of the desired X offset  @type int*/
    this._offsetX = 0;
    /**An internal variable keeping track of the desired Y offset  @type int*/
    this._offsetY = 0;
    /**An internal variable keeping track of the desired rotation offset  @type int*/
    this._offsetRotation = 0;
    /**An internal variable keeping track of the desired scale destination  @type float*/
    this._offsetScale = 0;

    this._positionalTickCount = 1;
    this._rotationTickCount = 1;
    this._scaleTickCount = 1;

    // Events...
    this.onComplete = null;

    /**
     * Set the desired offset of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite, offseting it's position the desired amount.
     * @param {int} x The X offset that the sprite is to offset by
     * @param {int} y The Y offset that the sprite is to offset by
     * @param {float} tickCount The number of ticks that it will take before the offset is complete
     */
    this.setPositionOffset = function(x,y, tickCount)
    {
        this._offsetX = x;
        this._offsetY = y;
        this._positionalTickCount = tickCount;

    };


    /**
     * Set the desired rotation offset of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite, offseting it's angle the desired amount.
     * @param {int} rotationOffset The rotation (in degrees) that the sprite is offset by
     * @param {float} tickCount The number of ticks that it will take before the offset is complete
     */
    this.setRotationOffset = function(rotationOffset, tickCount)
    {
        this._offsetRotation = rotationOffset;
        this._rotationalTickCount = tickCount;
    };

    /**
     * Set the desired scale offset of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired size.
     * @param {float} scaleOffset The scale (1 = 100%, 0.5 = 50%) that the sprite is to offset by
     * @param {float} tickCount The number of ticks that it will take before the offset is complete
     */
    this.setScaleOffset = function(scaleOffset, tickCount)
    {
        this._offsetScale = scaleOffset;
        this._scaleTickCount = tickCount;
    };

    this.isMoving = function()
    {
        if (this._offsetX != 0 ||
            this._offsetY != 0 ||
            this._offsetRotation != 0 ||
            this._offsetScale != 0)
            return true;
        else
            return false;
    };

    /**
     * Overriden update function from H5EBehaviour.
     * @see H5EBehaviour#update
     */
    this.update = function()
    {
        var wasMoving = this.isMoving();

        // if we're not moving then no need to continue
        if (wasMoving==false)
            return;
            
        // Lets just invalidate the graphic element now and get it over with
        this._element.invalidate();

        // Position offset...
        if (this._offsetX!=0 || this._offsetY!=0)
        {
        	if (this._offsetX!=0)
			{
        		var amount = (this._offsetX/this._positionalTickCount);
        		this._element._x += amount;
        		this._offsetX -= amount;
			}
        	if (this._offsetY!=0)
			{
        		var amount = (this._offsetY/this._positionalTickCount);
        		this._element._y += amount;
        		this._offsetY -= amount;
			}
        	this._positionalTickCount--;
        }
        // Rotation offset...
        if (this._offsetRotation!=0)
        {
    		var amount = (this._offsetRotation/this._rotationalTickCount);
    		this._element._rotation += amount;
    		this._offsetRotation -= amount;

    		this._rotationalTickCount--;
        }
        // Scale offset...
        if (this._offsetScale!=0)
        {
    		var amount = (this._offsetScale/this._scaleTickCount);
    		this._element._scale += amount;
    		this._offsetScale -= amount;

    		this._scaleTickCount--;
        }
        if (wasMoving && this.isMoving()==false)
            if (this.onComplete!=null)
                this.onComplete();
    };

    /**
     * Overriden from H5EBehaviour.
     * @see H5EBehaviour#getName
     */
    this.getName = function()
    {
        return "relativemotion";
    };
    //********************
    // Constructor area...
    //********************

}