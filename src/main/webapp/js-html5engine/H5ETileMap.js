/**
 * Construct a new H5ETileMap object.
 * @extends H5ELayer
 * @class This component extends an H5ELayer to become a tile-based map component. This
 * will serve the purposes of any 2D grided map (in the style of warcraft 2).
 * This component has functions for creating maps as well as viewing.
 * This component allows you to specify specify which images are your tiles and
 * requires a very specific naming system for tile images.
 *
 * Some other requirements of this component...
 * - All tile images are to be the same size
 *
 * TODO_OLD: Write an extensive description on how the tile map component actually works
 * and write a demo for this component
 * @constructor
 * @param {H5EEngine} engine The engine object that this layer belongs to
 * @param {String} tileImagesLocation The URL that specifies the location of the tile images. All tile images must be in the same location. IMPORTANT: This component expects a trailing slash on this parameter
 * @return A new H5ETileMap object that you can add to the engine using engine.addLayer()
 * @see H5EEngine#addLayer
 */
function H5ETileMap(engine, tileImagesLocation)
{
    // Inherit from h5eBehaviour...
    this._super = H5ELayer;
    this._super(engine);

    /**The size of tiles we're using. Usually 32 (32x32) but it can be anything. This field is calculated automatically when the tile images are added. @type int*/
    this._tileScale = null; 

    /**This is a sort of 'fake' hashmap object that keeps track of which terrain codes can mix with other terrain codes. The key is a single terrain code (example: 'g1' for grass type 1) and the value is an array of terrain codes (example: ['d1' - dirt type 1, 'w1' - water type 1..etc]) @type fakehashmap*/
    this._terrainCodes = new Object();
    /**This keeps a list of unique terrain codes which we use in the initialization of this component. @type String[]*/
    this._uniqueTerrainCodes = new Array();
    /**All possible tile configurations are stored in this fake hashmap. For example, if we have 2 terrainCodes - 'g1' for grass type 1 and 'd1' for dirt type 1, this hashmap will store g1g1g1g1 (solid grass) as the key and the value will be an integer specifying how many versions of that tile we have (the more versions the better so seeing the g1g1g1g1 tile side by side wont look repetative). We coudl also have g1g1d1d1 which would be grass and dirt together. @type fakehashmap*/
    this._tileCodes = new Object();
    /**The value of this is passed into the constructor at creation. @type String @see H5ETileMap*/
    this._tileImagesLocation = tileImagesLocation.replace("/+$", "")+"/";   // Ensure the tileImagesLocation ends with a forward slash

    /**The current brush we are using when drawing tiles on the map with the mouse. This must be a valid terrainCode. @type String*/
    this._terrainBrush = "w1";

    /**This stores the current tile map. Literally, this is a 3 dimensional array, the first two dimensions are the x,y coordinates on the map for the tile. The 3rd dimension is an array array that has a length of 2. The first element specifies the terrain code for the top left corner of the tile and the second element specifies which version of the tile (for the entire tile). In order to get the complete tileCode, you must retrieve the terrainCode for x/y & x+1/y & x+1/y+1 & x/y+1 and add the terrainCodes together into a single string. (I know its confusing but its actually very efficient and ingenious if I do say so myself) @type Object[][][]*/
    this._tiles = new Array();


    /**
     * Terrain codes are used to derive image names. The second parameter 
     * is the code that can mix with the first code.
     * @param {String} code A terrain code. It is ok to add the same terrain code more than once (for each mixCode combination)
     * @param {String} mixCode A terrain code that mixes with the first terrainCode parameter
     */
    this.addTerrainCode = function(code, mixCode)
    {
        // Now add it to the array we maintain so we can loop through them easily
        var mixCodeArray = this._terrainCodes[code];
        if (mixCodeArray == null)
        {
            mixCodeArray = new Array();
            this._terrainCodes[code] = mixCodeArray;
        }

        mixCodeArray.push(mixCode);

        if (this._uniqueTerrainCodes.indexOf(code)==-1)
            this._uniqueTerrainCodes.push(code);
    };

    /**
     * Returns the order of transition from one tile to another in an array.
     * This is a recursive method that determines the mixCode path we would take
     * to draw one terrain code type on top of another terrain code type. Since
     * there is a limitation to what terrain codes can mix with others, this
     * method navigates through this limitation by finding a way to mix terrain
     * codes anyway.
     * @param {String} startCode The terrain code we are plotting on the map
     * @param {String} endCode The terrain code we are plotting on
     * @param {String[]} alreadyTried This should be ignored! It is used internally by this method for recursion.
     * @return An array of terrainCode strings where 0 is the startCode and the last element is the endCode
     * @type String[]
     */
    this._getTerrainMixPath = function(startCode, endCode, alreadyTried)
    {
        if (startCode==endCode)
            return [endCode];
        
        if (alreadyTried==null)
            alreadyTried = new Array();

        if (alreadyTried.indexOf(startCode)>-1)
            return null;
        else
            alreadyTried.push(startCode);
        
        var mixCodes = this._terrainCodes[startCode];

        var addition = null;
        for(var i = 0; i<mixCodes.length; i++)
        {
            var childResult = this._getTerrainMixPath(mixCodes[i], endCode, alreadyTried);
            if (childResult != null && childResult.indexOf(endCode)>-1)
            {
                addition = childResult;
                break;
            }

        }

        var result = null;
        if (addition!=null)
            result = [startCode].concat(addition);

        return result;
    };

    /**
     * This method returns a arrays of unique terrain code pairs; pairs of terrain
     * codes that can mix together (because we have images for it). The tilemaps
     * initialization method needs this to find all the tile images we will be using.
     * @return A two dimensional array. The 2nd dimension has only 2 terrainCode elements (specifying the valid terrainCode mix).
     * @type String[][]
     */
    this._getUniqueTerrainMixPairs = function()
    {
        function alreadyExists(c1, c2, list)
        {
            for(var i = 0; i<list.length; i++)
            {
                if (list[i][0] == c1 && list[i][1] == c2)
                    return true;
                else if (list[i][0] == c2 && list[i][1] == c1)
                    return true;
            }
            return false;
        }

        var result = [];

        for(var codesIndex1 = 0; codesIndex1<this._uniqueTerrainCodes.length; codesIndex1++)
            for(var codesIndex2 = 0; codesIndex2<this._uniqueTerrainCodes.length; codesIndex2++)
            {
                var code1 = this._uniqueTerrainCodes[codesIndex1];
                var code2 = this._uniqueTerrainCodes[codesIndex2];

                var mixPath = this._getTerrainMixPath(code1, code2);
                if (mixPath!=null && mixPath.length>1)
                {
                    for(var i = 0; i<mixPath.length-1; i++)
                    {
                        if (alreadyExists(mixPath[i], mixPath[i+1], result)==false)
                            result.push([mixPath[i], mixPath[i+1]]);
                    }
                }

            }

        return result;

    };

    /**
     * This MUST be called after all terrain codes have been added. This method
     * will take the given terrain codes and enumerate the images associated with
     * each terrain code. In particular, this method will enumerate the number
     * of versions of each combination of terrain codes so the tilemap component
     * knows how many versions there are of the same tile when generating a random
     * tile.
     */
    this.initializeTerrainCodes = function()
    {
        var self = this;
        var addNumberOfTiles = function(tileCode)
        {
            var result = 0;
            while(true)
            {
                var image = self._engine.getResourceManager().getImage(self._tileImagesLocation + tileCode+(result+1)+".png");
                if (image==null)
                    break;
                else
                {
                    result++;
                    // If we haven't discovered what size of tiles we're using, do it now...'
                    if (self._tileScale == null)
                        self._tileScale = image.getWidth();
                }
            }
            self._tileCodes[tileCode] = result;
        };

        var uniqueTerrainMixPairs = this._getUniqueTerrainMixPairs()

        for(var terrainCodeIndex=0; terrainCodeIndex<uniqueTerrainMixPairs.length; terrainCodeIndex++)
        {
            var a = uniqueTerrainMixPairs[terrainCodeIndex][0];
            var b = uniqueTerrainMixPairs[terrainCodeIndex][1];

            addNumberOfTiles(a+a+a+a);
            addNumberOfTiles(b+a+a+a);
            addNumberOfTiles(b+b+a+a);
            addNumberOfTiles(b+b+b+a);
            addNumberOfTiles(b+b+b+b);
            addNumberOfTiles(a+b+a+a);
            addNumberOfTiles(a+b+b+a);
            addNumberOfTiles(a+b+b+b);
            addNumberOfTiles(a+a+b+a);
            addNumberOfTiles(a+a+b+b);
            addNumberOfTiles(a+a+a+b);
            addNumberOfTiles(a+b+a+b);
            addNumberOfTiles(b+a+b+a);
            addNumberOfTiles(b+a+a+b);
            addNumberOfTiles(b+b+a+b);
            addNumberOfTiles(b+a+b+b);
        }
    };


    /**
     * This retrieves the tile code string (which is terrainCode+terrainCode+terrainCode+terrainCode+imageVersion) at the
     * given x/y coordinates.
     * @param {int} x The x coordinate on the map to retrieve the tile code for
     * @param {int} y The y coordinate on the map to retrieve the tile code for
     * @return An array of terrainCodes that make up the tile code for this coordinate including the image version
     * @type String[]
     */
    this._getTileCode = function(x,y)
    {
        if (x>=0 && y>=0 && x<this._tiles.length-1 && y<this._tiles[0].length-1)
            return [this._tiles[x][y][0], this._tiles[x+1][y][0], this._tiles[x+1][y+1][0], this._tiles[x][y+1][0],this._tiles[x][y][1]];
        else
            return null;
    };


    /**
     * This is the same as _getTileCode except it returns a string that can be used
     * as part of the key to retrieve the image that is to be used for the given tile
     * coordinates.
     * @param {int} x The x coordinate on the map to retrieve the tile code for
     * @param {int} y The y coordinate on the map to retrieve the tile code for
     * @return The tile code for this position on the map
     * @type String
     * @see #_getTileCode
     */
    this._getTileImageCode = function(x,y)
    {
        if (x>=0 && y>=0 && x<this._tiles.length-1 && y<this._tiles[0].length-1)
        {
            var pos1 = this._tiles[x][y][0];
            var pos2 = this._tiles[x+1][y][0];
            var pos3 = this._tiles[x+1][y+1][0];
            var pos4 = this._tiles[x][y+1][0];
            var ver = this._tiles[x][y][1];
            return pos1+pos2+pos3+pos4+ver;
        }
        else
            return null;
    };

    /**
     * Sets the size of this tilemap and also clears the entire map to the
     * specified defaultTileCode. A tile code consists of 4 terrainCodes in a
     * string. For example if you had a terrain code of 'g1' for grass, you could
     * choose a default tile code of 'g1g1g1g1' for a solid grass tile.
     * @param {int} sizeX The width of the tilemap in tiles
     * @param {int} sizeY The height of the tilemap in tiles
     * @param {String} terrainCode The terrain code to be used as the default tile for the entire map
     */
    this.setSize = function(sizeX, sizeY, terrainCode)
    {
        this._tiles = new Array(sizeX+1);
        for(var x = 0; x<sizeX+1; x++)
        {
            this._tiles[x] = new Array(sizeY+1);
            for(var y = 0; y<sizeY+1; y++)
            {
                this._tiles[x][y] = new Array(2);
                if (x-1>=0 && y-1>=0)
                    this.setRandomTile(x-1, y-1, terrainCode, [terrainCode, terrainCode, terrainCode, terrainCode], false);
            }
        }
    };

    /**
     * This is an override of the base layer's newSprite method. DO NOT USE THIS.
     * Sprites cannot be added to tilemaps directly.
     * An exception will be thrown if this function is used.
     */
    this.newSprite = function(spriteTypeName)
    {
        throw("Sprites cannot be added to a TileMap layer. Please add sprites to a different layer instead");
    };

    /**
     * Draws this tilemap to the given context.
     * TODO_OLD: This method needs to be optimized very much. This simply draws all
     * tiles and has no concept of buffering or reusing tiles already drawn.
     * @param {CanvasRenderingContext2D} context The context on which to draw this TileMap
     */
    this._drawRawSelfTo = function(context)
    {
        var viewport = this._engine.viewport;

        // Determine tile offsets...
        var tileOffsetX = Math.floor(viewport.getViewportX()/this._tileScale);
        var tileOffsetY = Math.floor(viewport.getViewportY()/this._tileScale);
        var pixelOffsetX = viewport.getViewportX()-(tileOffsetX*this._tileScale);
        var pixelOffsetY = viewport.getViewportY()-(tileOffsetY*this._tileScale);

        // Determine the tile width/height that will fit in the viewport...
        var tileWidth = Math.ceil(this._engine.getWidth()/this._tileScale);
        var tileHeight = Math.ceil(this._engine.getHeight()/this._tileScale);

        for(var x = tileOffsetX; x<tileWidth+tileOffsetX+1; x++)
            for(var y = tileOffsetY; y<tileHeight+tileOffsetY+1; y++)
            {
                var tileCode = this._getTileImageCode(x,y);
                if (tileCode==null)
                {
                    context.fillStyle = "#000000";
                    context.fillRect((x*this._tileScale)-(tileOffsetX*this._tileScale)-pixelOffsetX, (y*this._tileScale)-(tileOffsetY*this._tileScale)-pixelOffsetY, this._tileScale, this._tileScale);
                }
                else
                {
                    var img = this._engine.getResourceManager().getImage(this._tileImagesLocation+tileCode+".png");
                    if (img!=null)
                        context.drawImage(img.getJSImage(), (x*this._tileScale)-(tileOffsetX*this._tileScale)-pixelOffsetX, (y*this._tileScale)-(tileOffsetY*this._tileScale)-pixelOffsetY);
                }
            }
    };


    /**
     * This will return the terrainCode that mixes with the given primaryCode
     * (the terrainCode that we wish to be in the given x,y coordinate).
     *
     * If there are two different terrain codes (other than the primaryCode),
     * only one will be returned and which one is decided randomly. It is not
     * important that we consider which one to return since the simplicity in the
     * 'web' design of terrain code mixing means that usually the same mix code
     * will be decided upon regardless.
     * @param {int} x The X coordinate on the tilemap
     * @param {int} y The Y coordinate on the tilemap
     * @param {String} primaryCode The terrain code that we are plotting and need to find a mix for on the tile at x/y
     * @return A valid terrain code that mixes with the primary code
     * @type String
     */
    this._getTerrainMixCode = function(x,y, primaryCode)
    {
        var mixCode = primaryCode;
        if (this._tiles[x][y][0]!=primaryCode)
            mixCode = this._tiles[x][y][0];
        else if (this._tiles[x+1][y][0]!=primaryCode)
            mixCode = this._tiles[x+1][y][0];
        else if (this._tiles[x+1][y+1][0]!=primaryCode)
            mixCode = this._tiles[x+1][y+1][0];
        else if (this._tiles[x][y+1][0]!=primaryCode)
            mixCode = this._tiles[x][y+1][0];

        var mixCodePath = this._getTerrainMixPath(primaryCode, mixCode);
        if (mixCodePath.length>2)
            mixCode = mixCodePath[1];

        return mixCode;
    };

    /**
     * Sets the given coordinate (x,y) tile. If you are setting a grass tile,
     * then the primaryTerrainCode will be the code for grass. The tileCode
     * must be a 4 length array of terrain codes. Currently, the terrain codes
     * should not differ from the primaryTerrainCode except for null (for specifying
     * only part of a tile).
     *
     * TODO_OLD: Plotting a partial tile seems to be broken. This must be fixed.
     *
     * @param {int} x The X coordinate on the map that specifies the tile we are attempting to set
     * @param {int} y The Y coordinate on the map that specifies the tile we are attempting to set
     * @param {String} primaryTerrainCode This simply specifies the terrain code that we are trying to draw on the map. I know it seems redundant and it sort of is.
     * @param {String[]} tileCode The 4 element array of terrain codes specifying what we would like to plot at this tile. Usually this would simply contain 4 elements of the primaryTerrainCode but you may set some to null in order to plot a partial tile.
     * @param {boolean} correctSurroundingTiles If this is set to true, the surrounding tiles will be corrected and mixed with the plotting tile
     */
    this.setRandomTile = function(x, y, primaryTerrainCode, tileCode, correctSurroundingTiles)
    {
        if (tileCode[0]==null)
            tileCode[0] = this._tiles[x][y][0];
        if (tileCode[1]==null)
            tileCode[1] = this._tiles[x+1][y][0];
        if (tileCode[2]==null)
            tileCode[2] = this._tiles[x+1][y+1][0];
        if (tileCode[3]==null)
            tileCode[3] = this._tiles[x][y+1][0];
        // Check if this new tile is different at all. If not, just leave.
        if (this._tiles[x][y][0] == tileCode[0] &&
        this._tiles[x+1][y][0] == tileCode[1] &&
        this._tiles[x+1][y+1][0] == tileCode[2] &&
        this._tiles[x][y+1][0] == tileCode[3])
            return;



        var tileCodeVersions = this._tileCodes[tileCode[0]+tileCode[1]+tileCode[2]+tileCode[3]];
        var version = 1;
        if (tileCodeVersions>1)
            version = random(1, tileCodeVersions);

        this._tiles[x][y][0] = tileCode[0];
        this._tiles[x+1][y][0] = tileCode[1];
        this._tiles[x+1][y+1][0] = tileCode[2];
        this._tiles[x][y+1][0] = tileCode[3];
        this._tiles[x][y][1] = version;

        this.invalidate();
        if (correctSurroundingTiles)
        {
            var ignoreBoxX = x;
            var ignoreBoxY = y;
            var ignoreBoxScale = 0;
            do
            {
                var mixCode = null;
                var correction = false;
                for(var xx = x-1-ignoreBoxScale; xx<x+2+ignoreBoxScale; xx++)
                    for(var yy = y-1-ignoreBoxScale; yy<y+2+ignoreBoxScale; yy++)
                        if (xx>=0 && xx<this._tiles.length-1 && yy>=0 && yy<this._tiles[0].length-1)
                        {
                            if (xx>=ignoreBoxX-ignoreBoxScale && yy>=ignoreBoxY-ignoreBoxScale && xx<=ignoreBoxX+ignoreBoxScale && yy<=ignoreBoxY+ignoreBoxScale)
                            {}
                            else
                            {
                                var code = this._getTileCode(xx,yy);
                                if (this._tileCodes[code[0]+code[1]+code[2]+code[3]]==null)
                                {
                                    mixCode = this._getTerrainMixCode(xx, yy, primaryTerrainCode);
                                    if (this._correctTile(xx, yy, primaryTerrainCode, mixCode)==true)
                                        correction = true;
                                }
                                else
                                {
                                    // just randomly reroll the tile then...
                                    tileCodeVersions = this._tileCodes[code[0]+code[1]+code[2]+code[3]];
                                    version = 1;
                                    if (tileCodeVersions>1)
                                        version = random(1, tileCodeVersions);
                                    this._tiles[xx][yy][1] = version;

                                }
                        }
                    }
                primaryTerrainCode = mixCode;
                ignoreBoxScale = ignoreBoxScale +1;
            }
            while(correction==true);


        }
    };

    /**
     * This method will take the terrainCode that is being plotted at the given x/y
     * location as well as the mixCode we will be using and changes the tile to only
     * contain the given terrain codes. It will return true if the tile was actually
     * changed or false if no changes were necessary.
     * This method will also randomly choose the version of the tile to use (even if
     * no changes were necessary).
     * @param {int} x The X coordinate on the map specifying the tile we're correcting
     * @param {int} y The Y coordinate on the map specifying the tile we're correcting
     * @param {String} primaryCode The terrain code that is being plotted at this location
     * @param {String} mixCode The terrain code that is being mixed with the plotted terrain code
     * @return True|false; if the tile needed to be corrected, this method will return true.
     * @type boolean
     */
    this._correctTile = function(x, y, primaryCode, mixCode)
    {
        if (x<0 || y<0 || x>=this._tiles.length-1 || y>=this._tiles[0].length-1)
            return false;

        var changed = false;
        if (this._tiles[x][y][0]!=primaryCode && this._tiles[x][y][0]!=mixCode)
        {
            this._tiles[x][y][0] = mixCode;
            changed = true;
        }
        if (this._tiles[x+1][y][0]!=primaryCode && this._tiles[x+1][y][0]!=mixCode)
        {
            this._tiles[x+1][y][0] = mixCode;
            changed = true;
        }
        if (this._tiles[x+1][y+1][0]!=primaryCode && this._tiles[x+1][y+1][0]!=mixCode)
        {
            this._tiles[x+1][y+1][0] = mixCode;
            changed = true;
        }
        if (this._tiles[x][y+1][0]!=primaryCode && this._tiles[x][y+1][0]!=mixCode)
        {
            this._tiles[x][y+1][0] = mixCode;
            changed = true;
        }

        // Do the random tile image thing...
        var tileCodeVersions = this._tileCodes[this._tiles[x][y][0], this._tiles[x+1][y][0], this._tiles[x+1][y+1][0], this._tiles[x][y+1][0]];
        var version = 1;
        if (tileCodeVersions>1)
            version = random(1, tileCodeVersions);
        this._tiles[x][y][1] = version;
        

        return changed;
    };

    /**
     * This may be temporary.
     * Right now it's just for testing purposes.
     */
    this._doClick = function(event)
    {
        var tileX = Math.floor(this._engine.viewport.transformToActualPixelX(event.canvasX)/this._tileScale);
        var tileY = Math.floor(this._engine.viewport.transformToActualPixelY(event.canvasY)/this._tileScale);

        this.setRandomTile(tileX, tileY, this._terrainBrush, [this._terrainBrush,this._terrainBrush,this._terrainBrush,this._terrainBrush], true);

    };

    /**
     * This may be temporary.
     * Right now it's just for testing purposes.
     */
    this._doMouseMove = function(event)
    {
        if (this._engine._leftMouseDown==true)
        {
            var tileX = Math.floor(this._engine.viewport.transformToActualPixelX(event.canvasX)/this._tileScale);
            var tileY = Math.floor(this._engine.viewport.transformToActualPixelY(event.canvasY)/this._tileScale);

            this.setRandomTile(tileX, tileY, this._terrainBrush, [this._terrainBrush,this._terrainBrush,this._terrainBrush,this._terrainBrush], true);
        }
    };

    /**
     * Sets the brush we will be using for drawing new tiles on the map's surface
     * with the mouse.
     *
     * This may be temporary.
     * Right now it's just for testing purposes.
     * @param {String} terrainCode A valid terrain code
     * @see #_terrainCodes
     */
    this.setBrush = function(terrainCode)
    {
        this._terrainBrush = terrainCode;
    };

    /**
     * This retrieves the tile code string without the image version
     * (which is terrainCode+terrainCode+terrainCode+terrainCode) at the
     * given x/y coordinates.
     *
     * This method can be used to determine if a tile is walkable/passable or not.
     * 
     * @param {int} x The x coordinate on the map to retrieve the tile code for
     * @param {int} y The y coordinate on the map to retrieve the tile code for
     * @return An array of terrainCodes that make up the tile code for this coordinate including the image version
     * @type String[]
     */
    this.getTile = function(x,y)
    {
        if (x>=0 && y>=0 && x<this._tiles.length-1 && y<this._tiles[0].length-1)
            return [this._tiles[x][y][0], this._tiles[x+1][y][0], this._tiles[x+1][y+1][0], this._tiles[x][y+1][0],this._tiles[x][y][1]];
        else
            return null;
    };
}