function H5EMapObject(engine, layer, typeObject, tileX, tileY, tileSize)
{
    var self = this;
    
    // extend sprite...
    this._super = H5ESprite;
    if (typeObject==null)
        this._super(engine);
    else if (isInstanceOf(typeObject, "H5EMapObjectType"))
        this._super(engine, layer, typeObject.getSpriteType());


    this._mapObjectType = typeObject;
    this._tileX = null;
    this._tileY = null;
    this._tileSize = tileSize;

    this.getMapObjectType = function()
    {
        return this._mapObjectType;
    };

    this.getTileX = function()
    {
        return this._tileX;
    };

    this.getTileY = function()
    {
        return this._tileY;
    };


    this.setPosition = function(tileX, tileY)
    {

        if (tileX!=this._tileX || tileY!=this._tileY)
        {
            // Remove the old footprint from the map object layer...
            if (this._tileX!=null && this._tileY!=null)
            {
                var startX = this._tileX+Math.floor(this._mapObjectType.getAdjustX()/this._tileSize);
                var startY = this._tileY+Math.floor(this._mapObjectType.getAdjustY()/this._tileSize);
                for(var x = startX; x<startX+this._mapObjectType.getTileSizeX(); x++)
                    for(var y = startY; y<startY+this._mapObjectType.getTileSizeY(); y++)
                        if (this._mapObjectType._footprint[x-startX][y-startY]==true)
                        {
                            try
                            {
                                this._layer._footprintMap[x][y] = null;
                            }
                            catch(outOfBounds)
                            {
                                // Ignore out of bounds errors here for now
                            }
                        }
            }
            this._tileX = tileX;
            this._tileY = tileY;

            var startX = this._tileX+Math.floor(this._mapObjectType.getAdjustX()/this._tileSize);
            var startY = this._tileY+Math.floor(this._mapObjectType.getAdjustY()/this._tileSize);
            for(var x = startX; x<startX+this._mapObjectType.getTileSizeX(); x++)
                for(var y = startY; y<startY+this._mapObjectType.getTileSizeY(); y++)
                    if (this._mapObjectType._footprint[x-startX][y-startY]==true)
                    {
                        try
                        {
                            this._layer._footprintMap[x][y] = this;
                        }
                        catch(outOfBounds)
                        {
                            // Ignore out of bounds errors here for now
                        }
                    }

            // Now set the sprite's x/y position based on this...
            this.setX(tileX*this._tileSize+this._mapObjectType.getAdjustX());
            this.setY(tileY*this._tileSize+this._mapObjectType.getAdjustY());
        }
    };


    ///////////////////////////////////
    // Constructor area...
    ///////////////////////////////////

    this.setPosition(tileX, tileY);
}