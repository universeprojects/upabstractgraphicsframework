/**
 * Construct a new H5EImage object.
 * @class This is a wrapper for the HTMLImageElement class and is used in the H5E
 * framework for image data.
 * @constructor
 * @param {H5EEngine} engine The engine object that this image belongs to
 * @return A new image
 */
function H5EImage(engine)
{
    // Defining the engine class...
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine=engine;
    /**The actual javascript image element @type HTMLImageElement*/
    this._image= new Image();
    /**This is the original url used to find the image. It is likely a relative path @type String*/
    this._url = null;

    /**
     * This triggers the load of the image and sets up the events to fire when
     * it is finished, errored out or aborted.
     * @param {String} src The url pointing to the image to load
     * @param {function} onCompleteEvent The event that is fired when the image is finished loading
     * @param {function} onErrorEvent The event that is fired when the image load results in an error
     * @param {function} onAbortEvent The event that is fired when the image load is aborted by the user clicking on the stop button
     */
    this.loadImage=function(src, onCompleteEvent, onErrorEvent, onAbortEvent)
    {
        this._url = src;
        this._image.onerror = onErrorEvent;
        this._image.onabort = onAbortEvent;
        this._image.onload = onCompleteEvent;
        this._image.src = src;
    };

    /**
     * Returns the javascript image object that this class is wrapping.
     * @return The javascript image that this class wraps
     * @type HTMLImageElement
     */
    this.getJSImage=function()
    {
        return this._image;
    };

    /**The height of the loaded image data. If no image is loaded, the return will be 0.
     * @return The height of the loaded image data. If no image is loaded, the return will be 0.
     * @type int
     */
    this.getHeight=function()
    {
        if (this._image==null)
            return 0;
        return this._image.height;
    };

    /**The width of the loaded image data. If no image is loaded, the return will be 0.
     * @return The width of the loaded image data. If no image is loaded, the return will be 0.
     * @type int
     */
    this.getWidth=function()
    {
        if (this._image==null)
            return 0;
        return this._image.width;
    };

    /**The full url used to load the image
     * @return The original url used to load the image
     * @type String
     */
    this.getUrl=function()
    {
        return this._image.src;
    };

    /**
     * The original url used to load the image. It is likely a relative
     * url.
     * @return The original url string used when loadImage() was called
     * @type String
     */
    this.getOriginalUrl = function()
    {
        return this._url;
    };
}