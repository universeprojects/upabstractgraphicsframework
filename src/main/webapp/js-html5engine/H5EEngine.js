var h5eEngines = new Array();

/**
 * @constructor
 * Construct a new Engine object that wraps the HTML5 canvas element. 
 * @class This is the central class for the html5 engine. This object will create
 * a new canvas element within the container that matches the given containerId
 * (where containerId is the element's id; the element you wish to contain the new canvas
 * element). The engine also contains the onTick event that fires whenever processing
 * should take place (like animations or any other processing that must happen in real-time).
 * It will also manage graphical draws ensuring that the processing "ticks" are all caught up
 * before a frame is drawn.
 *
 * @param {String} containerId The ID of the element (as found in the HTML page) that will be the container for the html5 canvas objects that this engine controls
 * @param {int} sizeX The width of the canvas to be generated
 * @param {int} sizeY The height of the canvas to be generated
 * @param {int} targetFps The target redraw speed for this engine. Usually setting an target FPS to greater than 60 is unnecessary since most monitors won't display refresh rates beyond 60 anyway. This acts as a frame rate limiter and if the engine cannont make the targetFps, the frames will simply be dropped.
 * @param {int} targetTps The target ticks-per-second that we would like to ensure occurs. Even though the FPS may not always hit it's target, the engine will ensure the number of ticks per second always succedes, even if that means locking up the browser to catch up. Use the onTick event to do your processing that needs to occur once per tick.
 * @param {boolean} doubleBuffered True|False. Turn double buffering on or not. The implementation of double buffering will slow the frame rate but it may improve visual quality in some situations. Decide if you need it on a case by case basis.
 * @param {boolean} runNow True|False. Should the engine start the "tick" process immediately after creation. If you need to load resources, you may wish set this to false and start the engine when everything has been loaded in already.
 * @param {boolean} enableFrameSkip Frameskip should pretty well always be turned on. It ensures that graphical frames will be skipped in favor of tick processing. If you have animation in your project, turning this off will cause the animations to slow down.
 * @param {boolean} enableDebugMode A line of text will appear at the top of the screen with some debugging information.
 * @return The H5EEngine object that you may use for your project.
 *
 */
function H5EEngine(containerId, sizeX, sizeY, targetFps, targetTps, doubleBuffered, runNow, enableFrameSkip, enableDebugMode)
{
    // Defining the engine class...
    // Major objects...
    /** The resource manager for this engine. Accessible via the getResourceManager() method. @type H5EResourceManager*/
    this._resourceManager=null;

    // Operating variables...
    /**The viewport object controls how the layers get displayed. Viewports can be zoomed, translated and possibly rotated. A viewport can be thought of as a camera looking at the layers. @type H5EViewport*/
    this._viewport = new H5EViewport(this);
    /** A list of graphical layers (H5ELayer) that this engine maintains @type H5ELayer[]*/
    this.layers = [];
    /** The id of the element on the webpage that will contain our canvas/canvases @type String*/
    this._containerId = containerId;
    /** The element that will act as the container for our canvas/canvases. @type HTMLElement*/
    this._container = null;
    /** The FPS we would like to achieve @type int*/
    this._targetFps = targetFps;
    /** The ticks per second we would like to achieve @type int*/
    this._targetTps = targetTps;
    /** Enables or disables the 'frameskip' feature. Frame skip will attempt to execute the onTick event at the requested target framerate and skip drawing graphical frames to do it. Making this value false will make the game operations dependant on frame rate. @type boolean*/
    this.frameSkip= enableFrameSkip;
    /** By default, double buffering is off until we get to the constructor bit @type boolean*/
    this._doubleBuffered = false;
    /** By default, the engine is off (paused) @type boolean*/
    this._running = false;
    /** Will be used as the 'backbuffer' when double buffering or as the main surface when not @type CanvasRenderingContext2D*/
    this._context = null;
    /** Will be used as the 'backbuffer' when double buffering or as the main surface when not @type HTMLCanvasElement*/
    this._canvas = null;
    /** When double buffering, this ends up being the visible surface while the above vars are actually what is drawn on @type HTMLCanvasElement*/
    this._frontbufferCanvas = null;
    /** When double buffering, this ends up being the visible surface while the above vars are actually what is drawn on @type CanvasRenderingContext2D*/
    this._frontbufferContext = null;
    /** This stores the index that this engine is in the global h5eEngines array @type int*/
    this._engineIndex = -1;
    /** This stores the 'setInterval' ID so we can cancel it when we want @type String*/
    this._mainLoopId = null;
    /** If this is turned on, a line of text will appear across the top of the canvas with some debugging details @type boolean*/
    this._debugMode=enableDebugMode;
    /** Keeps track of the engine's validity. If this field is true, a sub-component has been marked as invalid and needing to be redrawn. The next time a graphics pass is to take place, the engine will redraw the necessary components. @type boolean*/
    this._invalid= true;

    // Frameskip related vars
    /** This holds the time the last graphic frame was drawn (in milliseconds) @type int*/
    this._lastFrameDraw= 0;      
    /** The number of skipped graphical frames so far @type int*/
    this._skippedFrames=0;

    // FPS related vars...
    /** The current fps as maintained by the _mainLoop() method @type float*/
    this._fps=0;
    /** The last time we calculated FPS so we can determine the latest FPS accurately @type int*/
    this._lastFPSSecond= new Date().getUTCSeconds();
    /** The number of frames that have been drawn since the last time we calculated FPS @type int*/
    this._fpsFrameCount= 0;

    // Mouse related vars...
    /** Is the left mouse button down? A number of methods and events are used to keep track of this. @type boolean*/
    this._leftMouseDown= false;
    /** Is the right mouse button down? A number of methods and events are used to keep track of this. @type boolean*/
    this._rightMouseDown= false;
    /** This keeps track of the sprite we're currently over. The only reason why we're keeping track of this is because we don't do bubble ups for mouse events right now. That may change. @type H5ESprite*/
    this._spriteOver = null;



    // Events...
    /**
     * This event is fired every time the engine wants to process a new tick (all operations (like animations, input checking..etc) excluding graphical drawing should be done here)
     * @type function
     */
    this.onTick = null;
    /**
     * Any time the user clicks on the engine's canvas surface, this event is fired.
     * This event will pass a single parameter (the mouse event object) which is the same
     * object that standard html javascript uses for mouse events except it also includes
     * some additions to make cross-browser mouse operations easier.
     * So far, the additions are...
     * canvasX - The x position the user clicked on the canvas. (0,0 is top left)
     * canvasY - The y position the user clicked on the canvas. (0,0 is top left)
     * @type function
     */
    this.onClick= null;
    /**
     * Any time the user moves the mouse on the engine's canvas surface, this event is fired.
     * This event will pass a single parameter (the mouse event object) which is the same
     * object that standard html javascript uses for mouse events except it also includes
     * some additions to make cross-browser mouse operations easier.
     * So far, the additions are...
     * canvasX - The x position the user clicked on the canvas. (0,0 is top left)
     * canvasY - The y position the user clicked on the canvas. (0,0 is top left)
     * @type function
     */
    this.onMouseMove= null;

    /**
     * @return Returns a boolean, true|false, for if this engine is invalid (needs to be redrawn).
     * @type boolean
     * @see #invalidate
     */
    this.isInvalid=function()
    {
        return this._invalid;
    };

    /**
     * Sets this engine to invalid. (graphical components to be redrawn).
     * Invalidating the engine directly will not actually cause al sub-components to redraw.
     * It will simply cause the engine to check sub components if they have been
     * marked invalid and redraws them only in that case. Sub-components automatically
     * mark their parents as invalid.
     */
    this.invalidate=function()
    {
        this._invalid = true;
    };

    /**
     * Sets the target fps for this instance of the engine.
     * @param {int} targetFps The fps that you wish the engine to attempt to achieve.
     */
    this.setTargetFps=function(targetFps)
    {
        this._targetFps = targetFps;
        if (this._running)
        {
            // Re-running will re-schedule the interval at the new target fps
            this.pause();
            this.run();
        }
    };

    /**
     * @return the target FPS that this engine instance will attempt to achieve.
     * @type int
     */
    this.getTargetFps=function()
    {
        return this._targetFps;
    };

    this.isRunning = function()
    {
    	return this._running;
    };
    
    /**
     * Sets true or false determining if this engine instance will use
     * double buffering techniques. When this is true, all drawing is done
     * on a backbuffer before it is drawn on the canvas. Otherwise, drawing
     * is done directly to the canvas control.
     * @param {boolean} value
     */
    this.setDoubleBuffered=function(value)
    {
        if (value!=this._doubleBuffered)
        {
            if (value==true)
            {
                this._frontbufferCanvas = document.createElement("CANVAS");
                try
                {
                    G_vmlCanvasManager.initElement(this._frontbufferCanvas);
                }catch(e)
                {
                    // Ignore this. If this fails, it's because we're not using an IE browser
                }
                this._frontbufferCanvas.style.position = "absolute";
                this._frontbufferCanvas.style.width = "100%";
                this._frontbufferCanvas.style.height = "100%";
                this._frontbufferCanvas.width = this._canvas.width;
                this._frontbufferCanvas.height = this._canvas.height;
                this._canvas.style.visibility="hidden";
                this._frontbufferCanvas.style.visibility="visible";
                // Now add the frontbuffer to the parent of whatever the canvas is on...
                this._canvas.parentNode.appendChild(this._frontbufferCanvas);
                // Add the engine field to the canvas itself so we can get
                // the engine object on canvas events
                this._frontbufferCanvas.engine = this;
                // And get the context of the frontbuffer so we can use it for drawing later when we do paging
                this._frontbufferContext = this._frontbufferCanvas.getContext("2d");
                this._frontbufferCanvas.onclick = this._doClickEvent;
                this._frontbufferCanvas.onmousemove = this._doMouseMoveEvent;
                this._frontbufferCanvas.onmousedown = this._doMouseDownEvent;
                this._frontbufferCanvas.onmouseup = this._doMouseUpEvent;
            }
            else
            {
                // If we were using double buffering, remove the "front buffer" from the page
                // since we don't need it anymore
                if (this._frontbufferCanvas != null)
                {
                    this._canvas.parentNode.removeChild(this._frontbufferCanvas);
                }
                // And now nullify the frontbuffer so we cant use it without an exception
                this._frontbufferCanvas = null;
                this._frontbufferContext = null;
            }
            this._doubleBuffered = value;
        }
    };

    /**
     * This private method will take a mouse event object and add canvasX and
     * canvasY fields to it. These fields are the x,y co-ordinates that the
     * mouse was clicked, moved to..etc on the canvas itself.
     * @param e This is the standard javascript mouse event object that is given when a mouse event occurs
     * @return The same object that was passed in but with some additional fields
     */
    this._enhanceMouseEvent=function(e)
    {
        var obj = this._canvas;
        var x = obj.offsetLeft;
        var y = obj.offsetTop;
        while (obj.offsetParent!=null)
        {
            obj = obj.offsetParent;
            x+=obj.offsetLeft;
            y+=obj.offsetTop;
        }

        
        try
        {
            e.canvasX = e.layerX;
            e.canvasY = e.layerY;
        }catch(e)
        {
            e.canvasX = e.offsetX;
            e.canvasY = e.offsetY;
        }
        return e;
    };


    /**
     * This method is fired when a mouse click occurs on the canvas. This is where the
     * click event begins it's propagation to the rest of the controls (layers and sprites..etc).
     *
     * This method is incomplete and must be revised.
     *
     * @param e The original mouse click event as we received it from the canvas element.
     */
    this._doClickEvent=function(e)
    {
        var engine = this.engine;
        var enhancedE = engine._enhanceMouseEvent(e);

        // Do the canvas level click event
        if (engine.onClick!=null)
            engine.onClick(enhancedE);

        // Go through each sprite and do a hit test if they have an onclick event
        var spriteClicked = false;
        for(var layerIndex = engine.layers.length-1; layerIndex>=0; layerIndex--)
        {
            var layer = engine.layers[layerIndex];
            if (spriteClicked==false)
                for(var spriteIndex = 0; spriteIndex<layer.getSprites().length; spriteIndex++)
                {
                    var sprite = layer.getSprites()[spriteIndex];
                    if (sprite.onClick!=null)
                    {
                        // Do sprite hit test
                        var hitTest = sprite.hitTest(enhancedE.canvasX, enhancedE.canvasY);
                        if (hitTest==true)
                        {
                            sprite.onClick(enhancedE);
                            spriteClicked=true;
                            break;
                        }
                    }
                }

            // Now do the layer level click event...
            // If a click was found here, we will stop the bubble up of events
            // TODO_OLD: Review if this is good or not. I think NO
            if (layer._doClick!=null)
            {
                layer._doClick(enhancedE);
                break;
            }
        }
    };

    /**
     * This method is fired when a mouse move occurs on the canvas. This is where the
     * move event begins it's propagation to the rest of the controls (layers and sprites..etc).
     *
     * This method is incomplete and must be revised.
     *
     * @param e The original mouse move event as we received it from the canvas element.
     */
    this._doMouseMoveEvent=function(e)
    {
        var engine = this.engine;
        var enhancedE = engine._enhanceMouseEvent(e);

        // Do the canvas level click event
        if (engine.onMouseMove!=null)
            engine.onMouseMove(enhancedE);

        // Go through each sprite and do a hit test if they have an onmousemove, onmouseenter or onmouseexit event
        var spriteTriggered = false;
        for(var layerIndex = engine.layers.length-1; layerIndex>=0; layerIndex--)
        {
            var layer = engine.layers[layerIndex];
            if (spriteTriggered==false)
            for(var spriteIndex=0; spriteIndex<layer.getSprites().length; spriteIndex++)
            {
                var sprite = layer.getSprites()[spriteIndex];
                if (spriteTriggered==false && (sprite.onMouseMove!=null || sprite.onMouseEnter!=null || sprite.onMouseExit!=null))
                {
                    // Do sprite hit test
                    var hitTest = sprite.hitTest(enhancedE.canvasX, enhancedE.canvasY);
                    if (hitTest==true)
                    {
                        if (sprite.onMouseMove!=null)
                            sprite.onMouseMove(enhancedE);
                        if (this._spriteOver!=sprite)
                        {
                            if (this._spriteOver!=null && this._spriteOver.onMouseExit!=null)
                                this._spriteOver.onMouseExit(enhancedE);
                            if (sprite.onMouseEnter!=null)
                                sprite.onMouseEnter(enhancedE);
                        }
                        this._spriteOver=sprite;
                        spriteTriggered=true;
                        break;
                    }
                }
            }

            // Now do the layer level click event...
            // If a click was found here, we will stop the bubble up of events
            // TODO_OLD: Review if this is good or not. I think NO
            if (layer._doMouseMove!=null)
            {
                layer._doMouseMove(enhancedE);
                break;
            }
        }

        // If we reach this point and we haven't triggered a sprite, we are no longer 'over' any sprites
        // and we should call onMouseExit on the one we were over (if we were over one before).
        if (spriteTriggered==false && this._spriteOver!=null)
        {
            if (this._spriteOver.onMouseExit!=null)
                this._spriteOver.onMouseExit(enhancedE);
            this._spriteOver = null;
        }
    };

    /**
     * This method is fired when a mouse down event occurs on the canvas. This is where the
     * down event begins it's propagation to the rest of the controls (layers and sprites..etc).
     *
     * This method is incomplete and must be revised.
     *
     * @param e The original mouse down event as we received it from the canvas element.
     */
    this._doMouseDownEvent=function(e)
    {
        var engine = this.engine;
        var enhancedE = engine._enhanceMouseEvent(e);

        // Do the canvas level click event
        if (engine.onMouseDown!=null)
            engine.onMouseDown(enhancedE);

        if (e.button==0)
            engine._leftMouseDown=true;
        if (e.button==2)
            engine._rightMouseDown=true;
    };

    /**
     * This method is fired when a mouse up event occurs on the canvas. This is where the
     * up event begins it's propagation to the rest of the controls (layers and sprites..etc).
     *
     * This method is incomplete and must be revised.
     *
     * @param e The original mouse up event as we received it from the canvas element.
     */
    this._doMouseUpEvent=function(e)
    {
        var engine = this.engine;
        var enhancedE = engine._enhanceMouseEvent(e);

        // Do the canvas level click event
        if (engine.onMouseUp!=null)
            engine.onMouseUp(enhancedE);

        if (e.button==0)
            engine._leftMouseDown=false;
        if (e.button==2)
            engine._rightMouseDown=false;
    };

    /**
     * Returns true/false if this canvas is double buffered. Double buffering
     * enables smooth animations.
     * @return Whether or not double buffering is currently turned on.
     * @type boolean
     */
    this.isDoubleBuffered=function()
    {
        return this._doubleBuffered;
    };

    /**
     * When using double buffered mode, the main loop will automatically call this
     * method. The method will simply perform a page flip by drawing the main canvas
     * onto the frontbuffer canvas.
     */
    this._pageFlip=function()
    {
        var canvasData = this._context.getImageData(0, 0, this._canvas.width, this._canvas.height);
        this._frontbufferContext.putImageData(canvasData, 0,0);
    };

   /**
    * If the engine has been paused or if it has not been started yet, this will
    * start it.
    */
    this.run=function()
    {
        if (this._running==false)
        {
            // Schedules the regular firing of the mainLoop() method here in this class
            this._mainLoopId = setInterval("h5eEngines["+this._engineIndex+"]._mainLoop()", 1000/this._targetFps);

            // Set the running variable to true to let everyone know whats up
            this._running = true;
        }
    };

    /**
     * This will be called once every frame when the engine is in a running state.
     * @see #run
     */
    this._mainLoop=function()
    {
        // Implement the frameskip functionality here if it's turned on
        if (this.frameSkip)
        {
            // If we're using frameskip, we can't have last draw be 0, obviously
            if (this._lastFrameDraw == 0)
                this._lastFrameDraw = new Date().getTime();

            var millisPerFrame = Math.floor(1000/this._targetTps);
            var now = null;
            // Determine how many ticks we need to execute
            now = new Date().getTime();
            var timeDiff = now-this._lastFrameDraw;
            var neededTicks = Math.floor(timeDiff/(millisPerFrame));
            var ticksProcessed = 0;
            for(var i = 0; i<neededTicks; i++)
            {
                if(this.onTick!=null)
                    this.onTick();

                // Update the behaviours of sprites...
                for(var layerIndex=0; layerIndex<this.layers.length; layerIndex++)
                {
                    var layer = this.layers[layerIndex];
                    for(var spriteIndex=0; spriteIndex<layer.getSprites().length; spriteIndex++)
                    {
                        var sprite = layer.getSprites()[spriteIndex];
                        for(var bIndex=0; bIndex<sprite._behaviours.length; bIndex++)
                        {
                            var behaviour = sprite._behaviours[bIndex];
                            behaviour.update(sprite);
                        }
                    }
                }
                ticksProcessed++;

                // This will ensure the GUI doesn't lock up by exiting the method every
                // 1000 ticks of "catch-up"
                if (ticksProcessed>1000)
                    break;
            }
            // keep track of the skipped frames
            if (neededTicks>1)
                this._skippedFrames += neededTicks-1;

            // We are only going to remember the last frame draw time if we've actually caught up
            // If we haven't caught up yet, we're going to only do 100 operation cycles before we
            // allow the browser to execute any events it needs.
            // Instead, we will simply add on the number of cycles we did end up doing
            // to the lastFrameDraw time.
            this._lastFrameDraw += neededTicks*millisPerFrame;

            // Now exit early to ensure we don't lock up the browser and instead of drawing the graphics,
            // this method will be executed again for further catch-up
            if (ticksProcessed>1000)
                return;
        }
        else    //Otherwise, turn it off
        {
            if (this.onTick!=null)
                this.onTick();
        }

        // Clear the canvas
        if (this._invalid)
        {
            this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
            // Now redraw anything that is marked as invalid...
            for(var layerIndex=0; layerIndex<this.layers.length; layerIndex++)
            {
                layer = this.layers[layerIndex];
//                    if (layer.isVisible() && layer.isInvalid())
                if (layer.isVisible())
                    layer.drawSelfTo(this._context);
            }

            // Keep track of how many graphical frames we render...
            this._fpsFrameCount++;
            var thisSecond = new Date().getTime();
            if (thisSecond>this._lastFPSSecond+1000)
            {
                this._fps = this._fpsFrameCount/((thisSecond-this._lastFPSSecond)/1000);
                if (this._fps>0)
                    this._fps = Math.floor(this._fps);
                this._fpsFrameCount = 0;
                this._lastFPSSecond = thisSecond;
            }

            // Now draw debug info if it is enabled...
            if (this._debugMode)
            {
                this._context.font = 'bold 16px sans-serif';
                this._context.strokeText("FPS: "+this._fps+" Frames Skipped: "+this._skippedFrames, 0, 16);
            }

            // In a double buffered situation, we will now do a pageflip
            if (this._doubleBuffered)
                this._pageFlip();

            this._invalid=false;
        }

    };

    /**
     * This stops the engine from running. Stops all graphics and input.
     * Essentially freezes the canvas control without freezing the browser.
     */
    this.pause=function()
    {
        if (this._running==true)
        {
            // Cancels (pauses) the mainLoop() method from firing regularily
            clearInterval(this._mainLoopId);


            // Sets the running variable to false to let everyone know whats up
            this._running=false;
        }
    };

    /**
     * Gets the resource manager for this engine.
     * The resource manager allows full access to image/audio resources that
     * get loaded in.
     * @return The resource manager for this engine. There is only one.
     * @type H5EResourceManager
     */
    this.getResourceManager=function()
    {
        return this._resourceManager;
    };

    /**
     * Creates a new layer and appends it to the list of layers.
     * By default, it will be on top of all other layers.
     * The layer it creates is just the most basic H5ELayer object which is
     * usually used to contain sprites.
     * @return The newly created H5ELayer
     * @type H5ELayer
     */
    this.newLayer=function()
    {
        var layer = new H5ELayer(this);
        this.layers.push(layer);
        return layer;
    };


    /**
     * This allows you to add a custom layer object to the list of layers in the engine.
     * An example of this would be the H5ETileMap object which extends the H5ELayer.
     * Adding a tile map would be done through this method once it has been created.
     * @param {H5ELayer} layerObject The H5ELayer compatible object that you have created.
     */
    this.addLayer=function(layerObject)
    {
        this.layers.push(layerObject);
    };

    /**
     * This will remove a layer from the engine. All objects (sprites..etc) attached
     * to the removed layer will not be destroyed and the layer can be added again later.
     * @param {int} layerIndex The index of the layer to be removed
     * @return The layer object that was removed
     * @type H5ELayer
     */
    this.removeLayer = function(layerIndex)
    {
        return this.layers.splice(layerIndex, 1);
    };

    /**
     * This will clear all layers and objects within each layer. The default
     * background layer (at index 0) will be re-created and the control will be
     * redrawn.
     */
    this.clearAllLayers = function()
    {
        // Clear the canvases..
        if (this._context!=null)
            this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
        if (this._frontbufferContext!=null)
            this._frontbufferContext.clearRect(0, 0, this._frontbufferCanvas.width, this._frontbufferCanvas.height);

        // Remove all layers and recreate the default layer...
        this.layers = [];
        this.newLayer();

        // Trigger a redraw
        this.invalidate();
    };

    this.getWidth = function()
    {
        return this._canvas.width;
    }

    this.getHeight = function()
    {
        return this._canvas.height;
    }

    this.setSize = function(width, height)
    {
        this._canvas.style.height = height+"px";   // Set the width and height to fill the container
        this._canvas.style.width = width+"px";
        this._canvas.width = width;
        this._canvas.height = height;
    }

    /**
     * This will destory all elements in the library and create a new one. Use this
     * if you wish to create a new library.
     */
    this.clearResourceManager = function()
    {
        this._resourceManager = new H5EResourceManager(this);
    }
    
    /**
     * Returns the viewport this engine is currently using. A viewport is used
     * as a "camera", use the viewport's "translateViewport" function to move 
     * the "camera".
     *
     * @return {H5EViewport} The viewport this engine is currently using.
     */
    this.getViewport = function() 
    {
        return this._viewport;
    }

    /**################################################
     * This section serves as a constructor area...
     */
    // Get the container element
    this._container = document.getElementById(containerId);
    if (this._container==null)
        throw("The elementId '"+containerId+"' passed into the createH5E() to act as the canvas container was not found.");
    this._container.style.position = "relative";  // The container needs to be relative for overlapping to work. The canvases will be absolutely positioned
    // Now create the canvas...
    this._canvas = document.createElement("CANVAS");
    try
    {
        if ((typeof(G_vmlCanvasManager) == "undefined")==false)
            G_vmlCanvasManager.initElement(this._canvas);
    }
    catch(e)
    {
        // Ignore this. If this fails, it's because we're not using an IE browser
    }
    this._container.appendChild(this._canvas);  // Add it to the container that was passed in
    this._canvas.style.position = "absolute";
    this.setSize(sizeX, sizeY);
    this._canvas.engine = this;

    this._canvas.onclick = this._doClickEvent;
    this._canvas.onmousemove = this._doMouseMoveEvent;
    this._canvas.onmousedown = this._doMouseDownEvent;
    this._canvas.onmouseup = this._doMouseUpEvent;
   // Grab the context now for later
    this._context = this._canvas.getContext('2d');

    // Lets create the resource manager now...
    this._resourceManager = new H5EResourceManager(this);

    // Now create the default layer (layer 0; the background layer; the main layer)
    var layer = this.newLayer();

    // Now that the main canvas is setup, turn on double buffering if necessary...
    this.setDoubleBuffered(doubleBuffered);

    // Now add this instance to the array of engines we keep. This must be done before the
    // .run() method is called because setInterval will use this to get the engine again.
    h5eEngines.push(this);
    this._engineIndex = h5eEngines.length-1;

    

    // If yes, the developer has requested that the control start running right away
    if (runNow)
        this.run();

    //################################################


}

/**
 * This will accept primitives or objects and test them for type.
 * This replaces instanceof (since instanceof does not work with primitives)
 *
 * @param obj The object you are testing
 * @param {String} classType The string (text) name of the class you think this object should be (eg. String, Number...etc)
 * @return True or false if the obj passed in is of the classType given.
 * @type boolean
 */
function isInstanceOf(obj, classType)
{
  return (obj instanceof eval("("+classType+")")) || (typeof obj == classType.toLowerCase());
}


/**
 * This method will clean up a string that is to be used as a key so that we can use the
 * javascript Object class as an associative array.
 * This method will also replace all non alphanumeric characters with a dash (-).
 * @param {String} str Any string that is to be turned into a valid key.
 * @return A string that will be compatible as a method name in a javascript object. (This is how simple javascript 'hash' objects work though, it's kind of a hack)
 * @type String
 */
function stringToKey(str)
{
    var key = "key-"+str.replace(/[^0-9A-Za-z]/g, "-");
    return key;
}


/**
 * Generates a random number using the from and to parameters inclusive.
 * @param {int} from The starting point of the random range
 * @param {int} to The ending point of the random range
 * @return A random number between the from and to parameters given (including the from and two values)
 * @type int
 */
function random(from, to)
{
    return Math.floor(Math.random()*(to-from+1))+from;
}

// Adds support for the Array.indexOf function in IE
if(!Array.indexOf)
{
    Array.prototype.indexOf = function(obj){
        for(var i=0; i<this.length; i++){
            if(this[i]==obj){
                return i;
            }
        }
        return -1;
    }
}


function trim(text)
{
    if (text==null)
        return null;
    else
        return text.replace(/(^\\s+|\\s+$)/, "");
}