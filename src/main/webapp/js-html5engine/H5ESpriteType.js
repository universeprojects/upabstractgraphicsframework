/**
 * Construct a new H5ESpriteType object.
 * @class A sprite uses a SpriteType object for it's image data. The reason why
 * we don't simply use an H5EImage is simply because the SpriteType specifies
 * the area on the image from which to get the source data. This means that images
 * may contain multiple sprite image datas in order to shorten loading times and
 * reduce image data overhead. For example, a single image can contain all the animation
 * frames for a character and a SpriteType would be specified for each frame in the
 * animation and the sprite type would include the area on the image from which to
 * source the image data. In order to facilitate an animation, one only needs to
 * continuously change sprite types on a sprite. This way, all other settings on the
 * sprite is maintained (including events fired from the sprite itself) and all that
 * is changed is the image data source (which may even point to the same image but
 * a different part containing a different frame in the animation).
 * @constructor
 * @param {H5EEngine} engine The engine object that this sprite type belongs to
 * @param {String} imageKey The image key that points to an image this sprite type will use for it's its graphical data
 * @return A new sprite type object
 */
function H5ESpriteType(engine, imageKey)
{
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine=engine;
    /**The key that points to an image that is being used for this sprite type @type String*/
    this._imageKey=imageKey;
    /**The canvas element that may have been created by a call to getCanvas(). This would only be used if the user needed to get the pixel data for an image. @type HTMLCanvasElement*/
    this._canvas=null;
    /**The context object that may have been created by a call to getContext(). This would only be used if the user needed to get the pixel data for an image. @type CanvasRenderingContext2D*/
    this._context=null;

    /**When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the image to use @type int*/
    this.areaX=0;  
    /**When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the image to use @type int*/
    this.areaY=0;     
    /**When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the image to use @type int*/
    this.areaWidth=0;   
    /**When this sprite is going to be drawn, we use the area (areaX, areaY, areaWidth..etc) to specify what part of the image to use @type int*/
    this.areaHeight=0;  


    /**
     * This method will change the image key to the image the sprite is using. The next time
     * this sprite is drawn, the new image will be displayed.
     * @param {String} imageKey The image key that will be used for sprite type's image data acquisition
     */
    this.setImage=function(imageKey)
    {
        this._imageKey = imageKey;
    };


    /**
     * Returns the image being used for this sprite type.
     * If the image does not exist or is not loaded, this method will return null.
     * @return The H5EImage currently being used for this sprite type's image data
     * @type H5EImage
     */
    this.getImage=function()
    {
        var img = this._engine.getResourceManager().getImage(this._imageKey);
        return img;
    };

    /**
     * This method will return a new canvas for this sprite type for the purpose
     * of acquiring the per-pixel image data. If the sprite type has already
     * created the canvas once, the same canvas will be used rather than creating
     * a new one to save processing cycles.
     * @return A canvas with the sprite type's image data
     * @type HTMLCanvasElement
     */
    this.getCanvas=function()
    {
        if (this._canvas==null)
        {
            this._canvas = document.createElement("canvas");
            this._canvas.width = this.areaWidth;
            this._canvas.height = this.areaHeight;
        }
        return this._canvas;
    };

    /**
     * This method will return a new context for this sprite type for the purpose
     * of acquiring the per-pixel image data. If the sprite type has already
     * created the canvas and context once, the same canvas and context will be
     * used rather than creating a new one to save processing cycles.
     * @return A context with the sprite type's image data already applied to it
     * @type CanvasRenderingContext2D
     */
    this.getContext=function()
    {
        if (this._context==null)
        {
            var canvas = this.getCanvas();
            this._context = canvas.getContext("2d");
            this._context.drawImage(this.getJSImage(), this.areaX, this.areaY);
        }
        return this._context;
    };


    /**
     * Returns the javascript Image object that contains the image data. This
     * is what must be used when drawing using context.drawImage().
     * @return The actual image element that contains the image for this sprite type
     * @type HTMLImageElement
     */
    this.getJSImage=function()
    {
        return this.getImage().getJSImage();
    };


    /**
     * Returns the width for this sprite type. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     * @return The sprite type's width as it would be if it were drawn on a layer
     * @type int
     */
    this.getWidth=function()
    {
        return this.areaWidth;
    };

    /**
     * Returns the height for this sprite type. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     * @return The sprite type's height as it would be if it were drawn on a layer
     * @type int
     */
    this.getHeight=function()
    {
        return this.areaHeight;
    };

    /**
     * Saves this object to a file (the file parameter is actually just a string
     * that will be 'saved' later). This method simply adds this object's data
     * to the 'file'.
     * Most objects can be saved this way. They all use a function that looks
     * like this to add their data to the file.
     * 
     * @param {String} file The 'file' data that we will be adding to.
     * @return The modified file string
     * @type String
     */
    this._saveTo = function(file)
    {
        file += "H5ESpriteType:\n";
        
        // Save the sprite type key as it is in the resource manager
        var index = this._engine.getResourceManager()._spriteTypes.getValues().indexOf(this);
        var key = "";
        if (index!=-1)
            key = this._engine.getResourceManager()._spriteTypes.getKeys()[index];
        file += key+"\n";

        // Save the sprite type's IMAGE key
        file += this._imageKey+"\n";

        // Save the area this sprite type uses on the image
        file += this.areaX+"\n";
        file += this.areaY+"\n";
        file += this.areaWidth+"\n";
        file += this.areaHeight+"\n";
        return file;
    };


    //****************************************
    // This section acts as a constructor for the sprite object
    //

    // By default, we will set the area used to cover the entire image...
    if (this.getImage()!=null)
    {
        this.areaHeight = this.getImage().getHeight();
        this.areaWidth = this.getImage().getWidth();
    }
    //****************************************

}

/**
 * Creates a new H5ESpriteType in the given resourceManager from the data
 * located in the file (lines) starting from the given line number (lineNum).
 * @param {String[]} lines The file data
 * @param {int} lineNum The current position in the file data
 * @param {H5EResourceManager} resourceManager The resource manager to create this sprite type in
 * @return The new line position in the file. The line position will always be the 'next' line
 * in the file. In other words, there is no need to increment the line number once this method returns.
 * @type int
 */
H5ESpriteType.createFromFile = function(lines, lineNum, resourceManager)
{
    var spriteTypeKey = lines[lineNum];
    lineNum++;
    var imageKey = lines[lineNum];
    lineNum++;

    var areaX = lines[lineNum];
    lineNum++;
    var areaY = lines[lineNum];
    lineNum++;
    var areaWidth = lines[lineNum];
    lineNum++;
    var areaHeight = lines[lineNum];
    lineNum++;
    var spriteType = new H5ESpriteType(resourceManager._engine, imageKey);
    resourceManager.addSpriteType(spriteTypeKey, spriteType);
    spriteType.areaX = parseInt(areaX);
    spriteType.areaY = parseInt(areaY);
    spriteType.areaWidth = parseInt(areaWidth);
    spriteType.areaHeight = parseInt(areaHeight);
    return lineNum;
};
