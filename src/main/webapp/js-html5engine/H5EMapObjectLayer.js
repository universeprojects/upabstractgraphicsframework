function H5EMapObjectLayer(engine, tileSizeX, tileSizeY, tileScale)
{
    var self = this;

    // Inherit from h5eBehaviour...
    this._super = H5ELayer;
    this._super(engine);

    // Operating variables...
    this.mapObjects = new Array();
    this.sprites = this.mapObjects;
    this._tileSize = tileScale;
    /**The footprint map keeps track of 'walkable'/'buildable' areas that are/are-not being obstructed by something. @type Array*/
    this._footprintMap = [[]];

    this.getTileSize = function()
    {
        return this._tileSize;
    };

    /**
     * Appends the given sprite object to the list of GraphicElements in this layer.
     */
    this.addSprite=function(sprite)
    {
        throw("MapObjectLayer does not support sprites. Please use addMapObject() and instead, add map objects.");
    };

    this.addMapObject = function(mapObject)
    {
        this.mapObjects.push(mapObject);
        this.invalidate();
    };

    this.removeMapObject = function(mapObject)
    {
        var i = this.mapObjects.indexOf(mapObject);
        if (i!=null && i>-1)
        {
            this.mapObjects.splice(i,1);
            this.invalidate();
        }
    };

    this.isTileFootprintTaken = function(x, y)
    {
        if (x<0 || x>tileSizeX-1) return true;
        if (y<0 || y>tileSizeY-1) return true;

        var value = this._footprintMap[x][y];
        if (value==null)
            return false;
        else
            return true;
    };

    this.getMapObjectByFootprintAt = function(x, y)
    {
        if (x<0 || x>tileSizeX-1) return null;
        if (y<0 || y>tileSizeY-1) return null;

        return this._footprintMap[x][y];
    };

    this._drawRawSelfTo = function(context)
    {
        var viewport = this._engine.viewport;
        var objsToDraw = new Array();
        for(var moIndex = 0;moIndex<this.mapObjects.length; moIndex++)
        {
            var mapObject = this.sprites[moIndex];
            if (mapObject.isVisible()==true)
            {
                if (this.getIgnoreViewport()==false)
                {
                    if (mapObject.getX()>viewport.getViewportX()+this._engine.getWidth() || mapObject.getY()>viewport.getViewportY()+this._engine.getHeight() || mapObject.getX()+mapObject.getSpriteType().getWidth()<viewport.getViewportX() || mapObject.getY()+mapObject.getSpriteType().getHeight()<viewport.getViewportY())
                        continue;
                }
                else
                {
                    if (mapObject.getX()>this._engine.getWidth() || mapObject.getY()>this._engine.getHeight() || mapObject.getX()+mapObject.getSpriteType().getWidth()<0 || mapObject.getY()+mapObject.getSpriteType().getHeight()<0)
                        continue;
                }
                objsToDraw.push(mapObject);
            }
        }
        this._resortMapObjects(objsToDraw);
        for(var moIndex = 0;moIndex<objsToDraw.length; moIndex++)
            objsToDraw[moIndex].drawSelfTo(context);
    };

    this._resortMapObjects = function(mapObjects)
    {
        function sortFunc(mapObject1, mapObject2)
        {
            moY1 = (mapObject1.getY()+mapObject1.getAdjustedY()-mapObject1.getMapObjectType().getAdjustY()+mapObject1.getMapObjectType().getOriginY())+"";
            moY2 = (mapObject2.getY()+mapObject2.getAdjustedY()-mapObject2.getMapObjectType().getAdjustY()+mapObject2.getMapObjectType().getOriginY())+"";
            moY1 = moY1+"."+(mapObject1.getX()+mapObject1.getAdjustedX()-mapObject1.getMapObjectType().getAdjustX()+mapObject1.getMapObjectType().getOriginX());
            moY2 = moY2+"."+(mapObject2.getX()+mapObject2.getAdjustedX()-mapObject2.getMapObjectType().getAdjustX()+mapObject2.getMapObjectType().getOriginX());
            return parseFloat(moY1)-parseFloat(moY2);
        };
        mapObjects.sort(sortFunc);
    };

    ///////////////////////////////
    // Constructor area...
    ///////////////////////////////

    this._footprintMap = new Array(tileSizeX);
    for(var x = 0; x<tileSizeX; x++)
    {
        this._footprintMap[x] = new Array(tileSizeY);
        for(var y = 0; y<tileSizeY; y++)
        {
            this._footprintMap[x][y] = null;
        }
    }

}