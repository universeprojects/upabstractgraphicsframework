/**
 * Construct a new H5EMotionBehaviour object.
 * @extends H5EBehaviour
 * @class This behaviour will perform very basic animations (translation, rotation)
 * to a graphic element.
 * @constructor
 * @param {H5EGraphicElement} h5eGraphicElement The graphic element object that this behaviour belongs to
 * @return A new motion behaviour object
 */
function H5EMotionBehaviour(h5eGraphicElement)
{
    // Inherit from h5eBehaviour...
    this._super = H5EBehaviour;
    this._super(h5eGraphicElement);


    //Operating variables...
    /**An internal variable keeping track of the desired X destination  @type int*/
    this._destinationX = null;
    /**An internal variable keeping track of the desired Y destination  @type int*/
    this._destinationY = null;
    /**An internal variable keeping track of the desired rotation destination  @type int*/
    this._destinationRotation = null;
    /**An internal variable keeping track of the desired scale destination  @type float*/
    this._destinationScale = null;

    this._positionalSpeed = 1;
    this._rotationSpeed = 1;
    this._scaleSpeed = 0.1;

    // Events...
    this.onComplete = null;

    /**
     * Set the desired position of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired spot.
     * @param {int} x The X coordinate that the sprite is to animate to
     * @param {int} y The Y coordinate that the sprite is to animate to
     * @param {float} speed The speed that the motion will take place on a per tick basis
     */
    this.setPosition = function(x,y, speed)
    {
        this._destinationX = x;
        this._destinationY = y;
        this._positionalSpeed = speed;

    };


    /**
     * Set the desired rotation of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired angle.
     * @param {int} newRot The rotation (in degrees; 0-360) that the sprite is to animate to
     * @param {float} speed The speed that the motion will take place on a per tick basis
     */
    this.setRotation = function(newRot, speed)
    {
        this._destinationRotation = newRot;
        this._rotationalSpeed = speed;
    };

    /**
     * Set the desired scale of the sprite who owns this behaviour. The behaviour
     * will automatically animate the sprite to the desired size.
     * @param {float} newScale The scale (1 = 100%, 0.5 = 50%) that the sprite is to animate to
     * @param {float} speed The speed that the motion will take place on a per tick basis
     */
    this.setScale = function(newScale, speed)
    {
        this._destinationScale = newScale;
        this._scaleSpeed = speed;
    };

    this.isMoving = function()
    {
        if (this._destinationX != null ||
            this._destinationY != null ||
            this._destinationRotation != null ||
            this._destinationScale != null)
            return true;
        else
            return false;
    };

    /**
     * Overriden update function from H5EBehaviour.
     * @see H5EBehaviour#update
     */
    this.update = function()
    {
        var wasMoving = this.isMoving();

        // if we're not moving then no need to continue
        if (wasMoving==false)
            return;
            
        // Lets just invalidate the graphic element now and get it over with
        this._element.invalidate();

        // Motion X...
        if (this._destinationX!=null && this._destinationX>this._element._x)
        {
            this._element._x += this._positionalSpeed;
            if (this._destinationX<=this._element._x)
            {
                this._element._x = this._destinationX;
                this._destinationX = null;
            }
        }
        else if (this._destinationX!=null && this._destinationX<this._element._x)
        {
            this._element._x -= this._positionalSpeed;
            if (this._destinationX>=this._element._x)
            {
                this._element._x = this._destinationX;
                this._destinationX = null;
            }
        }
        // Motion Y...
        if (this._destinationY!=null && this._destinationY>this._element._y)
        {
            this._element._y += this._positionalSpeed;
            if (this._destinationY<=this._element._y)
            {
                this._element._y = this._destinationY;
                this._destinationY = null;
            }
        }
        else if (this._destinationY!=null && this._destinationY<this._element._y)
        {
            this._element._y -= this._positionalSpeed;
            if (this._destinationY>=this._element._y)
            {
                this._element._y = this._destinationY;
                this._destinationY = null;
            }
        }
        // Motion rotation...
        if (this._destinationRotation!=null && this._destinationRotation>this._element._rotation)
        {
            this._element._rotation += this._rotationSpeed;
            if (this._destinationRotation<=this._element._rotation)
            {
                this._element._rotation = this._destinationRotation;
                this._destinationRotation = null;
            }
        }
        else if (this._destinationRotation!=null && this._destinationRotation<this._element._rotation)
        {
            this._element._rotation -= this._rotationSpeed;
            if (this._destinationRotation>=this._element._rotation)
            {
                this._element._rotation = this._destinationRotation;
                this._destinationRotation = null;
            }
        }
        // Bouncy scale...
        if (this._destinationScale!=null && this._destinationScale>this._element._scale)
        {
            this._element._scale += this._scaleSpeed;
            if (this._destinationScale<=this._element._scale)
            {
                this._element._scale = this._destinationScale;
                this._desinationScale = null;
            }
        }
        else if (this._destinationScale!=null && this._destinationScale<this._element._scale)
        {
            this._element._scale -= this._scaleSpeed;
            if (this._destinationScale>=this._element._scale)
            {
                this._element._scale = this._destinationScale;
                this._desinationScale = null;
            }
        }
        if (wasMoving && this.isMoving()==false)
            if (this.onComplete!=null)
                this.onComplete();
    };

    /**
     * Overriden from H5EBehaviour.
     * @see H5EBehaviour#getName
     */
    this.getName = function()
    {
        return "motion";
    };
    //********************
    // Constructor area...
    //********************

}