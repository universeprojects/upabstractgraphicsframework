/**
 * Construct a new H5EAudio object.
 * @class This is a wrapper for the HTMLAudioElement class and is used in the H5E
 * framework for audio data.
 * @constructor
 * @param {H5EEngine} engine The engine object that this audio file belongs to
 * @return A new audio object
 */
function H5EAudio(engine)
{
    /**A reference to the engine so we can do work inside this class and can be self sufficient @type H5EEngine*/
    this._engine = engine;
    /**The actual javascript audio element @type HTMLAudioElement*/
    this._audio = new Audio();
    /**This is the original url used to find the image. It is likely a relative path @type String*/
    this._url = null;

    /**
     * This triggers the load of the audio file and sets up the events to fire when
     * it is finished, errored out or aborted.
     * @param {String} src The url pointing to the audio file to load
     * @param {function} onCompleteEvent The event that is fired when the audio file is finished loading
     * @param {function} onErrorEvent The event that is fired when the audio file load results in an error
     * @param {function} onAbortEvent The event that is fired when the audio file load is aborted by the user clicking on the stop button
     */
    this.loadAudio=function(src, onCompleteEvent, onErrorEvent, onAbortEvent)
    {
        this._url = src;
        this._audio.onerror = onErrorEvent;
        this._audio.onabort = onAbortEvent;
        this._audio.onload = onCompleteEvent;
        this._audio.src = src;
        this._audio.load();
    };


    /**
     * Returns the javascript audio element that this class is wrapping.
     * @return The javascript audio element that this class wraps
     * @type HTMLAudioElement
     */
    this.getJSAudio=function()
    {
        return this._audio;
    };

    /**The original url used to load the audio file
     * @return The original url used to load the audio file
     * @type String
     */
    this.getUrl=function()
    {
        return this._audio.src;
    };

    /**
     * Gets the duration of this audio file. If the file is not yet loaded
     * or is in an erroneous state, the length will be 0.
     * @return The duration of the audio file in seconds
     * @type int
     */
    this.getDuration = function()
    {
        return this._audio.duration;
    };

    /**
     * The original url used to load the audio file. It is likely a relative
     * url.
     * @return The original url string used when loadAudio() was called
     * @type String
     */
    this.getOriginalUrl = function()
    {
        return this._url;
    };
}