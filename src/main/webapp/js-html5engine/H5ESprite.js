/**
 * The H5ESprite class.
 * @extends H5EGraphicElement
 * @class The sprite class gets it's graphical data from the H5ESpriteType which
 * uses the H5EImage. Essentially, a sprite is an image that is displayed on a layer.
 * It has a number of transformation properties like rotation, translation and scale
 * as well as control over transparency.
 * The sprite also has it's own mouse events that can be set to trigger on a per
 * pixel basis or based simply on bounding box. The per-pixel basis allows you to
 * detect clicks on an image only if the clicked area is not made invisible by
 * the alpha mask of the image (like from a PNG file). You may also set the invisibility
 * threshold for semi transparent images.
 * @constructor The constructor for this class should not be called directly as
 * this class can be considered abstract.
 * @param {H5EEngine} engine The engine object that this image belongs to
 * @param {H5ELayer} layer The layer this sprite belongs to
 * @param {H5ESpriteType} h5eSpriteType The sprite type that this sprite will use for it's image data
 * @return A new image
 */
function H5ESprite(engine, layer, h5eSpriteType)
{
    // extend graphic element...
    this._super = H5EGraphicElement;
    this._super(engine, layer);


    // References...
    /**The h5eSpriteType that is being used for this sprite @type H5ESpriteType*/
    this._spriteType=h5eSpriteType;    

    /**The scale of this image. 1 means 100% (not scaled), 0.5 is 50%. @type float*/
    this._scale = 1;
    /**The level of transparency from 0 to 1 of the image when it is displayed. @type float*/
    this._transparency = 1;
    /**The rotation of this sprite (0 - 360) @type float*/
    this._rotation = 0;
    /**Do we want to use per-pixel hit detection for mouse actions? (clicking, mouse over..etc) @type boolean*/
    this._perPixelDetetion = false;
    /**When using perPixelDetection, a detection threshold of 0 will cause hit tests to only return true if the pixel found is completely opaque. This value can be up to 255. @type int*/
    this._detectionThreshold = 0;   
    /**The highlight level of the sprite (0 to 1). Setting this value to 1 will cause the sprite to render lighter, 0 will cause the sprite to render normally. @type float*/
    this._highlight = 0;
    /**Keeps track of whether or not the mouse is currently hovering over this sprite. This value is controlled by the H5EEngine class. @type boolean*/
    this._mouseOver = false;
    /**When the sprite is added to a layer, the layer will change this value based on where the sprite happens to be in the layer's sprite array. This allows us to remove the sprite or move it easily without having to scan through the list. @type Integer*/
    this.layerIndex = null;

    //Events...
    /**This event is fired when a click occurs on the sprite. If perPixelDetection is active, the alpha mask (with the detectionThreshold) is used to detect a hit on this sprite. @type function*/
    this.onClick = null;
    /**This event is fired when the mouse enters the area of the sprite. If perPixelDetection is active, the alpha mask (with the detectionThreshold) is used to detect a hit on this sprite. @type function*/
    this.onMouseEnter = null;
    /**This event is fired when the mouse leaves the area of the sprite. If perPixelDetection is active, the alpha mask (with the detectionThreshold) is used to detect a hit on this sprite. @type function*/
    this.onMouseExit = null;
    /**This event is fired when a mouse button is held down on the area of the sprite. If perPixelDetection is active, the alpha mask (with the detectionThreshold) is used to detect a hit on this sprite. @type function*/
    this.onMouseDown = null

    /**
     * Sets whether or not this sprite should use the 'per pixel hit detection' technique.
     * When true, all hit detection (especially with mouse actions like clicking or mouse overs)
     * will detect hits based on the transparency mask of the image. As long as the
     * transparency is within the specified threshold (sprite.setDetectionThreshold()) then
     * sprite.hitTest() would return true.
     *
     * @param {boolean} newVal True|False; are we using per pixel detection or just simple bounding box detection
     */
    this.setPerPixelDetection = function(newVal)
    {
        this._perPixelDetection = newVal;
    };

    /**
     * Returns true|false; current per pixel detection on or off.
     * @return True|False; the per pixel detections state.
     * @type boolean
     */
    this.getPerPixelDetection = function()
    {
        return this._perPixelDetection;
    };

    /**
     * Setting the highlight to a value greater than 0 will cause the sprite to
     * be drawn lighter than it would otherwise.
     *
     * @param {float} newVal A value from 0 to 1. The amount of highlighting to perform on the sprite when it is drawn.
     */
    this.setHighlight = function(newVal)
    {
        this._highlight = newVal;
        this.invalidate();
    };

    /**
     * A highlight value greater than 0 will cause the sprite to
     * be drawn lighter than it would otherwise.
     *
     * @return A value from 0 to 1. The amount of highlighting to perform on the sprite when it is drawn.
     * @type float
     */
    this.getHighlight = function()
    {
        return this._highlight;
    };

    /**
     * When using perPixelDetection, a detection threshold of 0 will 
     * cause hit tests to only return true if the pixel found is 
     * completely opaque. This value can be up to 255.
     *
     * @param {int} newVal A value from 0 - 255 representing the per pixel hit detection threshold
     */
    this.setDetectionThreshold = function(newVal)
    {
        this._detectionThreshold = newVal;
    };
    
    /**
     * When using perPixelDetection, a detection threshold of 0 will 
     * cause hit tests to only return true if the pixel found is 
     * completely opaque. This value can be up to 255.
     * @return The current per pixel hit detection threshold
     * @type int
     */
    this.getDetectionThreshold = function()
    {
        return this._detectionThreshold;
    };

    /**
     * Sets the scale of this sprite. 1 = 100% (not scaled), 0.5 = 50% scale (half size)
     * @param {float} newVal The new scale of the sprite.
     */
    this.setScale = function(newVal)
    {
        this._scale = newVal;
        this.invalidate();
    };

    /**
     * Get the scale of this sprite. 1 = 100% (not scaled), 0.5 = 50% scale (half size)
     * @return The current scale of this sprite
     * @type float
     */
    this.getScale = function()
    {
        return this._scale;
    };

    /**
     * Sets the transparency for this sprite.
     * This must be a value from 0 to 1. Values that are out of range
     * will be adjusted to the nearest boundary.
     * @param {float} newVal A value from 0 to 1 for the new transparency level of the sprite.
     */
    this.setTransparency = function(newVal)
    {
        if (newVal>1) newVal = 1;
        if (newVal<0) newVal = 0;
        if (newVal!=this._transparency)
        {
            this._transparency = newVal;
            this.invalidate();
        }
    };

    /**
     * Returns the current transparency level for this sprite.
     * @return A value from 0 to 1; the current transparency level for this sprite
     * @type float
     */
    this.getTransparency = function()
    {
        return this._transparency;
    };

    /**
     * Sets the rotation of this sprite. This should be a number from 0 to 360
     * however numbers that are beyond that range will still work.
     *
     * @param {float} newVal A value from 0 to 360 for the new rotation of the sprite
     */
    this.setRotation = function(newVal)
    {
        if (newVal!=this._rotation)
        {
            this._rotation = newVal;
            this.invalidate();
        }
    };

    /**
     * Gets the rotation of the sprite.
     * @return A value from 0 to 360; the current rotation of the sprite
     * @type float
     */
    this.getRotation = function()
    {
        return this._rotation;
    };


    /**
     * This method will change the spriteType the sprite is using. The next time
     * this sprite is drawn; the new spriteType will be displayed.
     * @param {H5ESpriteType} newH5ESpriteType The sprite type to be used for the graphical data
     */
    this.setSpriteType=function(newH5ESpriteType)
    {
        this._spriteType = newH5ESpriteType;
        this.invalidate();
    };

    /**
     * This method simply retrieves the spriteType being used to draw this sprite.
     * @return The current H5ESpriteType that is being used for this sprite's image data
     * @type H5ESpriteType
     */
    this.getSpriteType=function()
    {
        return this._spriteType;
    };

    /**
     * Returns the width for this sprite. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     * @return The width of the sprite as it is drawn on the layer
     * @type int
     */
    this.getWidth=function()
    {
        if (this._spriteType==null)
            return null;
        else
            return this.getSpriteType().getWidth();
    };

    /**
     * Returns the height for this sprite. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     * @return The height of the sprite as it is drawn on the layer
     * @type int
     */
    this.getHeight=function()
    {
        if (this._spriteType==null)
            return null;
        else
            return this.getSpriteType().getHeight();
    };



    /**
     * This draws the sprite onto the given canvas. All sprite transformation settings are
     * taken into account (x/y, rotation, scale..etc).
     *
     * TODO_OLD: The adjustedX and adjustedY need to be taken into account properly when rotating.
     *
     * @param {CanvasRenderingContext2D} canvasContext The context (surface) on which to draw this sprite
     */
    this.drawSelfTo=function(canvasContext)
    {
        if (this.getSpriteType()==null)
            return;

        var viewport = this._engine.getViewport(); 


          // Uncomment this when trying to catch a certain sprite for testing
//        var iOfSpriteType = this._engine.getResourceManager().getSpriteTypes().indexOf(this.getSpriteType());
//        if (this._engine.getResourceManager().getSpriteTypeKeys()[iOfSpriteType] == "Demo UI")
//        {
//            var i = 1;
//            i += 1;
//        }

        var validScale = this.getScale();
        if (validScale<0) validScale = 0;

        var transformations = false;
        if (this.getRotation()!=0 && this.getRotation()!=360)
            transformations = true;
        if (transformations)
            canvasContext.save();
        
        if (this.getRotation()!=0 && this.getRotation()!=360)
        {
            // Do the origin stuff...
            canvasContext.translate(this.getX()-viewport.getViewportX(), this.getY()-viewport.getViewportY())

            canvasContext.rotate(this.getRotation() * Math.PI / 180);

            // Restore origin stuff
            canvasContext.translate(-this.getX()+viewport.getViewportX(), -this.getY()+viewport.getViewportY())
//            canvasContext.translate(this.getX()+(this.getAdjustedX()*validScale)+(this.getOriginX()*validScale), this.getY()+this.getAdjustedY()+(this.getOriginY()*validScale))
//
//            canvasContext.rotate(this.getRotation() * Math.PI / 180);
//
//            // Restore origin stuff
//            canvasContext.translate(-this.getX()-(this.getAdjustedX()*validScale)-(this.getOriginX()*validScale), -this.getY()-this.getAdjustedY()-(this.getOriginY()*validScale))
        }

        // Do transparencies if it applies
        if (this.getTransparency()<1)
            canvasContext.globalAlpha = this.getTransparency();

        // Get the proper sprite source image width/height such that it
        // doesn't go out of bounds...
        var sourceImageWidth = parseInt(this.getSpriteType().areaWidth);
        var sourceImageHeight = parseInt(this.getSpriteType().areaHeight);

        if (sourceImageWidth+this.getSpriteType().areaX>this.getSpriteType().getJSImage().width)
            sourceImageWidth = this.getSpriteType().getJSImage().width-this.getSpriteType().areaX;
        if (sourceImageHeight+this.getSpriteType().areaY>this.getSpriteType().getJSImage().height)
            sourceImageHeight = this.getSpriteType().getJSImage().height-this.getSpriteType().areaY;

        // Determine if we're really going to draw or if the sprite is out of bounds...
        var skipRender = false;
        var objViewportX = this.getX()+(this.getAdjustedX()*validScale)-(this.getOriginX()*validScale);
        var objViewportY = this.getY()+(this.getAdjustedY()*validScale)-(this.getOriginY()*validScale);
        if (this._layer.getIgnoreViewport()==false)
        {
            objViewportX = viewport.transformToViewportPixelsX(objViewportX);
            objViewportY = viewport.transformToViewportPixelsY(objViewportY);
        }


        if (objViewportX>canvasContext.canvas.width ||
            objViewportY>canvasContext.canvas.height ||
            objViewportX+this.getSpriteType().areaWidth*validScale < 0 ||
            objViewportY+this.getSpriteType().areaHeight*validScale < 0)
        {
            skipRender = false;
        }

        if (skipRender==false)
        {
            canvasContext.drawImage(this.getSpriteType().getJSImage(),
                                    this.getSpriteType().areaX,
                                    this.getSpriteType().areaY,
                                    sourceImageWidth,
                                    sourceImageHeight,
                                    objViewportX,
                                    objViewportY,
                                    sourceImageWidth*validScale,
                                    sourceImageHeight*validScale);
        }

        // Now if we're using the highlight feature, we need to draw the image
        // again with the 'lighter' composite option...
        if (this._highlight>0 && skipRender==false)
        {
            canvasContext.globalAlpha = this._highlight;
            var oldComposite = canvasContext.globalCompositeOperation;
            canvasContext.globalCompositeOperation = "lighter";
            canvasContext.drawImage(this.getSpriteType().getJSImage(),
                                    this.getSpriteType().areaX,
                                    this.getSpriteType().areaY,
                                    sourceImageWidth,
                                    sourceImageHeight,
                                    objViewportX,
                                    objViewportY,
                                    sourceImageWidth*validScale,
                                    sourceImageHeight*validScale);
            canvasContext.globalCompositeOperation = oldComposite;
        }
        // And reset the canvas context to stop using a transparency when drawing
        canvasContext.globalAlpha = 1;
        if (transformations)
            canvasContext.restore();

        this._invalid = false;
    };

    /**
     * Returns true or false if the canvasX/canvasY co-ordinates intersects with
     * this sprite. If perPixel is false, a simple bounding box detection method
     * is used. Otherwise, perPixel detection is used and the given threshold will
     * be used for determining how finicky the the detection will be. A threshold
     * of 0 will cause the method to only return true if the image pixel that was
     * detected has an alpha transparency level of 255 (completely opaque). A threshold
     * of 255 will negate the alpha testing altogether.
     *
     * @param {int} canvasX The X coordinate on the layer/canvas that is being tested for a hit on this sprite
     * @param {int} canvasY The Y coordinate on the layer/canvas that is being tested for a hit on this sprite
     * @param {boolean} perPixel True|false; are we going to use the bounding box or the per pixel detection method
     * @param {int} threshold A value from 0 to 255 specifying the threshold for detecting a hit on an image with transparent or semi-transparent pixels
     * @return True or false if the hit was successful or not
     * @type boolean
     */
    this.hitTest = function(canvasX, canvasY, perPixel, threshold)
    {
        if (this.getSpriteType()==null)
            return;

        // If no arguments are supplied for these, use the sprite's settings...
        if (perPixel==null)
            perPixel = this._perPixelDetection;
        if (threshold==null)
            threshold = this._detectionThreshold;

        // Reverse translate the canvas point based on the sprite position
        canvasX-=this.getX()-this.getAdjustedX();
        canvasY-=this.getY()-this.getAdjustedY();

        // Rotate the canvas point...
        if (this.getRotation()!=0 && this.getRotation()!=360)
        {

            var rad = -this.getRotation()*Math.PI/180;
            //var rad = this.getRotation();
            var xtemp;
            xtemp = (canvasX * Math.cos(rad)) - (canvasY * Math.sin(rad));
            canvasY = (canvasX * Math.sin(rad)) + (canvasY * Math.cos(rad));
            canvasX = xtemp;

        }

        // Reverse translate the canvas point based on the origin point
        canvasX+=this.getOriginX()*this.getScale();
        canvasY+=this.getOriginY()*this.getScale();



        // Reverse scale the canvas point...
        canvasX/=this.getScale();
        canvasY/=this.getScale();


        var imagePixelX = canvasX;
        var imagePixelY = canvasY;

        if (imagePixelX>=this.getSpriteType().areaX &&
            imagePixelX<=this.getSpriteType().areaWidth+this.getSpriteType().areaX &&
            imagePixelY>=this.getSpriteType().areaY &&
            imagePixelY<=this.getSpriteType().areaHeight+this.getSpriteType().areaY)
        {
            if (perPixel && threshold<255)
            {
                var ctx = this.getSpriteType().getContext();
                var imgData = ctx.getImageData(imagePixelX, imagePixelY, 1, 1);
                var transparency = imgData.data[3];
                if (transparency>=255-threshold)
                    return true;
                else
                    return false;
            }
            else
            {
                return true;
            }
        }
        else
            return false;

        
    };



    //********************
    // Constructor area...
    //********************


}