function H5EMapObjectType(engine, spriteTypeKey, tileScale)
{
    var self = this;

    // Reference variables
    this._engine = engine;

    // Operating variables...
    this._tileScale = tileScale;
    this._spriteTypeKey = spriteTypeKey;
    this.footprint = [[]];
    this.heightmap = [[]];
    this._adjustX = 0;
    this._adjustY = 0;
    this._originX = 0;
    this._originY = 0;

    this.getSpriteType = function()
    {
        return engine.getResourceManager().getSpriteType(this._spriteTypeKey);
    };

    this.setAdjustX = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._adjustX=value;
        this.resetFootprint();
    };

    this.setAdjustY = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._adjustY=value;
        this.resetFootprint();
    };

    this.addToAdjustX = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._adjustX+=value;
        this.resetFootprint();
    };

    this.addToAdjustY = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._adjustY+=value;
        this.resetFootprint();
    };

    this.getAdjustX = function()
    {
        return this._adjustX;
    };

    this.getAdjustY = function()
    {
        return this._adjustY;
    };

    this.setOriginX = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._originX=value;
        this.resetFootprint();
    };

    this.setOriginY = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._originY=value;
        this.resetFootprint();
    };

    this.addToOriginX = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._originX+=value;
        this.resetFootprint();
    };

    this.addToOriginY = function(value)
    {
        if (value==null || isNaN(value)==true)
            value = 0;
        this._originY+=value;
        this.resetFootprint();
    };

    this.getOriginX = function()
    {
        return this._originX;
    };

    this.getOriginY = function()
    {
        return this._originY;
    };

    this.getTileAdjustX = function()
    {
        return Math.floor(this._adjustX/this._tileScale);
    };

    this.getTileAdjustY = function()
    {
        return Math.floor(this._adjustY/this._tileScale);
    };

    this.getTileSizeX = function()
    {
        var x = Math.floor(this._adjustX/this._tileScale);
        var x2 = Math.ceil((this.getSpriteType().getWidth()+this._adjustX)/this._tileScale);
        var width = x2-x;
        return width;
    };

    this.getTileSizeY = function()
    {
        var y = Math.floor(this._adjustY/this._tileScale);
        var y2 = Math.ceil((this.getSpriteType().getHeight()+this._adjustY)/this._tileScale);
        var height = y2-y;
        return height;
    };

    /**
     * Sets the footprint for this map object type to the given width/height and
     * resets all cells to true (footprint).
     *
     * @param {int} width The width of the footprint for this map object type. This can be null and if null, a width will be calculated based on the image being used for this map object type.
     * @param {int} height The height of the footprint for this map object type. This can be null and if null, a height will be calculated based on the image being used for this map object type.
     *
     */
    this.resetFootprint = function()
    {

        var width = this.getTileSizeX();
        var height = this.getTileSizeY();

        if (this._footprint!=null && this._footprint[0]!=null && this._footprint.length==width && this._footprint[0].length==height)
            return;


        // Now recreate the footprint 2D array and set all cells to true
        this._footprint = new Array(width);
        for(var x = 0; x<width; x++)
        {
            this._footprint[x] = new Array(height);
            for(var y = 0; y<height; y++)
                this._footprint[x][y] = true;
        }
    };

    this.getFootprint = function()
    {
        return this._footprint;
    };

    /**
     * Saves this object to a file (the file parameter is actually just a string
     * that will be 'saved' later). This method simply adds this object's data
     * to the 'file'.
     * Most objects can be saved this way. They all use a function that looks
     * like this to add their data to the file.
     *
     * @param {String} file The 'file' data that we will be adding to.
     * @return The modified file string
     * @type String
     */
    this._saveTo = function(file)
    {
        file += "H5EMapObjectType:\n";

        // Save the sprite type key as it is in the resource manager
        var index = this._engine.getResourceManager().getMapObjectTypes().indexOf(this);
        var key = "";
        if (index!=-1)
            key = this._engine.getResourceManager().getMapObjectTypeKeys()[index];
        file += key+"\n";

        // Save the sprite type's IMAGE key
        file += this._spriteTypeKey+"\n";

        // Save alignment...
        file += this._adjustX+"\n";
        file += this._adjustY+"\n";
        file += this._originX+"\n";
        file += this._originY+"\n";

        // Save tile scale...
        file += this._tileScale+"\n";

        // Save footprint...
        var width = this._footprint.length;
        var height = 0;
        if (width>0)
            height = this._footprint[0].length;
        file += width+"\n";
        file += height+"\n";
        for(var x = 0; x<width; x++)
            for(var y = 0; y<height; y++)
                file += this._footprint[x][y]+"\n";

        // Saving the 'heightmap' which isn't currently implemented yet but will be in the future
        for(var x = 0; x<width; x++)
            for(var y = 0; y<height; y++)
                file += "0\n";

        return file;
    };

    /////////////////////////////////////
    // Constructor area...
    /////////////////////////////////////

    this.resetFootprint();
}



/**
 * Creates a new H5EMapObjectType in the given resourceManager from the data
 * located in the file (lines) starting from the given line number (lineNum).
 * @param {String[]} lines The file data
 * @param {int} lineNum The current position in the file data
 * @param {H5EResourceManager} resourceManager The resource manager to create this sprite type in
 * @return The new line position in the file. The line position will always be the 'next' line
 * in the file. In other words, there is no need to increment the line number once this method returns.
 * @type int
 */
H5EMapObjectType.createFromFile = function(lines, lineNum, resourceManager)
{
    var mapObjectTypeKey = lines[lineNum];
    lineNum++;
    var spriteTypeKey = lines[lineNum];
    lineNum++;

    var adjustX = parseInt(lines[lineNum]);
    lineNum++;
    var adjustY = parseInt(lines[lineNum]);
    lineNum++;
    var originX = parseInt(lines[lineNum]);
    lineNum++;
    var originY = parseInt(lines[lineNum]);
    lineNum++;

    var tileScale = parseInt(lines[lineNum]);
    lineNum++;

    var tileWidth = parseInt(lines[lineNum]);
    lineNum++;
    var tileHeight = parseInt(lines[lineNum]);
    lineNum++;


    // Now create the object before we continue...the rest of the data will be
    // input directly into the new object...
    var mapObjectType = new H5EMapObjectType(resourceManager._engine, spriteTypeKey, tileScale);
    resourceManager.addMapObjectType(mapObjectTypeKey, mapObjectType);
    mapObjectType.setAdjustX(adjustX);
    mapObjectType.setAdjustY(adjustY);
    mapObjectType.setOriginX(originX);
    mapObjectType.setOriginY(originY);

    // Now it's time to read in the footprint...
    mapObjectType._footprint = new Array(tileWidth);
    for(var x = 0; x<tileWidth; x++)
    {
        mapObjectType._footprint[x] = new Array(tileHeight);
        for(var y = 0; y<tileHeight; y++)
        {
            mapObjectType._footprint[x][y] = (lines[lineNum]=="true");
            lineNum++;
        }
    }

    // Now it's time to read in the heightmap...
    mapObjectType._heightmap = new Array(tileWidth);
    for(var x = 0; x<tileWidth; x++)
    {
        mapObjectType._heightmap[x] = new Array(tileHeight);
        for(var y = 0; y<tileHeight; y++)
        {
            mapObjectType._heightmap[x][y] = parseInt(lines[lineNum]);
            lineNum++;
        }
    }

    return lineNum;
};
