var AtmosphereConnectionType =
{
    "Websocket":"Websocket",
    "Comet":"Comet",
    "Long-Polling":"Long-Polling",
    "Polling":"Polling"
}

var AtmosphereConnectionStatus =
{
    "Disconnected":0,
    "Connecting":1,
    "Connected":2
}

function AtmosphereConnection()
{
    //********************
    // Object-wide variables...
    this._messageQueue = new Array();
    this._serverUrl = "";
    this._connectionType = null;
    this._maxReconnectionTries = 0;
    this._reconnectionCount = 0;
    this._connectionStatus = AtmosphereConnectionStatus.Disconnected;
    this._latencySimulation = 0;	// This is used to simulate having a higher latency situation
    this._latencyVariationSimulation = 0;	// This is used to simulate having a fluctuating ping
    
    //********************
    // Performance related variables
    this._bytesSent = 0;
    this._bytesReceived = 0;
    this._totalBytesSent = 0;
    this._totalBytesReceived = 0;
    this._currentBytesSent = 0;
    this._currentBytesReceived = 0;
    this._performanceMonitorTimer = null;

    //*********************
    // Events that this object fires
    // onConnect(String statusMessage)
    this.onConnect = null;
    // onDisconnect(String statusMessage)
    this.onDisconnect = null;
    this.onReconnect = null;
    // onMessage(String data)
    this.onMessage = null;
    // Every second this fires if performance monitor is turned on
    this.onPerformanceUpdate = null;
    this.onMessageSent = null;

    //*********************
    //All Comet connection type variables...
    this._incomingXhr = null;
    this._gotWelcomeMessage = false;
    this._outgoingXhr = null;
    this._expectedMessageSize = 2113;   // Keeps track of the size of the message that is being downloaded from the server. The first message always contains a "welcome" string of 2113 characters.
    this._readPosition = 0; // Keeps track of where in the response.responseText buffer we last read so we can pick up where we left off (the response.responseText never empties, it just keeps filling)
    this._xhrConnectionTimeout = null;   // When a connection request is made, there are no mechanisms to keep track of a timeout situation so, we have to do it ourselves.

    //*********************
    // All WebSocket connection type variables...
    this._webSocket = null;
    this._sending = false;
    this._intervalId = null;

    this.simulateLatency = function(latency, variation)
    {
    	this._latencySimulation = latency;
    	this._latencyVariationSimulation = variation;
    };
    
    this.clearLatencySimulation = function()
    {
    	this._latencySimulation = 0;
    	this._latencyVariationSimulation = 0;
    };
    
    /**
     * Starts a new connection to the specified server (serverUrl) of the
     * given connectionType. The connection will automatically fall back to
     * various connection types depending on the support provided by the browser.
     *
     * The connection type fallback list is as follows:
     * Websocket --> Comet (Ajax, XHR) --> Long-polling --> Polling
     *
     * For example, if the connection type of "WebSocket" is given but the browser
     * does not support it, the Comet connection type will automatically be used
     * instead. If the Comet type is not supported, Long-polling is automatically
     * used..etc.
     *
     * This method will throw an exception when all fallback connection types
     * fail.
     * 
     * @param serverUrl {String} The url of the server you wish to connect to
     * @param connectionType {AtmosphereConnectionType} The type of connection to use. Default is Websocket.
     * This can be (and can only be) one of the following:
     * AtmosphereConnectionType.Websocket
     * AtmosphereConnectionType.Comet
     * AtmosphereConnectionType.Long-Polling
     * AtmosphereConnectionType.Polling
     * @param maximumAutoReconnectTries {int} The number of times this connection
     * should attempt to reconnect when a connection failure has occured before
     * giving up. Default is 0 (will not reconnect automatically)
     * @param autoFallback {boolean}
     */
    this.connect = function(serverUrl, connectionType, maximumAutoReconnectTries, autoFallback)
    {
        if (!serverUrl.startsWith("http://"))
            if (window.location.href.charAt(window.location.href.length-1)=="/")
                serverUrl = window.location.href+serverUrl;
            else
                serverUrl = window.location.href+"/"+serverUrl;

        // Always disconnect any ongoing connections first before attempting to
        // connect. If there are no ongoing connections, disconnect() will do nothing
        this.disconnect();

        this._reconnectionCount = 0;

        if (maximumAutoReconnectTries!=null)
            this._maxReconnectionTries = maximumAutoReconnectTries;
        if (connectionType==null)
            connectionType = AtmosphereConnectionType.Comet;
        if (autoFallback==null)
            autoFallback = true;

        // Now try to connect using the requested connectionType...
        if (connectionType == AtmosphereConnectionType.Websocket)
        {
            if (this.isWebSocketCapable()==false && autoFallback==true)
                connectionType = AtmosphereConnectionType.Comet;   // Fallback
            else if (this.isWebSocketCapable()==true)
            {
                // Web sockets are supported, now connect...
                this._connectWebSocketType(serverUrl);
                this._connectionType = connectionType;
                this._serverUrl = serverUrl;
                return;
            }
            else
            {
                throw("Your browser does not support Websockets and auto-fallback has been turned off.");
            }
        }
        if (connectionType == AtmosphereConnectionType.Comet)
        {
            if (this.isCometCapable()==false && autoFallback)
                connectionType = AtmosphereConnectionType.Long-Polling;    // Fallback
            else if (this.isCometCapable()==true)
            {
                // Comet is supported, now connect...
                this._connectCometType(serverUrl);
                this._connectionType = connectionType;
                this._serverUrl = serverUrl;
                return;
            }
            else
            {
                throw("Your browser does not support Comet and auto-fallback has been turned off.");
            }

        }

        // If we get here then the browser doesn't support any connectionTypes
        // that we handle so we're throwing an error...
        throw("Your browser does not support any of the necessary transport methods. Please use a more updated browser.");
    };

    this._connectWebSocketType = function(serverUrl)
    {
        if (serverUrl.toLowerCase().startsWith("http://"))
            serverUrl = serverUrl.replace(/http:/i, "ws:");
        else if (serverUrl.toLowerCase().startsWith("https://"))
            serverUrl = serverUrl.replace(/https:/i, "wss://");
        else if (serverUrl.indexOf("://")==-1)
            serverUrl = "ws://"+serverUrl;

        this._webSocket = new WebSocket(serverUrl);
        this._webSocket.atmosphereConnection = this;
        this._webSocket.onopen = this._doWSConnect;
        this._webSocket.onmessage = this._doWSMessage;
        this._webSocket.onerror = this._doWSError;
        this._webSocket.onclose = this._doWSDisconnect;
        this._connectionStatus = AtmosphereConnectionStatus.Connecting;
    };

    this._doWSConnect = function(message)
    {
        var self = this.atmosphereConnection;
        self._expectedMessageSize=-1;
        if (self.onConnect!=null)
            self.onConnect("Connected using WebSockets to "+self._serverUrl.replace("http:", "ws:"));
        self._connectionStatus = AtmosphereConnectionStatus.Connected;
    };

    this._doWSMessage = function(rawData)
    {
        var self = this.atmosphereConnection;

        if (self._performanceMonitorTimer!=null)
            self._currentBytesReceived+=message.data.length;

        // The old way of handling the packet
//        if (self.onMessage!=null)
//            self.onMessage(message.data);
        
        var message = rawData.data;
    	var index = 0;
        // Get the size of the data sent to us...
        if (self._expectedMessageSize==-1 && message.indexOf("|")>-1)
    	{
        	var msgSizeStr = "";
        	while(message.charAt(index)!="|")
        	{
        		msgSizeStr+=message.charAt(index);
        		index++;
        	}
        	index++;
            self._expectedMessageSize = parseInt(msgSizeStr);
    	}

        if (self._expectedMessageSize>-1)
        while(message.length>=self._expectedMessageSize)
        {
            // Substring the part of the message that we care about
            // (excluding stuff that has already been read in) and
            // chop off the header bit that talks about the length of the message
            // Except for the welcome message, there is no header for that.
            message = message.substr(index, self._expectedMessageSize-(self._expectedMessageSize+"").length-1);

            // Remember up to what point we've dealt with the message data we received
            index+=message.length;

            // Aaand, send the packet to the javascript client
            if (self.onMessage!=null)
                self.onMessage(message);

            
            // Now check to see if we have the header for another packet...
            message = rawData.data.substr(index);
            if (message.indexOf("|")>-1)
        	{
            	var msgSizeStr = "";
            	while(message.charAt(index)!="|")
            	{
            		msgSizeStr+=message.charAt(index);
            		index++;
            	}
            	index++;
                self._expectedMessageSize = parseInt(msgSizeStr);
        	}
            else
            {
                self._expectedMessageSize = -1;
                break;
            }

        }
    };

    this._doWSError = function(evt)
    {
        var self = this.atmosphereConnection;

        if (self._reconnectionCount<self._maxReconnectionTries)
        {
            // Don't report an error, we're just going to try to reconnect...
            self.reconnect();
        }
        else
        {
            // No reconnect. Disconnect ourselves completely and report
            // the disonnection...
            self.disconnect();
            if (self.onDisconnect!=null)
                self.onDisconnect("Connection error: "+evt.type);
        }
    };

    this._doWSDisconnect = function(evt)
    {
        var self = this.atmosphereConnection;
        // No reconnect. Disconnect ourselves completely and report
        // the disonnection...
        if (self.onDisconnect!=null && self._connectionStatus == AtmosphereConnectionStatus.Connecting)
        {
            if (evt.type == "close")
            {
                self.disconnect();
                if (self.onDisconnect!=null)
                    self.onDisconnect("Connection error: Unable to connect to server");
            }
            else
            {
                self.disconnect();
                if (self.onDisconnect!=null)
                    self.onDisconnect("Connection error: "+evt.type);
            }
        }
        else if (self.onDisconnect!=null && self._connectionStatus == AtmosphereConnectionStatus.Connected)
        {
            self.disconnect();
            if (self.onDisconnect!=null)
                self.onDisconnect("Connection error: Disconnected from server");
        }
    };


    //*********************************************************************
    //*********************************************************************
    // All comet related code (AJAX/Comet)

    /**
     * Creates a new XMLHttpRequest object and returns it if the browser supports it.
     * This method even has support back to IE5.
     * If the current browser does not support AJAX, this method will return null.
     * @return {XMLHttpRequest} A new XMLHttpRequest object or null if the browser doesn't support it
     */
    this._createAjaxObject = function()
    {
        var xmlhttp = null;
        // JScript gives us Conditional compilation, we can cope with old IE versions.
        // and security blocked creation of the objects.
        try
        {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e)
        {
            try
            {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E)
            {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest!='undefined') 
            xmlhttp = new XMLHttpRequest();

        return xmlhttp;
    };

    /**
     * Connects to the given serverUrl using the Comet (Ajax push) technique.
     * @param serverUrl {String} The url being used to find the server on the mighty interweb
     */
    this._connectCometType = function(serverUrl)
    {
        if (serverUrl.toLowerCase().startsWith("ws://"))
            serverUrl = serverUrl.replace(/ws:/i, "http:");
        else if (serverUrl.toLowerCase().startsWith("wss://"))
            serverUrl = serverUrl.replace(/wss:/i, "https:");
        else if (serverUrl.indexOf("://")==-1)
            serverUrl = "http://"+serverUrl;

        // Create the incoming data ajax object. This is for receiving messages
        // from the server only. Not transmitting is done with this
        this._expectedMessageSize = 2113;
        this._gotWelcomeMessage = false;
        this._readPosition = 0;
        this._incomingXhr = this._createAjaxObject();
        this._incomingXhr.atmosphereConnection = this;
        this._incomingXhr.onreadystatechange = this._doIncomingXhr;
        this._incomingXhr.open("GET", serverUrl, true);
        this._xhrConnectionTimeout = setTimeout(this._doXhrTimeout, 5000, this);
        this._incomingXhr.send(null);
        
    };

    this._doXhrTimeout = function(atmosphereConnectionObj)
    {
        var self = atmosphereConnectionObj;
        if (self._reconnectionCount<self._maxReconnectionTries)
        {
            // Don't report an error, we're just going to try to reconnect...
            self._reconnectionCount++;
            self.reconnect();
        }
        else
        {
            // No reconnect. Disconnect ourselves completely and report
            // the disonnection...
            self.disconnect();
            if (self.onDisconnect!=null)
                self.onDisconnect("Connection error: Unable to connect to server.");
        }
    };

    this._doOutgoingXhr = function()
    {
        if (this.readyState == 4)
        {
            if (this.status==200)  // Received a message
            {
                if (this.atmosphereConnection.onMessageSent!=null)
                    this.atmosphereConnection.onMessageSent(this.atmosphereConnection._messageQueue[0]);
                // Message sent successfully so we'll remove it from the queue
                // and try to process the next message if there is any
                this.atmosphereConnection._messageQueue.shift();
            	this.atmosphereConnection._processMessageQueue();
            }
            else
            {
                // We had an error so we are going to attempt to re-send the message
                // every 500 milliseconds.
                setTimeout("this.atmosphereConection._processMessageQueue()", 500);
            }
        }
    };

    /**
     * This handles the incoming xhr's ready state change.
     */
    this._doIncomingXhr = function()
    {
    	window.atmosphereConnection = this.atmosphereConnection;
        var self = this.atmosphereConnection;
        if (this.readyState == 3)
        {
            if (this.status==200)  // Received a message
            {

                var message = this.responseText;
                if (self._performanceMonitorTimer!=null)
                    self._currentBytesReceived+=message.length-self._readPosition;

                if (self._expectedMessageSize==-1 && message.indexOf("|", self._readPosition)>-1)
            	{
                    self._expectedMessageSize = parseInt(message.substr(self._readPosition).split("\\|")[0]);
                    // And add on the header size...
                    self._expectedMessageSize += (self._expectedMessageSize+"|").length;
            	}
                                
                
                if (self._expectedMessageSize>-1)
            	{
                	while(message.length>=self._expectedMessageSize+self._readPosition)
	                {
	                    // Substring the part of the message that we care about
	                    // (excluding stuff that has already been read in) and
	                    // chop off the header bit that talks about the length of the message
	                    // Except for the welcome message, there is no header for that.
	                    if (self._readPosition>0)
	                        message = message.substr(self._readPosition+(self._expectedMessageSize+"").length+1, self._expectedMessageSize-(self._expectedMessageSize+"").length+1);
	
	
	                    
	                    if (self._gotWelcomeMessage && self.onMessage!=null)
                    	{
	                    	if (self._latencySimulation>0)
                    		{
		                    	var temp = message.toString();
	                    		setTimeout(function(){this.atmosphereConnection.onMessage(temp);},  this._latencySimulation*2+Math.round(Math.random()*this._latencyVariationSimulation));
                    		}
	                    	else
	                    		self.onMessage(message);
                    	}
	                    else if (message.length>=2113)
	                    {
	                        self._gotWelcomeMessage = true;
	                        if (self.onConnect!=null)
	                            self.onConnect("Connected using Comet to "+self._serverUrl);
	
	                        clearTimeout(self._xhrConnectionTimeout);
	                    }
	
	                    // And remember where we read up to in the receive buffer
	                    self._readPosition += self._expectedMessageSize;
	
	                    // Now check to see if we have the header for another packet...
	                    message = this.responseText;
	                    if (message.indexOf("|", self._readPosition)>-1)
                    	{
	                        self._expectedMessageSize = parseInt(message.substr(self._readPosition).split("\\|")[0]);

	                        // And add on the header size...
	                        self._expectedMessageSize += (self._expectedMessageSize+"|").length;
                    	}
	                    else
	                    {
	                        self._expectedMessageSize = -1;
	                        break;
	                    }
	
	                }
            	}                
            }
        }
        else if (this.readyState == 4)
        {
            if (self._reconnectionCount<self._maxReconnectionTries)
            {
                // Don't report an error, we're just going to try to reconnect...
                self._reconnectionCount++;
                self.reconnect();
            }
            else
            {
                // No reconnect. Disconnect ourselves completely and report
                // the disonnection...
                self.disconnect();
                if (self.onDisconnect!=null && this.status == 200)
                    self.onDisconnect("Connection error: The connection was lost. Either the server closed the connection or your connection to the internet failed.");
                else if (self.onDisconnect!=null)
                    self.onDisconnect("Connection error "+this.status+": "+this.statusText);
            }
        }
    };
    this.sendMessage = function(text)
    {
        if (this.isConnected()==false)
            throw("You are not connected to a server.");
        
        if (this._latencySimulation>0)
    	{
        	var temp = text.toString();
        	window.atmosphereConnection = this;
    		setTimeout(function(){window.atmosphereConnection._sendDelayedMessage(temp);},  this._latencySimulation*2+Math.round(Math.random()*this._latencyVariationSimulation));
    	}
        else
    	{
	        // Add to message queue...
	        this._messageQueue.push(text);
	
	        // And attempt to process the queue
	    	this._processMessageQueue();
    	}
    };

    this._sendDelayedMessage = function(text)
    {
        // Add to message queue...
        this._messageQueue.push(text);

        // And attempt to process the queue
    	this._processMessageQueue();
    };

    this._processMessageQueue = function()
    {

        if (this._connectionType == AtmosphereConnectionType.Websocket)
        {
        	if (this._webSocket.bufferedAmount>0)
        		return;
        	
        	// If we're here and sending = true then that can only mean that the last message
        	// was sent successfully and we can continue. Now we must remove the last messsage 
        	// from the queue that was sent successfully and move on.
            if (this._sending==true)
                this._messageQueue.shift();
            
            if (this._messageQueue.length>0 && this._webSocket.bufferedAmount==0)
            {
                this._webSocket.send(this._messageQueue[0]);

                // Performance monitor stuff
                if (this._performanceMonitorTimer!=null)
                    this._currentBytesSent+=this._messageQueue[0].length;
                if (this._sending==false)
            	{
	                this._sending=true;
	                this._intervalId = setInterval(function(that){that._processMessageQueue();}, 5, this);
            	}
            }
            else if (this._sending==true && this._webSocket.bufferedAmount==0)
            {
                this._sending=false;
                clearInterval(this._intervalId);
            }
        }
        else if (this._connectionType == AtmosphereConnectionType.Comet)
        {
            if (this.isConnected()==false && this._messageQueue.length>0)
                throw("You are not connected to a server.");
            
            if (this._messageQueue.length>0 && (this._outgoingXhr==null || this._outgoingXhr.readyState==4))
            {
                var text = this._messageQueue[0];
                if (this._outgoingXhr==null)
                {
                    this._outgoingXhr = this._createAjaxObject();
                    this._outgoingXhr.atmosphereConnection = this;
                    this._outgoingXhr.onreadystatechange = this._doOutgoingXhr;
                }
//                this._outgoingXhr.abort();
                this._outgoingXhr.onreadystatechange = this._doOutgoingXhr;
                this._outgoingXhr.open("POST", this._serverUrl, true);
                this._outgoingXhr.send(text);

                // Performance monitor stuff
                if (this._performanceMonitorTimer!=null)
                    this._currentBytesSent+=text.length;

            }
        }
    };

    /**
     * Disconnects from the server. If no connection is active then
     * nothing happens.
     */
    this.disconnect = function()
    {
        this._connectionRequestSent = false;
        this._messageQueue = new Array();
        
        if (this._connectionType == AtmosphereConnectionType.Websocket)
        {
            this._webSocket.close();
        }
        else if (this._connectionType == AtmosphereConnectionType.Comet)
        {
            var wasConnected = this.isConnected();
            if (this._incomingXhr!=null)
            {
                this._incomingXhr.onreadystatechange = null;
                this._incomingXhr.abort();
                this._incomingXhr = null;
            }
            if (this._outgoingXhr!=null)
            {
                this._outgoingXhr.onreadystatechange = null;
                this._outgoingXhr.abort();
                this._outgoingXhr = null;
            }

            if (wasConnected && this.onDisconnect!=null)
                this.onDisconnect("Disconnected Comet connection from server "+this._serverUrl);

            clearTimeout(this._xhrConnectionTimeout);
            this._xhrConnectionTimeout = null;
        }
    };

    /**
     * Reconnects with the server using the last serverUrl and connectionType
     * (the last successful connectionType used which may be a fallback type).
     *
     * This method will also disconnect if a connection is already in place.
     */
    this.reconnect = function()
    {
        // First ensure we're fully disconnected
        this.disconnect();
        this._reconnectionCount++;
        var temporaryCount = this._reconnectionCount;   // Reconnection count gets reset in .connect()
        // Now reconnect to the same server with the connection type we were last connected with
        this.connect(this._serverUrl, this._connectionType);
        this._reconnectionCount = temporaryCount;
    };

    /**
     * @return {boolean} Returns true|false; whether we are connected to a server or not
     */
    this.isConnected = function()
    {
        if (this._connectionType == AtmosphereConnectionType.Websocket)
        {
            if (this._webSocket!=null && this._webSocket.readyState==1)
                return true;
            else
                return false;
        }
        else if (this._connectionType == AtmosphereConnectionType.Comet)
        {
            if (this._incomingXhr!=null && this._incomingXhr.readyState==3 && this._incomingXhr.status == 200)
                return true;
            else
                return false;
        }
        else
            return false;
    };

    /**
     * @Return True if the browser is capable of using WebSocket technology. False
     * if it is not.
     */
    this.isWebSocketCapable = function()
    {
        try
        {
            return (WebSocket!=null);
        }
        catch(e)
        {
            return false;
        }
    };


    /**
     * @Return True if the browser is capable of using Comet (Ajax, XHR) technology. False
     * if it is not.
     */
    this.isCometCapable = function()
    {
        if (this._createAjaxObject()!=null)
            return true;
        else
            return false;
    };

    /**
     * @return True if the browser is capable of using the Long-Polling technique
     * for data transfer. False if it is not.
     */
    this.isLongPollingCapable = function()
    {
        //TODO_OLD: Determine if the browser is capable of using the long polling technique
        return false;   // For now, we're always returning false because we have not implemented this connection type yet
    };


    /**
     * @return True if the browser is capable of using the polling technique
     * for data transfer. False if it is not.
     */
    this.isPollingCapable = function()
    {
        //TODO_OLD: Determine if the browser is capable of using the polling technique  
        return false;   // For now, we're always returning false because we have not implemented this connection type yet
    };

    /**
     * Outputs a line of text to a javascript console. In firefox, it is best to use
     * firebug. In chrome, the javascript console should contain the text.
     */
    this.log = function(text)
    {
        if (window.console && window.console.log)
            window.console.log(text);
    };

    /**
     * Outputs a line of error text to a javascript console. In firefox, it is best to use
     * firebug. In chrome, the javascript console should contain the text.
     */
    this.error = function(text)
    {
        if (window.console && window.console.log)
            window.console.error(text);
    };

    /**
     * Outputs a line of info text to a javascript console. In firefox, it is best to use
     * firebug. In chrome, the javascript console should contain the text.
     */
    this.info = function(text)
    {
        if (window.console && window.console.log)
            window.console.info(text);
    };

    /**
     * Outputs a line of warning text to a javascript console. In firefox, it is best to use
     * firebug. In chrome, the javascript console should contain the text.
     */
    this.warn = function(text)
    {
        if (window.console && window.console.log)
            window.console.warn(text);
    };

    /**
     * Enables or disables the performance monitoring system which keeps track of
     * network transfer rates and amounts.
     */
    this.enablePerformanceMonitor = function(value)
    {
        if (this._performanceMonitorTimer!=null)
            clearInterval(this._performanceMonitorTimer);
        if (value==true)
        {
            this._performanceMonitorTimer = setInterval(this._refreshPerformanceMonitor, 1000, this);
        }
    }

    this._refreshPerformanceMonitor = function(atmosphereConnection)
    {
        atmosphereConnection._totalBytesSent += atmosphereConnection._currentBytesSent;
        atmosphereConnection._totalBytesReceived += atmosphereConnection._currentBytesReceived;
        atmosphereConnection._bytesSent = atmosphereConnection._currentBytesSent;
        atmosphereConnection._bytesReceived = atmosphereConnection._currentBytesReceived;
        atmosphereConnection._currentBytesSent = 0;
        atmosphereConnection._currentBytesReceived = 0;
        if (atmosphereConnection.onPerformanceUpdate!=null)
            atmosphereConnection.onPerformanceUpdate(atmosphereConnection.getPerformanceStatus());
    }

    this.getPerformanceStatus = function()
    {
        if (this._performanceMonitorTimer==null)
            return "Performance monitor is off";
        else
            return this._bytesSent+" bytes sent/s, "+this._bytesReceived+" bytes received/s, "+(this._bytesSent+this._bytesReceived)+" bytes/s, "+this._totalBytesSent+" bytes sent, "+this._totalBytesReceived+" bytes received, "+(this._totalBytesSent+this._totalBytesReceived)+" total bytes";
    }

    /**
     *  The number of messages waiting to be sent to the server.
     *  
     * @return {int} Returns the number of messages waiting to be sent to the server.
     */
    this.getMessageQueueSize = function()
    {
        return this._messageQueue.length;
    }
}

// Add startsWith to the string prototype
String.prototype.startsWith = function(str) {return (this.match("^"+str)==str)};